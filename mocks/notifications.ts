export const NOTIFICATIONS_CHATS = [
    {
        "author": {
            "avatar": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
            "name": "Sam Smith"
        },
        "time": "2020-07-04T11:34:56Z",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    },
    {
        "author": {
            "avatar": "",
            "name": "Sam Smith"
        },
        "time": "2020-07-04T11:34:56Z",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    },
    {
        "author": {
            "avatar": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
            "name": "Sam Smith"
        },
        "time": "2020-07-04T11:34:56Z",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    }
];

export const NOTIFICATIONS_SPACES = [
    {
        "author": {
            "avatar": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
            "name": "Householders"
        },
        "business_type": "Community",
        "time": "2020-07-04T11:34:56Z"
    },
    {
        "author": {
            "avatar": "",
            "name": "Bakery"
        },
        "business_type": "Business",
        "time": "2020-07-04T11:34:56Z"
    }
];