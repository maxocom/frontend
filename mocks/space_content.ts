export const space_content = [
    {   
        "type": "tabs",
        "tabs": {
            "heads": ["Buy", "Rent", "Newsfeed", "Jobs", "Contacts", "Project chats", "Services", "Work schedule", "Reviews"],
            "content":[
                [
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                ],
                [
                    {
                        "type": "house",
                        "image": "",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    },
                    {
                        "type": "house",
                        "image": "",
                        "title": "Name of house",
                        "items": [
                            {
                                "name": "Area:",
                                "value": "129 m2"
                            },
                            {
                                "name": "Rooms:",
                                "value": "3"
                            }
                        ]
                    }
                ],
                [
                    {
                        "type": "news_preview",
                        "image": "https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2020/03/single_arm_galaxy/21893256-1-eng-GB/Single_arm_galaxy_pillars.jpg",
                        "title": "News title",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus",
                        "date": "2020-07-25T12:34:56Z",
                        "id": 0,
                        "content": [
                          {
                              "type": "contentParagraph",
                              "text": "Title for paragraph"
                          },
                          {
                              "type": "contentText",
                              "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                          },
                          {
                              "type": "contentImage",
                              "image": "https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2020/03/single_arm_galaxy/21893256-1-eng-GB/Single_arm_galaxy_pillars.jpg"
                          },
                          {
                              "type": "contentText",
                              "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                          },
                          {
                              "type": "contentParagraph",
                              "text": "Title for paragraph"
                          },
                          {
                              "type": "contentText",
                              "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                          },
                           {
                                "type": "contentList",
                                "items": [
                                    {
                                        "type": "listText",
                                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur"
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    }
                                ]
                            },
                          {
                              "type": "contentSlider",
                              "items": [
                                  {
                                      "type": "sliderImage",
                                      "image": "https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2020/03/single_arm_galaxy/21893256-1-eng-GB/Single_arm_galaxy_pillars.jpg"
                                  },
                                  {
                                      "type": "sliderImage",
                                      "image": "https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2020/03/single_arm_galaxy/21893256-1-eng-GB/Single_arm_galaxy_pillars.jpg"
                                  },
                                  {
                                      "type": "sliderImage",
                                      "image": "https://www.esa.int/var/esa/storage/images/esa_multimedia/images/2020/03/single_arm_galaxy/21893256-1-eng-GB/Single_arm_galaxy_pillars.jpg"
                                  },
                              ]
                          },
                        ]
                    },
                    {
                        "type": "news_ads",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Name of Ads",
                        "sticker": "By 'Happy Cake Corp.'",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis",
                        "id": 1,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "Title for paragraph"
                            },
                        ]
                    },
                    {
                        "type": "news_preview",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Title",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus",
                        "id": 2,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "Title for paragraph"
                            },
                            {
                                "type": "contentText",
                                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                            },
                            {
                                "type": "contentList",
                                "items": [
                                    {
                                        "type": "listText",
                                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur"
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    }
                                ]
                            },
                            {
                                "type": "contentParagraph",
                                "text": "About this offer"
                            },
                            {
                                "type": "contentText",
                                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                            },
                            {
                                "type": "contentList",
                                "items": [
                                    {
                                        "type": "listText",
                                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur"
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        "type": "news_preview",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Title",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus",
                        "id": 3,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "Title for paragraph"
                            }
                        ]
                    }
                ],
                [
                    {
                        "type": "job_offer",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Lawyer at Gazprom",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis",
                        "date": "2020-07-25T12:34:56Z",
                        "id": 0,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "About this offer"
                            },
                            {
                                "type": "contentText",
                                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                            },
                            {
                                "type": "contentList",
                                "items": [
                                    {
                                        "type": "listText",
                                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur"
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    }
                                ]
                            },
                            {
                                "type": "contentParagraph",
                                "text": "About this offer"
                            },
                            {
                                "type": "contentText",
                                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur. Et, convallis faucibus dictum adipiscing eu pretium. Nulla blandit eget tempor id risus tellus. Aenean amet egestas et sed nibh eu, pellentesque at. Adipiscing diam suspendisse consectetur nulla. Nulla fermentum rhoncus, in odio. Egestas nunc, blandit a vel pretium. Sagittis, est facilisis orci, scelerisque massa. Gravida vulputate consectetur hac nunc pellentesque a faucibus mi lectus."
                            },
                            {
                                "type": "contentList",
                                "items": [
                                    {
                                        "type": "listText",
                                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur"
                                    },
                                    {
                                      "type": "listText",
                                      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tortor consequat lobortis id dis eleifend ornare sit nascetur."
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        "type": "job_offer",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Job offer",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis",
                        "date": "2020-07-25T12:34:56Z",
                        "id": 1,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "Title for paragraph"
                            }
                        ]
                    },
                    {
                        "type": "job_offer",
                        "image": "https://learn.zoner.com/wp-content/uploads/2018/08/landscape-photography-at-every-hour-part-ii-photographing-landscapes-in-rain-or-shine.jpg",
                        "title": "Job offer",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis",
                        "date": "2020-07-25T12:34:56Z",
                        "id": 2,
                        "content": [
                            {
                                "type": "contentParagraph",
                                "text": "Title for paragraph"
                            }
                        ]
                    },
                ],
                [
                    {
                        "type": "row_text",
                        "label": "Phone",
                        "text": "+1 (212) 142-23-22"
                    },
                    {
                        "type": "row_text",
                        "label": "E-mail",
                        "text": "example@gmail.com"
                    },
                    {
                        "type": "location",
                        "label": "Location",
                        "text": "17, St. Willington",
                        "lat": 38.24151992265516,
                        "lon": 55.576889091808944,
                        "marker_title": "Householders Business"
                    },
                    {
                        "type": "group",
                        "title": "Group of companies",
                        "group": [
                            {
                                "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                                "title": "General bakery"
                            },
                            {
                                "image": "",
                                "title": "Second bakery"
                            },
                            {
                                "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                                "title": "Third bakery"
                            }
                        ]
                    }
                ],
                [
                    {
                        "type": "chat_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Bakers",
                        "last_message": {
                            "author": {
                                "name": "Mark"
                            },
                            "text": "We need more buns",
                            "time": "2020-06-25T11:34:56Z"
                        },
                        "unreaded_count": 3
                    },
                    {
                        "type": "chat_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Administration",
                        "last_message": {
                            "author": {
                                "name": "Ana"
                            },
                            "text": "Please fill out the documents to we need more buns we need more buns we need more buns",
                            "time": "2020-06-25T11:34:56Z"
                        },
                        "unreaded_count": 10
                    },
                    {
                        "type": "chat_preview",
                        "image": "",
                        "title": "Marketing team",
                        "last_message": {
                            "author": {
                                "name": "Ana"
                            },
                            "text": "Please fill out the documents to we need more buns we need more buns we need more buns",
                            "time": "2020-06-25T11:34:56Z"
                        },
                        "unreaded_count": 0
                    },
                    {
                        "type": "chat_preview",
                        "image": "",
                        "title": "Directors ' chat",
                        "last_message": {
                            "author": null,
                            "text": "Limited access",
                            "time": null
                        },
                        "unreaded_count": 0
                    }
                ],
                [
                    {
                        "type": "services_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Repair of classic shoes",
                        "price": "from $10"
                    },
                    {
                        "type": "services_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Repair of professional shoes",
                        "price": "from $20"
                    },
                    {
                        "type": "services_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Repair of sneakers",
                        "price": "from $5"
                    },
                    {
                        "type": "services_preview",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Repair of women's shoes",
                        "price": "from $3"
                    },
                ],
                [
                    {
                        "type": "schedule",
                        "days": [
                            {
                                "type": "schedule_item",
                                "day": "Monday",
                                "value": "8:00 am - 7:30 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Tuesday",
                                "value": "10:00 am - 5:30 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Wednesday",
                                "value": "3:00 am - 9:30 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Thursday",
                                "value": "8:00 am - 8:30 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Friday",
                                "value": "8:00 am - 4:00 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Saturday",
                                "value": "1:00 pm - 5:00 pm"
                            },
                            {
                                "type": "schedule_item",
                                "day": "Wednesday",
                                "value": "Output"
                            }
                        ]
                    }
                ],
                [
                    {
                        "type": "review_item",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Kristin Watson",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem et pretium eleifend vel mauris tellus. Felis luctus nec consectetur porta sapien amet vitae. Faucibus cursus tristique fermentum, neque, scelerisque. Sed nisi vitae nunc blandit sit. Adipiscing in adipiscing pharetra, morbi aliquam. Vel lectus morbi urna lacus vel, augue massa erat mi.",
                        "date": "2020-06-25T11:34:56Z",
                        "rating": 3.5
                    },
                    {
                        "type": "review_item",
                        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
                        "title": "Kristin Watson",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem et pretium eleifend vel mauris tellus. Felis luctus nec consectetur porta sapien amet vitae. Faucibus cursus tristique fermentum, neque, scelerisque.",
                        "date": "2020-06-15T11:34:56Z",
                        "rating": 4
                    }
                ]
            ]
        }
    }
]