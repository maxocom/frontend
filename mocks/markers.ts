import images from '../src/images';

export const MOCK_MARKERS = [
  {
    coordinate: {
      latitude: 51.5305817,
      longitude: -0.1324007,
    },
    title: 'Nails Studio',
    description: 'Nails by Marisa',
    marker: images.markerBlue,
    distance: '4 min',
    users: 37,
  },

  {
    coordinate: {
      latitude: 51.5244817,
      longitude: -0.1314007,
    },
    title: 'Barber Shop',
    description: 'All beards here',
    marker: images.markerBlue,
    distance: '6 min',
    users: 264,
  },

  {
    coordinate: {
      latitude: 51.5327817,
      longitude: -0.1444007,
    },
    title: 'Meat&Fruits',
    description: 'Grocery shop',
    marker: images.markerBlue,
    distance: '10 min',
    users: 1576,
  },
];
