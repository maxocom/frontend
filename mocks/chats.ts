export const CHATS = [
     {
        "type": "chat_preview",
        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
        "title": "Bakers",
        "group": {
            "label": "Business",
            "id": 1
        },
        "space_title": "Happy Cake",
        "last_message": {
            "author": {
                "name": "Mark"
            },
            "text": "We need more buns",
            "time": "2020-06-25T11:34:56Z"
        },
        "unreaded_count": 3
    },
    {
        "type": "chat_preview",
        "image": "https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png",
        "title": "Administration",
        "group": {
            "label": "Business",
            "id": 1
        },
        "space_title": "Happy Cake",
        "last_message": {
            "author": {
                "name": "Ana"
            },
            "text": "Please fill out the documents to we need more buns we need more buns we need more buns",
            "time": "2020-06-25T11:34:56Z"
        },
        "unreaded_count": 10
    },
    {
        "type": "chat_preview",
        "image": "",
        "title": "Marketing team",
        "group": {
            "label": "Community",
            "id": 2
        },
        "space_title": "Happy Cake",
        "last_message": {
            "author": {
                "name": "Ana"
            },
            "text": "Please fill out the documents to we need more buns we need more buns we need more buns",
            "time": "2020-06-25T11:34:56Z"
        },
        "unreaded_count": 0
    },
    {
        "type": "chat_preview",
        "image": "",
        "title": "Directors ' chat",
        "group": {
            "label": "Self-Employed",
            "id": 3
        },
        "space_title": "Happy Cake",
        "last_message": {
            "author": null,
            "text": "Limited access",
            "time": null
        },
        "unreaded_count": 0
    }
]