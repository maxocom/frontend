export const members = [
    {
        "id": 1,
        "name": "Jane Cooper",
        "position": "Creator",
        "position_id": 0,
        "type": "owner",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 2,
        "name": "Wade Warren",
        "position": "Administrator",
        "position_id": 1,
        "type": "admin",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 3,
        "name": "Esther Howard",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 4,
        "name": "Cameron Williamson",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 5,
        "name": "Brooklyn Simmons",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 6,
        "name": "Leslie Alexander",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 7,
        "name": "Jenny Wilson",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 8,
        "name": "Jane Cooper",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 9,
        "name": "Wade Warren",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 10,
        "name": "Esther Howard",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 11,
        "name": "Cameron Williamson",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 12,
        "name": "Brooklyn Simmons",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 13,
        "name": "Leslie Alexander",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 14,
        "name": "Jenny Wilson",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
    {
        "id": 15,
        "name": "Jenny Wilson",
        "position": "Designer",
        "image": 'https://bitnovosti.com/wp-content/uploads/2017/02/telegram-icon-7.png'
    },
]