# Original from https://github.com/FontCustom/fontcustom/blob/master/lib/fontcustom/scripts/generate.py

import argparse
import fontforge
import os
import subprocess
import tempfile
import json
import xml.etree.ElementTree as et

parser = argparse.ArgumentParser()
parser.add_argument('manifest', help='Path to manifest.json')
args = parser.parse_args()
base_dir = os.path.dirname(os.path.abspath(args.manifest))
manifestfile = open(args.manifest)

manifest = json.load(manifestfile)
options = manifest['options']

glyphmap = dict()

#
# Font
#

design_px = options['font_em'] / options['font_design_size']

font = fontforge.font()
font.encoding = 'UnicodeFull'
font.design_size = options['font_design_size']
font.em = options['font_em']
font.ascent = options['font_ascent']
font.descent = options['font_descent']
font.fontname = options['font_name']
font.familyname = options['font_name']
font.fullname = options['font_name']
font.copyright = options['copyright']
if options['autowidth']:
    font.autoWidth(0, 0, options['font_em'])

#
# Glyphs
#

def processSvg(src, dest):
    tree = et.parse(src)
    root = tree.getroot()
    # Remove viewBox, because ff will autorisize to fit em box
    if root.attrib['viewBox']:
        del root.attrib['viewBox']
    tree.write(dest, encoding='utf-8')

def createGlyph(name, data):
    code = data['code']
    source = data['source']
    glyphmap[name] = code
    full_source_path = os.path.join(base_dir, source)
    frag, ext = os.path.splitext(full_source_path)

    if ext == '.svg':
        glyph = font.createChar(code, name)
        with tempfile.NamedTemporaryFile(suffix='.svg', delete=False) as tmp:
            processSvg(full_source_path, tmp.name)
            glyph.importOutlines(tmp.name)

        if options['autowidth']:
            glyph.left_side_bearing = glyph.right_side_bearing = 0
            glyph.round()
        else:
            glyph.width = options['font_em']
            width = glyph.width - glyph.left_side_bearing - glyph.right_side_bearing
            aligned_to_pixel_grid = (width % design_px == 0)
            if (aligned_to_pixel_grid):
                shift = glyph.left_side_bearing % design_px
                glyph.left_side_bearing = glyph.left_side_bearing - shift
                glyph.right_side_bearing = glyph.right_side_bearing + shift

# Add valid space glyph to avoid "unknown character" box on IE11
glyph = font.createChar(32)
glyph.width = 200

for name, data in manifest['glyphs'].items():
    createGlyph(name, data)

#
# Generate Files
#

try:
    # Generate TTF
    fontfile = os.path.join(base_dir, options['output']['fonts'], options['font_name'] + '.ttf')
    font.generate(fontfile)
    # Generate glyphmaps
    if options['output']['glyphmaps']:
        mapsfile = os.path.join(base_dir, options['output']['glyphmaps'], options['font_name'] + '.json')
        with open(mapsfile, 'w') as f:
            json.dump(glyphmap, f, sort_keys=True, indent=2, separators=(',', ': '))

finally:
    manifestfile.close()
