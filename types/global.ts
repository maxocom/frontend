declare module '*.png' {
  import {ImageRequireSource} from 'react-native';
  const content: ImageRequireSource;
  export default content;
}
declare module '*.jpg' {
  import {ImageRequireSource} from 'react-native';
  const content: ImageRequireSource;
  export default content;
}
