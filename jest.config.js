module.exports = {
  preset: 'react-native',
  haste: {
    defaultPlatform: 'android',
    platforms: ['android', 'ios', 'native'],
  },
  resolver: '<rootDir>/testing/rnResolver.js',
  setupFiles: ['<rootDir>/testing/setup.js'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transformIgnorePatterns: ['node_modules/(?!(react-native.*|mobx.*|react-navigation|@react-native-community)/)'],
  globalSetup: '<rootDir>/testing/globalSetup.js',
  collectCoverageFrom: ['src/**/*.{ts,tsx}', '!src/**/*.spec.{ts,tsx}'],
  coverageReporters: ['text-summary', 'html', 'cobertura'],
};
