import {timers, install, FakeMethod} from '@sinonjs/fake-timers';

const clock = install({
  toFake: Object.keys(timers) as FakeMethod[],
});

export {clock};
