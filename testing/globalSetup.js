module.exports = function () {
  // Set TZ to some predifined value for predictable testing
  process.env.TZ = 'Europe/Moscow';
  return Promise.resolve();
};
