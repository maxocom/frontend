/* eslint-disable no-undef */
const pkg = require('../package.json');
// Use mockable promises
global.Promise = require('promise/setimmediate');
import mockRNPermissions from 'react-native-permissions/mock';

// Very simpe XMLHttpRequest
const xhrMock = {
  open: jest.fn(),
  setRequestHeader: jest.fn(),
  onreadystatechange: jest.fn(),
  send: jest.fn(),
  readyState: 4,
  responseText: JSON.stringify({}),
  status: 200,
};
global.XMLHttpRequest = jest.fn(() => xhrMock);

jest
  .mock('react-native/Libraries/Animated/src/Animated', () => {
    const Animated = jest.requireActual('react-native/Libraries/Animated/src/Animated');
    Animated.Text.__skipSetNativeProps_FOR_TESTS_ONLY = true;
    Animated.View.__skipSetNativeProps_FOR_TESTS_ONLY = true;
    return Animated;
  })
  .mock('react-native/Libraries/Animated/src/AnimatedImplementation', () => {
    const AnimatedImplementation = jest.requireActual('react-native/Libraries/Animated/src/AnimatedImplementation');
    const oldCreate = AnimatedImplementation.createAnimatedComponent;
    AnimatedImplementation.createAnimatedComponent = function (Component, defaultProps) {
      const Wrapped = oldCreate(Component, defaultProps);
      Wrapped.__skipSetNativeProps_FOR_TESTS_ONLY = true;
      return Wrapped;
    };
    return AnimatedImplementation;
  })
  .mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

jest.unmock('react-native/Libraries/AppState/AppState').mock('react-native/Libraries/AppState/NativeAppState', () => {
  return {
    getConstants: () => ({initialAppState: 'active'}),
    getCurrentAppState: jest.fn(),
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
});

const NativeModules = {
  PermissionsAndroid: {
    requestMultiplePermissions: jest.fn(() => Promise.resolve(true)),
  },
  RNGestureHandlerModule: {
    attachGestureHandler: jest.fn(),
    createGestureHandler: jest.fn(),
    dropGestureHandler: jest.fn(),
    updateGestureHandler: jest.fn(),
    State: {},
    Directions: {},
  },
  RNNBridgeModule: {
    setRoot: jest.fn(() => Promise.resolve()),
    setDefaultOptions: jest.fn(),
    mergeOptions: jest.fn(),
    push: jest.fn(() => Promise.resolve()), // (commandId: string, onComponentId: string, layout: object): Promise<any>;
    pop: jest.fn(() => Promise.resolve()), // (commandId: string, componentId: string, options?: object): Promise<any>;
    popTo: jest.fn(() => Promise.resolve()), // (commandId: string, componentId: string, options?: object): Promise<any>;
    popToRoot: jest.fn(() => Promise.resolve()), // (commandId: string, componentId: string, options?: object): Promise<any>;
    setStackRoot: jest.fn(() => Promise.resolve()), // (commandId: string, onComponentId: string, layout: object): Promise<any>;
    showModal: jest.fn(() => Promise.resolve()), // (commandId: string, layout: object): Promise<any>;
    dismissModal: jest.fn(() => Promise.resolve()), // (commandId: string, componentId: string, options?: object): Promise<any>;
    dismissAllModals: jest.fn(() => Promise.resolve()), // (commandId: string, options?: object): Promise<any>;
    showOverlay: jest.fn(() => Promise.resolve()), // (commandId: string, layout: object): Promise<any>;
    dismissOverlay: jest.fn(() => Promise.resolve()), // (commandId: string, componentId: string): Promise<any>;
    dismissAllOverlays: jest.fn(() => Promise.resolve()), // (commandId: string): Promise<any>;
    getLaunchArgs: jest.fn(() => Promise.resolve()), // (commandId: string): Promise<any>;
  },
  RNBranch: {
    ADD_TO_CART_EVENT: 'ADD_TO_CART_EVENT',
    ADD_TO_WISHLIST_EVENT: 'ADD_TO_WISHLIST_EVENT',
    PURCHASE_INITIATED_EVENT: 'PURCHASE_INITIATED_EVENT',
    PURCHASED_EVENT: 'PURCHASED_EVENT',
    REGISTER_VIEW_EVENT: 'REGISTER_VIEW_EVENT',
    SHARE_COMPLETED_EVENT: 'SHARE_COMPLETED_EVENT',
    SHARE_INITIATED_EVENT: 'SHARE_INITIATED_EVENT',
    redeemInitSessionResult: jest.fn(() => Promise.resolve()),
  },
};

import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

jest.mock('amplitude-js', () => {
  return {
    getInstance: jest.fn(() => ({
      init: jest.fn(),
      setUserProperties: jest.fn(),
      logEvent: jest.fn(),
    })),
  };
});

jest.mock('@react-native-community/geolocation', () => {
  return {
    addListener: jest.fn(),
    getCurrentPosition: jest.fn(),
    removeListeners: jest.fn(),
    requestAuthorization: jest.fn(),
    setConfiguration: jest.fn(),
    startObserving: jest.fn(),
    stopObserving: jest.fn(),
  };
});

jest.mock('react-native-permissions', () => {
  return mockRNPermissions;
});

jest.mock('react-native-localize', () => {
  return {
    getLocales: () => [{countryCode: 'RU', languageTag: 'ru-RU', languageCode: 'ru', isRTL: false}],
    getNumberFormatSettings: () => ({
      decimalSeparator: '.',
      groupingSeparator: ',',
    }),
    getCalendar: () => 'gregorian',
    getCountry: () => 'RU',
    getCurrencies: () => [],
    getTemperatureUnit: () => 'celsius',
    getTimeZone: () => 'Europe/Paris',
    uses24HourClock: () => true,
    usesMetricSystem: () => true,
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
  };
});

jest.mock('@react-native-firebase/messaging', () => {
  return {
    messaging: jest.fn(() => ({
      hasPermission: jest.fn(() => Promise.resolve(true)),
      subscribeToTopic: jest.fn(),
      unsubscribeFromTopic: jest.fn(),
      requestPermission: jest.fn(() => Promise.resolve(true)),
      getToken: jest.fn(() => Promise.resolve('myMockToken')),
    })),
    notifications: jest.fn(() => ({
      onNotification: jest.fn(),
      onNotificationDisplayed: jest.fn(),
    })),
  };
});

Object.keys(NativeModules).forEach((name) => {
  mockReactNativeModule(name, NativeModules[name]);
});

function mockReactNativeModule(name, shape) {
  const rnNativeModules = require('react-native/Libraries/BatchedBridge/NativeModules');
  rnNativeModules[name] = shape;
}
