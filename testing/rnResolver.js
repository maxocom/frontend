module.exports = (request, options) => {
  // Call the defaultResolver, so we leverage its cache, error handling, etc.
  return options.defaultResolver(request, {
    ...options,
    packageFilter: (pkg) => {
      let main = pkg.main;
      //      if (pkg.module) main = pkg.module;
      if (typeof pkg['react-native'] === 'string') main = pkg['react-native'];
      return {...pkg, main};
    },
  });
};
