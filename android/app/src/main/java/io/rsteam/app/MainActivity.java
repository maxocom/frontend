package io.rsteam.app;
import com.facebook.react.ReactActivity;
import android.os.Bundle;
import android.view.View;
import io.rsteam.app.modules.SplashScreenModule;

public class MainActivity extends ReactActivity {

  protected void onStart() {
    super.onStart();
  }

 @Override
  protected void onCreate(Bundle savedInstance) {
    setTheme(R.style.AppTheme);
    SplashScreenModule.show(this);
    super.onCreate(savedInstance);
  }

  @Override
  protected String getMainComponentName() {
   return "app";
  }


  @Override
  protected void onResume() {
    super.onResume();
    hideNavigationBar();
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if (hasFocus) {
      hideNavigationBar();
    }
  }

  private void hideNavigationBar() {
    getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
  }
}
