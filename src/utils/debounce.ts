export function debounce(func: (...args: any[]) => void, waitMilliseconds: number) {
  let timer;
  return function (context: any, ...args: any[]) {
    if (timer !== undefined) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => func.apply(context, args), waitMilliseconds);
  };
}
