import Moment from 'moment';

export const formatWorkTime = (time: string) => {
  return Moment(time, 'hh:mm a').unix();
};

export function decodeDate(data: string | number | undefined): number {
  try {
    if (typeof data === 'number') {
      return new Date(data).getTime();
    }
    const res = data ? Date.parse(data) : 0;
    return res;
  } catch {
    return 0;
  }
}
