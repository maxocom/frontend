export const papulate = (number: number) => {
  const cases = [2, 0, 1, 1, 1, 2];
  const values = ['one', 'many', 'other'];

  number = Math.abs(number);
  let c = number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5];
  return values[c];
};
