import {isEmpty} from './isEmpty';

export function isEqual(val1: any, val2: any) {
  if (isEmpty(val1) && isEmpty(val2)) return true;
  return val1 === val2;
}
