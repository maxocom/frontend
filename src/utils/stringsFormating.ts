export const cutLastWord = (description: string, range: number) => {
  const str = description.substring(0, range);
  const lastIndex = str.lastIndexOf(' ');

  return str.substring(0, lastIndex);
};
