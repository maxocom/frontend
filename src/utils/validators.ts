export function validatePhoneNumber(phone?: string) {
  if (!phone) return false;
  const reg = /^[0-9\-\+]{9,15}$/;
  return reg.test(phone);
}

export function validateShortPhoneNumber(phone?: string) {
  if (!phone) return false;
  const reg = /^[0-9\-\+]{5,15}$/;
  return reg.test(phone);
}

export function validateFirstName(first_name?: string) {
  if (!first_name) return false;
  const reg = /^[A-Za-zА-Яа-яЁё,. *'-]+$/;
  return reg.test(first_name);
}

export function validateLastName(last_name?: string) {
  if (!last_name) return false;
  const reg = /^[A-Za-zА-Яа-яЁё,. *'-]+$/;
  return reg.test(last_name);
}

export function validateEmail(email?: string) {
  if (!email) return false;
  const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return reg.test(email);
}

export function validateInstagram(website?: string) {
  if (!website) return false;
  const reg = /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/;
  return reg.test(website);
}
