import ImagePicker from 'react-native-image-crop-picker';
import {appProvider} from '../appProvider';

export type ImageSelectorResponse = {
  url: string;
  filename?: string;
  data?: string | null;
};

export const selectPicture = (needCrop?: boolean): Promise<ImageSelectorResponse> => {
  const dimentions = appProvider.application.model.userState.photoDimentions;
  return ImagePicker.openPicker({
    width: dimentions.maxWidth,
    height: dimentions.maxHeight,
    cropping: !!needCrop,
    multiple: false,
    mediaType: 'photo',
    smartAlbums: ['UserLibrary', 'PhotoStream', 'Panoramas'],
  }).then((image) => ({url: image.path, filename: image.filename}));
};
