import Moment from 'moment-timezone';

export function dateToString(date: number): string {
  return Moment.tz(date, 'UTC').tz('Europe/Moscow').format('D MMMM, YYYY');
}

export function cleanStringWithNumberVal(val: string) {
  if (val === undefined) return '';
  // it is possible to enter space, hiphen and comma symbols, according for RN numeric keyboard type
  // we must to clean these symbols
  return ('' + val)
    .replace(/[- ]/g, '')
    .replace(/\,/g, '.')
    .replace(/^([^\.]*\.)|\./g, '$1');
}

export function formatDistance(distance: number) {
  if (distance === 0) return '0 m';
  if (!distance || distance < 0) return '';
  if (distance < 2000) {
    return distance + ' m';
  }
  return (distance / 1000).toFixed(1) + ' km';
}
