import React from 'react';
import {NavigationContainerRef} from '@react-navigation/core';

import {IFs} from './storage/storageTypes';
import {AppScreens} from './constants/screens';
import {Trainings} from './models/trainings';
import {Teams} from './models/teams';
import {Top} from './models/top';
import {Statistics} from './models/statistics';
import {Events} from './models/events';
import {Profile} from "./models/profile";

export interface IConsole {
  error(message?: any, ...optionalParams: any[]): void;
  info(message?: any, ...optionalParams: any[]): void;
  log(message?: any, ...optionalParams: any[]): void;
  warn(message?: any, ...optionalParams: any[]): void;
}

export interface ILogger extends IConsole {
  info(message?: any, ...args: any[]): void;
  error(...args: any[]): void;
}

export type AppNavigationType = React.RefObject<NavigationContainerRef>;

export interface IApplicationConfig {
  console?: IConsole;
  fs: IFs;
  navigation: AppNavigationType;
  apiBase: string;
}

export type BackTo = {
  screen: AppScreens;
  params?: object;
};

export type TeamsListsConfig = {
  teams: Teams;
};

export type TrainingListsConfig = {
  trainings: Trainings;
};

export type TopListsConfig = {
  top: Top;
  teams: Teams;
};

export type StatisticsConfig = {
  statistics: Statistics;
  user: Profile;
};

export type CalendarConfig = {
  events: Events;
  user: Profile;
};

