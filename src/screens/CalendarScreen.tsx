import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, View, TouchableOpacity, Text, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import {LocaleConfig, Calendar} from 'react-native-calendars';
import Moment from 'moment-timezone';
import 'moment/min/locales';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {MainMenu} from '../components/MainMenu';
import {EventListItem} from '../components/EventListItem';
import Icon from '../elements/OwnIcons';
import {CalendarConfig} from '../types';
import {Profile} from "../models/profile";

Moment.locale('ru');

LocaleConfig.locales['ru'] = {
  monthNames: ['Январь','Февраль','Март','Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  monthNamesShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
  dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
  dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
  today: 'Сегодня'
};
LocaleConfig.defaultLocale = 'ru';

type Props = NavigationScreenProps<AppScreens.Calendar>

const CalendarScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: CalendarConfig = props.route.params?.cfg;
  const user:Profile = cfg.user;

  if (!cfg) return null;

  const renderContent = () => {
    return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          {renderCalendar()}
          {!!cfg.events.selectedDateEvents ? renderDateEvents() : null}
          <View style={styles.spacer} />
          { user.isVerified && renderUpcomingEvents()}
        </ScrollView>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  const renderCalendar = () => {

    const calendarTheme = {
      calendarBackground: 'rgba(48, 47, 47, 0)',
      indicatorColor: theme.bgSpinner,
      monthTextColor: user.isVerified ? theme.textColor : theme.textInactiveColor,
      textMonthFontFamily: theme.fontFamilyRegular,
      textMonthFontSize: theme.space16,
      textMonthFontWeight: '600',
      textSectionTitleColor: user.isVerified ? theme.textColor : theme.textInactiveColor,
      textDayHeaderFontFamily: theme.fontFamilyRegular,
      textDayHeaderFontSize: theme.space16,
      textDayHeaderFontWeight: 'normal',
    };

    return (
      <View>
        <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
          <Calendar
            current={cfg.events.maxDate}
            minDate={cfg.events.minDate}
            maxDate={cfg.events.maxDate}
            displayLoadingIndicator={cfg.events.loading}
            firstDay={1}
            renderArrow={(direction) => renderArrow(direction)}
            disableArrowLeft={!user.isVerified}
            disableArrowRight={!user.isVerified}
            dayComponent={({date, state, marking}) => renderDays(date, state, marking)}
            disableTouchEvent={true}
            theme={calendarTheme}
            markedDates={cfg.events.eventsDates}
            onMonthChange={(month) => onMonthChange(month)}
          />
        </View>
        <View style={styles.spacer} />
      </View>
    );
  };

  const onMonthChange = (month) => {
    const currentYear = Number(Moment.tz().format('YYYY'));
    const currentMonth = Number(Moment.tz().format('M'));
    const currentMonthMM = Moment.tz().format('MM');
    const currentDay = Moment.tz().format('DD');
    const lastDay = Moment(`${month.year}-${month.month}-01`, 'YYYY-M-DD').endOf('month').format('DD');
    const selectedMonth = Moment(`${month.year}-${month.month}-01`, 'YYYY-M-DD').format('MM');

    if (currentMonth === month.month && currentYear === month.year) {
      cfg.events.loadEvents(`${currentYear}-${currentMonthMM}-${currentDay}`, `${currentYear}-${currentMonthMM}-${lastDay}`);
    } else {
      cfg.events.loadEvents(`${month.year}-${selectedMonth}-01`, `${month.year}-${selectedMonth}-${lastDay}`);
    }
  };

  const onDayPress = (day) => {
    cfg.events.setDateEvents(day.dateString);
  };

  const renderArrow = (direction: string) => {
    const style = StyleSheet.flatten([
      styles.backArrCalendar,
      !user.isVerified && styles.backArrCalendarInactive,
    ]);

    const styleIcon = StyleSheet.flatten([
      styles.backArrCalendarIcon,
      !user.isVerified && styles.backArrCalendarIconInactive,
    ]);

    switch (direction) {
      case 'left':
        return (
          <View style={style}>
            <Icon name="backArr" size={16} style={styleIcon} />
          </View>
        )
        break;
      case 'right':
        return (
          <View style={style}>
            <Icon name="nextArr" size={16} style={styleIcon} />
          </View>
        )
        break;
      default:
        return null
        break;
    }
  };

  const renderDays = (date, state, marking) => {
    const selected = user.isVerified && marking &&  marking.selected;
    const style = [
      styles.calendarDayStyle,
      selected && styles.calendarDaySelectedStyle,
    ];

    const textStyle = [
      styles.calendarDayTextStyle,
      (state === 'disabled') && styles.calendarDayDisablesTextStyle,
      (state === 'today') && styles.calendarDayTodayTextStyle,
      !user.isVerified && styles.calendarDayTextStyleInactive,
    ];

    return (
      <TouchableOpacity
        onPress={() => onDayPress(date)}
        style={style}
        disabled={!selected}
      >
        <Text
          style={textStyle}
        >
          {date.day}
        </Text>
      </TouchableOpacity>
    )
  };

  const renderUpcomingEventsList = () => {
    const upcomingEventsList = cfg.events.upcomingEventsList;

    return upcomingEventsList.map((upcomingEvent) => <EventListItem event={upcomingEvent} key={`training-${upcomingEvent.id}`} />);
  };

  const renderDateEventsList = () => {
    const dateEventsList = cfg.events.dateEventsList;

    return dateEventsList.map((upcomingEvent) => <EventListItem event={upcomingEvent} key={`training-${upcomingEvent.id}`} />);
  };

  const renderUpcomingEvents = () => {
    if (!cfg.events.upcomingEventsList.length) return null;

    return (
      <View>
        <View style={styles.upcomingEventsWrapTitle}>
          <Text style={styles.upcomingEventsTitle}>Ближайшие мероприятия</Text>
        </View>
        <View style={styles.flex}>
          {renderUpcomingEventsList()}
        </View>
      </View>
    );
  };

  const renderDateEvents = () => {
    return (
      <View>
        <View style={styles.upcomingEventsWrapTitle}>
          <Text style={styles.upcomingEventsTitle}>Мероприятия за {Moment(cfg.events.selectedDateEvents).tz('Europe/Moscow').format('DD MMM')}</Text>
        </View>
        <View style={styles.flex}>
          {renderDateEventsList()}
        </View>
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Calendar} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Календарь'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    minHeight: theme.aligned(335),
    overflow: 'hidden',
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
  backArrCalendar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.aligned(40),
    height: theme.aligned(40),
    borderRadius: theme.aligned(8),
    borderColor: theme.borderCalendarArr,
    borderWidth: theme.aligned(1),
  },
  backArrCalendarInactive: {
    borderColor: theme.borderCalendarInactive,
  },
  backArrCalendarIcon: {
    color: theme.bgBtnOrange
  },
  backArrCalendarIconInactive: {
    color: theme.borderCalendarInactive
  },
  calendarDayStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.aligned(30),
    height: theme.aligned(30),
  },
  calendarDaySelectedStyle: {
    borderColor: theme.textLinkColor,
    borderWidth: theme.aligned(1),
    borderRadius: theme.aligned(15),
  },
  calendarDayTextStyle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  calendarDayDisablesTextStyle: {
    color: theme.textDisabledDateTextColor,
  },
  calendarDayTodayTextStyle: {
    color: theme.textLinkColor,
  },
  calendarDayTextStyleInactive: {
    color: theme.textInactiveColor,
  },
  upcomingEventsWrapTitle: {
    paddingLeft: theme.aligned(19),
    marginBottom: theme.aligned(15),
  },
  upcomingEventsTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontWeight: '600',
    fontSize: theme.fontSize20,
    lineHeight: theme.space26,
    color: theme.textColor,
  }
});

export default CalendarScreen;
