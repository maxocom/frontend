import React from 'react';
import {Keyboard, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';
import {observer} from 'mobx-react-lite';
import {expr} from 'mobx-utils';

import theme from '../styles/theme';
import {LoginController, LoginWizardStep} from '../models/loginController';
import {LoginFormStep} from '../components/auth/LoginFormStep';
import {ConfirmationCodeStep} from '../components/auth/ConfirmationCodeStep';

type RouteProps = {
  params: {
    ctrl: LoginController;
  };
};

export const LogInScreen: React.FC<{route: RouteProps}> = observer((props) => {
  const ctrl = props.route.params.ctrl;
  
  const content = expr(() => {
    switch (ctrl.step) {
      case LoginWizardStep.PhoneNumber:
        return <LoginFormStep ctrl={ctrl} />;
      case LoginWizardStep.ConfirmationCode:
        return <ConfirmationCodeStep ctrl={ctrl} />;
      default:
        return null;
    }
  });

  return (
    <KeyboardAvoidingView
      style={styles.parent}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      onTouchStart={Keyboard.dismiss}>
      {content}
    </KeyboardAvoidingView>
  );
});

const styles = StyleSheet.create({
  parent: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: theme.bgColor,
    width: theme.windowWidth,
    height: theme.windowHeight,
    padding: 0,
  },
});
