import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, ScrollView, Image, Linking} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import RenderHtml, { HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';
import {appProvider} from '../appProvider';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Button} from '../elements/Button';
import {LeaderStory} from '../models/leaderStories';
import {MainMenu} from '../components/MainMenu';
import Icon from "../elements/OwnIcons";

const customHTMLElementModels = {
  p: defaultHTMLElementModels.p.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
    },
    contentModel: HTMLContentModel.block
  })
};

const tagsStyles = {
  body: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
};

type Props = NavigationScreenProps<AppScreens.LeaderStory>

const LeaderStoryScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const leaderStory: LeaderStory = props.route.params?.leaderStory;

  useFocusEffect(
    React.useCallback(() => {
      leaderStory.loadLeaderStory();
    }, [leaderStory]),
  );

  const onPressBack = () => {
    if (props.route.params?.backTo) {
      app.navigate(props.route.params?.backTo, props.route.params.backToParams || {});
    } else {
      app.navigateToLeaderStories();
    }
  };

  if (!leaderStory) return null;

  const renderContent = () => {
    if (leaderStory.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <View style={styles.eventImageWrap}>
              {
                leaderStory.image ?
                  <Image source={{uri: leaderStory.image}} style={styles.eventImage} />
                :
                  <View style={styles.eventImageEmptyWrap}>
                    <Icon name="emptyPhoto" size={theme.aligned(102)} style={styles.eventImageEmptyIcon} />
                  </View>
              }
            </View>

            <View style={styles.eventDescWrap}>
              <RenderHtml
                source={{html: leaderStory.description}}
                customHTMLElementModels={customHTMLElementModels}
                tagsStyles={tagsStyles}
              />
            </View>
          </View>
        </ScrollView>
        <View style={styles.spacer} />
        {
          leaderStory.link ? (
            <View style={styles.buttonBlock}>
              <Button
                onPress={onGoToLink}
                text={'Youtube'}
              />
            </View>
          ) : null
        }
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const onGoToLink = () => {
    Linking.openURL(leaderStory.link);
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={`${leaderStory.firstName} ${leaderStory.lastName}`} left={{icon: 'backArr', onPress: onPressBack}} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    overflow: 'hidden',
  },
  eventImageWrap: {
    width: '100%',
    height: theme.aligned(247),
    marginBottom: theme.aligned(38),
    borderRadius: theme.aligned(8),
  },
  eventImageEmptyWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.eventAvatarBg,
    width: '100%',
    height: '100%',
    borderRadius: theme.aligned(8),
  },
  eventImageEmptyIcon: {
    color: theme.borderDDArea,
  },
  eventImage: {
    width: '100%',
    height: '100%',
    borderRadius: theme.aligned(8),
  },
  eventDescWrap: {
    padding: theme.aligned(19),
  },
  eventSpeakerWrap: {
    paddingVertical: theme.aligned(27),
    paddingHorizontal: theme.aligned(19),
  },
  eventSpeakerLine: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  eventSpeakerTitleWrap: {
    marginBottom: theme.aligned(11),
  },
  eventSpeakerTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor,
  },
  eventSpeakerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventSpeakerIcon: {
    color: theme.bgBtnOrange,
    marginRight: theme.aligned(14),
  },
  eventSpeakerText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  lineSpacer: {
    height: theme.aligned(24),
  },
  spacer: {
    height: theme.aligned(30),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default LeaderStoryScreen;
