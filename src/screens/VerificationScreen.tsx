import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, View} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {VerificationFormView} from '../components/VerificationFormView';
import {Button} from '../elements/Button';
import {TeamsListsConfig} from '../types';

type Props = NavigationScreenProps<AppScreens.Verification>;

export const VerificationScreen: React.FC<Props> = observer((props: Props) => {

  const form = props.route.params?.form;
  const cfg: TeamsListsConfig = props.route.params?.cfg;

  if (!form) return null;
  const renderContent = () => {
    if (form.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <VerificationFormView form={form} teams={cfg} />
          </View>
          <View style={styles.spacer} />
          <View style={styles.buttonBlock}>
            <Button
              onPress={form.onSave}
              text={'Отправить'}
              disabled={!form.hasChanges}
              loading={form.loading}
            />
          </View>
        </ScrollView>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Подтвердите партнёрство'} subtitle={'Анкета'} />
      <View style={styles.container}>
        {renderContent()}
      </View>
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingVertical: theme.aligned(34),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  spacer: {
    height: theme.aligned(30),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});
