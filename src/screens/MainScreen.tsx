import React from 'react';
import {observer} from 'mobx-react-lite';

import {NavigationScreenProps} from '../Routes';

import {ScrollView, StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import images from '../images';

import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {appProvider} from '../appProvider';
import {Screen} from '../elements/Screen';
import Icon from "../elements/OwnIcons";


type Props = NavigationScreenProps<AppScreens.Main>;

export const MainScreen: React.FC<Props> = observer(() => {

  const renderContent = () => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => appProvider.application.navigateToBusiness()}
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.pageBlock}>
            <View style={styles.pageImageWrap}>
              <Image source={images.mainBusiness} style={styles.pageImage} />
            </View>

            <View style={styles.pageContent}>
              <View style={styles.pageTitleWrap}>
                <Text style={styles.pageTitle}>Бизнес</Text>
              </View>
              <View style={styles.pageDescriptionWrap}>
                <Text style={styles.pageDescription}>Пошаговое обучение и готовые алгоритмы по получению предпринимательских навыков и проработке мышления</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.bottomSpacer} />
        <View
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.pageBlock}>
            <View style={styles.pageImageWrap}>
              <Image source={images.mainFinancial} style={styles.pageImage} />
            </View>

            <View style={styles.pageContent}>
              <View style={styles.pageTitleWrap}>
                <Icon style={styles.pageLockIcon} name={'lock'} size={theme.aligned(20)} />
                <Text style={styles.pageTitle}>Финансы</Text>
              </View>
              <View style={styles.pageDescriptionWrap}>
                <Text style={styles.pageDescription}>Преумножаем заработанное благодаря самым передовым инструментам на рынке инвестиций и криптовалюты</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.bottomSpacer} />
        <TouchableOpacity
          onPress={() => appProvider.application.navigate(AppScreens.Spirituality)}
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.pageBlock}>
            <View style={styles.pageImageWrap}>
              <Image source={images.mainSpirituality} style={styles.pageImage} />
            </View>

            <View style={styles.pageContent}>
              <View style={styles.pageTitleWrap}>
                <Text style={styles.pageTitle}>Духовность</Text>
              </View>
              <View style={styles.pageDescriptionWrap}>
                <Text style={styles.pageDescription}>Глубинные практики для проработки своего тела и сознания</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.bottomSpacer} />
        <View
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.pageBlock}>
            <View style={styles.pageImageWrap}>
              <Image source={images.mainTravel} style={styles.pageImage} />
            </View>

            <View style={styles.pageContent}>
              <View style={styles.pageTitleWrap}>
                <Icon style={styles.pageLockIcon} name={'lock'} size={theme.aligned(20)} />
                <Text style={styles.pageTitle}>Travel</Text>
              </View>
              <View style={styles.pageDescriptionWrap}>
                <Text style={styles.pageDescription}>Путешествия и тусовки по всему миру с сообществом</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Наши направления'} subtitle={'Авторизация'} />
      <View style={styles.container}>
        {renderContent()}
      </View>
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingLeft: theme.aligned(6),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: theme.aligned(150),
    overflow: 'hidden',
  },
  pageBlock: {
    alignSelf: 'stretch',
    position: 'relative',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  pageImageWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.bgColorVaryDarkGray,
    width: theme.aligned(108),
    height: theme.aligned(108),
    marginRight: theme.aligned(22),
    borderRadius: theme.aligned(6),
  },
  pageImage: {
    width: theme.aligned(54),
    height: theme.aligned(54),
  },
  pageContent: {
    justifyContent: 'flex-start',
    maxWidth: theme.aligned(233),
    flexGrow: 1,
  },
  pageTitleWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginBottom: theme.aligned(5),
    width: theme.aligned(213),
  },
  pageLockIcon: {
    color: theme.textColor,
    marginRight: theme.aligned(10),
  },
  pageTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize24,
    lineHeight: theme.space30,
    color: theme.textColor,
  },
  pageDescriptionWrap: {
    width: theme.aligned(213),
  },
  pageDescription: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.descColor,
  },
  bottomSpacer: {
    height: theme.aligned(8),
  },
});
