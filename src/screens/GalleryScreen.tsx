import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {appProvider} from '../appProvider';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {ContentSlider} from '../components/ContentSlider';
import {Gallery} from '../models/galleries';
import {MainMenu} from '../components/MainMenu';

type Props = NavigationScreenProps<AppScreens.Gallery>

const GalleryScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const gallery: Gallery = props.route.params?.gallery;

  useFocusEffect(
    React.useCallback(() => {
      gallery.loadGallery();
    }, [gallery]),
  );

  const onPressBack = () => {
    if (props.route.params?.backTo) {
      app.navigate(props.route.params?.backTo, props.route.params.backToParams || {});
    } else {
      app.navigateToGalleries();
    }
  };

  if (!gallery) return null;

  const renderContent = () => {
    if (gallery.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
          <ContentSlider content={gallery.contentList} />
        </View>

        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={gallery.name} left={{icon: 'backArr', onPress: onPressBack}} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    height: 'auto',
    overflow: 'hidden',
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default GalleryScreen;
