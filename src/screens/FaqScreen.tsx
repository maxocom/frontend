import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, FlatList} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Faq, FaqItem} from '../models/Faq';
import {MainMenu} from '../components/MainMenu';
import {FaqListItem} from '../components/FaqListItem';

type Props = NavigationScreenProps<AppScreens.Faq>

const FaqScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: Faq = props.route.params?.cfg;

  useFocusEffect(
    React.useCallback(() => {
      cfg.loadFaq();
    }, [cfg]),
  );

  if (!cfg) return null;

  const renderContent = () => {
    if (cfg.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <FlatList<FaqItem>
          showsVerticalScrollIndicator={false}
          data={cfg.faqList}
          keyExtractor={(item) => `faq-${item.id}`}
          renderItem={(data) => <FaqListItem faqItem={data.item} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'База знаний'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default FaqScreen;
