import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, FlatList} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Training} from '../models/training';
import {MainMenu} from '../components/MainMenu';
import {TrainingListItem} from '../components/TrainingListItem';
import {TrainingListsConfig} from '../types';

type Props = NavigationScreenProps<AppScreens.Trainings>

const TrainingsScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: TrainingListsConfig = props.route.params?.cfg;

  useFocusEffect(
    React.useCallback(() => {
      cfg.trainings.loadTrainings();
    }, [cfg]),
  );

  if (!cfg) return null;

  const renderContent = () => {
    if (cfg.trainings.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }


    return (
      <View style={styles.flex}>
        <FlatList<Training>
          showsVerticalScrollIndicator={false}
          data={cfg.trainings.trainingsList}
          keyExtractor={(item) => `training-${item.id}`}
          renderItem={(data) => <TrainingListItem training={data.item} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Training} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Обучение'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default TrainingsScreen;
