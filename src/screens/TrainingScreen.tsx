import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, FlatList} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {appProvider} from '../appProvider';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Training} from '../models/training';
import {Lesson} from '../models/lesson';
import {MainMenu} from '../components/MainMenu';
import {LessonListItem} from '../components/LessonListItem';

type Props = NavigationScreenProps<AppScreens.Training>

const TrainingScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const training: Training = props.route.params?.training;

  useFocusEffect(
    React.useCallback(() => {
      training.loadLessons();
    }, [training]),
  );

  const onPressBack = () => {
    if (props.route.params?.backTo) {
      app.navigate(props.route.params?.backTo, props.route.params.backToParams || {});
    } else {
      app.navigateToTrainings();
    }
  };

  if (!training) return null;

  const renderContent = () => {
    if (training.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }

    return (
      <View style={styles.flex}>
        <FlatList<Lesson>
          showsVerticalScrollIndicator={false}
          data={training.lessonsList}
          keyExtractor={(item) => `lesson-${item.id}`}
          renderItem={(data) => <LessonListItem lesson={data.item} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Training} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={training.name} left={{icon: 'backArr', onPress: onPressBack}} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default TrainingScreen;
