import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, View, Text} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {StatisticFormView} from '../components/StatisticFormView';
import {Button} from '../elements/Button';
import {MainMenu} from '../components/MainMenu';
import Icon from '../elements/OwnIcons';

type Props = NavigationScreenProps<AppScreens.SendStatistics>;

export const SendStatisticsScreen: React.FC<Props> = observer((props: Props) => {

  const form = props.route.params?.form;
  const profile = props.route.params?.profile;

  if (!form) return null;
  if (!profile) return null;

  const renderContent = () => {
    if (form.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }

    if (profile.statisticIsWriten) {
      return (
        <View style={styles.flex}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <View style={styles.sendedBlock}>
              <View style={styles.sendedBlockIconWrap}>
                <Icon name={'check'} size={77} style={styles.sendedBlockIcon} />
              </View>
              <View style={styles.sendedBlockTextWrap}>
                <Text style={styles.sendedBlockText}>Вы уже отправили результаты</Text>
              </View>
            </View>
          </View>
        </View>
      );
    }

    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.flex}
        keyboardVerticalOffset={theme.aligned(180)}
      >
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <StatisticFormView form={form} />
          </View>
          <View style={styles.spacer} />
        </ScrollView>
        <View style={styles.buttonBlock}>
          <Button
            onPress={form.onSave}
            text={'Сохранить'}
            disabled={!form.hasChanges}
            loading={form.loading}
          />
        </View>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Statistics} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Статистика'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingVertical: theme.aligned(34),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  spacer: {
    height: theme.aligned(30),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
  sendedBlock: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendedBlockIconWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: theme.aligned(160),
    height: theme.aligned(160),
    borderRadius: theme.aligned(80),
    backgroundColor: theme.bgCheck,
    marginBottom: theme.aligned(23),
  },
  sendedBlockIcon: {
    color: theme.bgBtnOrange
  },
  sendedBlockTextWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendedBlockText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize24,
    lineHeight: theme.space30,
    color: theme.textColor,
    textAlign: 'center'
  },
});
