import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, KeyboardAvoidingView, Platform, ScrollView, Image, Text, Linking} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import RenderHtml, { HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';
import {appProvider} from '../appProvider';
import Moment from 'moment-timezone';
import 'moment/min/locales';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Button} from '../elements/Button';
import {Event} from '../models/event';
import {MainMenu} from '../components/MainMenu';
import Icon from '../elements/OwnIcons';

Moment.locale('ru');

const customHTMLElementModels = {
  p: defaultHTMLElementModels.p.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
    },
    contentModel: HTMLContentModel.block
  })
};

const tagsStyles = {
  body: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
};

type Props = NavigationScreenProps<AppScreens.Event>

const EventScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const event: Event = props.route.params?.event;

  useFocusEffect(
    React.useCallback(() => {
      event.loadEvent();
    }, [event]),
  );

  const onPressBack = () => {
    if (props.route.params?.backTo) {
      app.navigate(props.route.params?.backTo, props.route.params.backToParams || {});
    } else {
      app.navigateToCalendar();
    }
  };

  if (!event) return null;
  console.log('event', event);

  const renderContent = () => {
    if (event.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <View style={styles.eventImageWrap}>
              {
                event.image ?
                  <Image source={{uri: event.image}} style={styles.eventImage} />
                :
                  <View style={styles.eventImageEmptyWrap}>
                    <Icon name="emptyPhoto" size={theme.aligned(102)} style={styles.eventImageEmptyIcon} />
                  </View>
              }
            </View>

            <View style={styles.eventDescWrap}>
              <RenderHtml
                source={{html: event.description}}
                customHTMLElementModels={customHTMLElementModels}
                tagsStyles={tagsStyles}
              />
            </View>
          </View>

          <View style={styles.spacer} />

          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <View style={styles.eventSpeakerWrap}>
              <View style={styles.eventSpeakerLine}>
                <View style={styles.eventSpeakerTitleWrap}>
                  <Text style={styles.eventSpeakerTitle}>Спикер</Text>
                </View>
                <View style={styles.eventSpeakerContent}>
                  <Icon name="user" size={22} style={styles.eventSpeakerIcon} />
                  <Text style={styles.eventSpeakerText}>{event.speaker}</Text>
                </View>
              </View>
              <View style={styles.lineSpacer} />
              <View style={styles.eventSpeakerLine}>
                <View style={styles.eventSpeakerTitleWrap}>
                  <Text style={styles.eventSpeakerTitle}>Когда</Text>
                </View>
                <View style={styles.eventSpeakerContent}>
                  <Icon name="clock" size={22} style={styles.eventSpeakerIcon} />
                  <Text style={styles.eventSpeakerText}>{Moment(event.time).tz('Europe/Moscow').format('DD MMM YYYY в HH:mm')}</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.spacer} />
        <View style={styles.buttonBlock}>
          {
            (!event.eventOutdated && !event.subscribeEvent) && (
              <Button
                onPress={onSubmitEvent}
                text={'Записаться'}
              />
            )
          }
          {
            (!event.eventOutdated && event.subscribeEvent) && (
              <Button
                onPress={() => {return null}}
                text={'Вы записаны на мероприятие'}
                disabled={true}
              />
            )
          }
          {
            (event.eventOutdated && !event.eventReady) && (
              <Button
                onPress={onSubmitEvent}
                text={'Мероприятие уже закончилось'}
                disabled={true}
              />
            )
          }
          {
            (event.eventOutdated && event.eventReady) && (
              <Button
                onPress={onGoToEvent}
                text={'Перейти на мероприятие'}
              />
            )
          }
        </View>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  const onSubmitEvent = () => {
    event.requestEvent();
  };

  const onGoToEvent = () => {
    Linking.openURL(event.link);
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Calendar} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={event.name} left={{icon: 'backArr', onPress: onPressBack}} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    overflow: 'hidden',
  },
  eventImageWrap: {
    width: '100%',
    height: theme.aligned(247),
    marginBottom: theme.aligned(38),
    borderRadius: theme.aligned(8),
  },
  eventImageEmptyWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.eventAvatarBg,
    width: '100%',
    height: '100%',
    borderRadius: theme.aligned(8),
  },
  eventImageEmptyIcon: {
    color: theme.borderDDArea,
  },
  eventImage: {
    width: theme.aligned(80),
    height: theme.aligned(80),
    borderRadius: theme.aligned(40),
  },
  eventDescWrap: {
    padding: theme.aligned(19),
  },
  eventSpeakerWrap: {
    paddingVertical: theme.aligned(27),
    paddingHorizontal: theme.aligned(19),
  },
  eventSpeakerLine: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  eventSpeakerTitleWrap: {
    marginBottom: theme.aligned(11),
  },
  eventSpeakerTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor,
  },
  eventSpeakerContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  eventSpeakerIcon: {
    color: theme.bgBtnOrange,
    marginRight: theme.aligned(14),
  },
  eventSpeakerText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  lineSpacer: {
    height: theme.aligned(24),
  },
  spacer: {
    height: theme.aligned(30),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default EventScreen;
