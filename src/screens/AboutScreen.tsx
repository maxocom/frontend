import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, ScrollView} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import RenderHtml, { HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {About} from '../models/about';
import {MainMenu} from '../components/MainMenu';

type Props = NavigationScreenProps<AppScreens.About>

const customHTMLElementModels = {
  p: defaultHTMLElementModels.p.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
      marginBottom: theme.aligned(18),
    },
    contentModel: HTMLContentModel.block
  }),
  img: defaultHTMLElementModels.img.extend({
    mixedUAStyles: {
      borderRadius: theme.aligned(8),
      overflow: 'hidden',
      width: '100%',
      marginBottom: theme.aligned(18),
    },
    contentModel: HTMLContentModel.block
  })
};

const tagsStyles = {
  body: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
};

const AboutScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: About = props.route.params?.cfg;

  useFocusEffect(
    React.useCallback(() => {
      cfg.loadAbout();
    }, [cfg]),
  );

  if (!cfg) return null;

  const renderContent = () => {
    if (cfg.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
            <View style={styles.textBlock}>
              <RenderHtml
                source={{html: cfg.text}}
                customHTMLElementModels={customHTMLElementModels}
                tagsStyles={tagsStyles}
              />
            </View>
          </View>
          <View style={styles.bottomSpacer} />
        </ScrollView>
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'О нас'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingTop: theme.aligned(20),
    paddingBottom: theme.aligned(2),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  textBlock: {
    alignSelf: 'stretch',
    width: '100%',
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default AboutScreen;
