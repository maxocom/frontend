import React from 'react';

import {View, StyleSheet, ActivityIndicator} from 'react-native';

import theme from '../styles/theme';

export class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.parent}>
        <View style={styles.activityIndicatorWrap}>
          <ActivityIndicator size="large" color="#FE5A57" style={styles.activityIndicator} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  parent: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: theme.bgColor,
    width: theme.windowWidth,
  },
  activityIndicatorWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: theme.windowHeight - theme.aligned(400),
  },
  activityIndicator: {
    flex: 4,
  },
});
