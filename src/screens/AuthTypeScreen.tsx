import React from 'react';
import {observer} from 'mobx-react-lite';

import {NavigationScreenProps} from '../Routes';

import {ScrollView, StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import images from '../images';

import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {appProvider} from '../appProvider';
import {Screen} from '../elements/Screen';


type Props = NavigationScreenProps<AppScreens.AuthType>;

export const AuthTypeScreen: React.FC<Props> = observer((props: Props) => {

  const onPressAuthType = (authType) => {
    appProvider.application.navigateToAuthTypeScreen(authType);
  };

  const renderContent = () => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => onPressAuthType('demo')}
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.authTypeBlock}>
            <View style={styles.authTypeBlockText}>
              <Text style={styles.authTypeTitle}>Демо-версия</Text>
              <Text style={styles.authTypeDesc}>Краткое описание возможностей демо-версии</Text>
            </View>
            <View style={styles.authTypeBlockImages}>
              <Image source={images.accountTypeBg} style={styles.accountTypeBg} />
              <Image source={images.accountTypeDemo} style={styles.accountTypeImage} />
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.bottomSpacer} />
        <TouchableOpacity onPress={() => onPressAuthType('verified')} style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
          <View style={styles.authTypeBlock}>
            <View style={styles.authTypeBlockText}>
              <Text style={styles.authTypeTitle}>Версия для партнеров RS</Text>
              <Text style={styles.authTypeDesc}>Краткое описание возможностей версии для партнеров</Text>
            </View>
            <View style={styles.authTypeBlockImages}>
              <Image source={images.accountTypeBg} style={styles.accountTypeBg} />
              <Image source={images.accountTypePartners} style={styles.accountTypeImage} />
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Выбери тип авторизации'} subtitle={'Авторизация'} />
      <View style={styles.container}>
        {renderContent()}
      </View>
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingLeft: theme.aligned(26),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: theme.aligned(192),
    overflow: 'hidden',
  },
  authTypeBlock: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  authTypeBlockText: {
    width: theme.aligned(200),
  },
  authTypeBlockImages: {

  },
  authTypeTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize18,
    lineHeight: theme.space24,
    color: theme.textColor,
    marginBottom: theme.aligned(16),
    width: theme.aligned(200),
  },
  authTypeDesc: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space22,
    color: theme.descColor,
    width: theme.aligned(200),
  },
  accountTypeBg: {
    height: theme.aligned(192),
    width: undefined,
    resizeMode: 'contain',
    aspectRatio: 1,
  },
  accountTypeImage: {
    position: 'absolute',
    top: 8,
    height: theme.aligned(176),
    width: undefined,
    resizeMode: 'contain',
    aspectRatio: 1,
  },
  bottomSpacer: {
    height: theme.aligned(32),
  },
});
