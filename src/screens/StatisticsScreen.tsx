import React, {useState, useCallback} from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, KeyboardAvoidingView, ScrollView, Platform, TouchableOpacity, Text, processColor} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import Moment from 'moment-timezone';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {LineChart} from 'react-native-charts-wrapper';
import {appProvider} from '../appProvider';

import theme, {withShadow} from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Button} from '../elements/Button';
import {MainMenu} from '../components/MainMenu';
import {StatisticsConfig} from '../types';
import {TimeIntervals, TimeIntervalsValues, TimeIntervalsName, TimeIntervalsMoment} from '../models/statistics';
import {Profile} from "../models/profile";

type Props = NavigationScreenProps<AppScreens.Statistics>

const StatisticsScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const cfg: StatisticsConfig = props.route.params?.cfg;
  const user:Profile = cfg.user;

  const [isDatePickerVisible, setDatePickerVisibility] = useState<boolean>(false);
  const [dateRow, setDateRow] = useState<string>('');
  const [date, setDate] = useState<any>(Moment.tz());

  useFocusEffect(
    useCallback(() => {
      cfg.statistics.loadStatistics();
    }, [cfg]),
  );

  const showDatePicker = (newDate:any, row:string) => {
    setDate(newDate);
    setDateRow(row);
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const onChangeDate = (date) => {
    if (dateRow === 'timeStart') {
      cfg.statistics.setTimeInterval(Moment(date), cfg.statistics.timeEnd, TimeIntervals.CUSTOM);
    } else {
      cfg.statistics.setTimeInterval(cfg.statistics.timeStart, Moment(date), TimeIntervals.CUSTOM);
    }

    setDatePickerVisibility(false);
  };

  const onSubmitStatistics = () => {
    app.navigateToStatistic();
  };

  if (!cfg) return null;

  const renderIntervals = () => {
    if (!cfg.statistics.hasData) return null;

    return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <View style={withShadow(styles.intervalsContent, {x: 0, y: 0, r: 40, o: 1})}>
          {renderIntervalsBtn(TimeIntervals.ONE_WEEK, true)}
          {renderIntervalsBtn(TimeIntervals.ONE_MONTH, true)}
          {renderIntervalsBtn(TimeIntervals.THREE_MONTH, true)}
        </View>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  const renderIntervalsBtn = (intervalName, last?) => {
    const style = StyleSheet.flatten([
      styles.intervaBtn,
      (cfg.statistics.timeInterval === TimeIntervalsValues[intervalName]) ? withShadow(styles.intervaBtnActive, {x: 0, y: 0, r: 40, o: 1}) : null,
      last ? styles.intervaBtnLast : null
    ]);

    const timeStart: any = Moment.tz().subtract(TimeIntervalsMoment[intervalName].count, `${TimeIntervalsMoment[intervalName].value}`);
    const timeEnd: any = Moment.tz();

    return (
      <TouchableOpacity
        onPress={() => cfg.statistics.setTimeInterval(timeStart, timeEnd, intervalName)}
        style={style}
        disabled={cfg.statistics.loading}
      >
        <Text maxFontSizeMultiplier={1.5} style={styles.intervaBtnText}>{TimeIntervalsName[intervalName]}</Text>
      </TouchableOpacity>
    );
  };

  const renderContent = () => {
    return (
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.flex}>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>

            <View style={styles.statisticsResultContainer}>

              {
                cfg.statistics.hasData && (
                  <View style={styles.statisticsChartTitleWrap}>
                    <Text maxFontSizeMultiplier={1.5} style={styles.statisticsChartTitle}>Период</Text>
                  </View>
                )
              }

              {renderDates()}
              {renderContentData()}
            </View>

          </View>

        </ScrollView>
        <View style={styles.spacer} />
        <View style={styles.buttonBlock}>
          <Button
            onPress={onSubmitStatistics}
            text={'Подать статистику'}
            disabled={!user.isVerified}
          />
        </View>
        <View style={styles.bottomSpacer} />
      </KeyboardAvoidingView>
    );
  };

  const renderContentData = () => {
    if (cfg.statistics.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }

    return (
      <View style={styles.statisticsChartWrap}>
        {renderChart()}
        {renderResult()}
      </View>
    );
  };

  const renderDates = () => {
    if (!cfg.statistics.hasData) return null;

    return (
      <View style={styles.statisticsChartDatesWrap}>
        <View style={styles.statisticsChartDateWrap}>
          <TouchableOpacity
            disabled={cfg.statistics.loading}
            onPress={() => showDatePicker(cfg.statistics.timeStart, 'timeStart')}
            style={styles.statisticsChartDate}
          >
            <Text maxFontSizeMultiplier={1.5} style={styles.statisticsChartDateText}>{ cfg.statistics.timeStart.format('DD/MM/YYYY') }</Text>
          </TouchableOpacity>
        </View>
        <Text maxFontSizeMultiplier={1.5} style={styles.statisticsChartDateSeparator}>—</Text>
        <View style={styles.statisticsChartDateWrap}>
          <TouchableOpacity
            disabled={cfg.statistics.loading}
            onPress={() => showDatePicker(cfg.statistics.timeEnd, 'timeEnd')}
            style={styles.statisticsChartDate}
          >
            <Text maxFontSizeMultiplier={1.5} style={styles.statisticsChartDateText}>{ cfg.statistics.timeEnd.format('DD/MM/YYYY') }</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const renderChart = () => {
    if (!cfg.statistics.hasData) return null;

    const chartData: any = Object.keys(cfg.statistics.chartValuesList).map((itemName) => {
      return {
        values: cfg.statistics.chartValuesList[itemName],
        label: '',
        config: {
          mode: 'CUBIC_BEZIER',
          drawValues: false,
          lineWidth: 2,
          drawCircles: true,
          circleColor: processColor(theme[itemName]),
          drawCircleHole: false,
          circleRadius: 5,
          highlightColor: processColor('transparent'),
          color: processColor(theme[itemName]),
          // drawFilled: true,
          // fillGradient: {
          //   colors: [processColor(theme[itemName])],
          //   positions: [0, 0.5],
          //   angle: 90,
          //   orientation: 'TOP_BOTTOM',
          // },
          fillAlpha: 1000,
          valueTextSize: 15,
        }
      }
    });

    return (
      <LineChart
        style={styles.statisticsChart}
        data={{dataSets: chartData}}
        legend={
          {
            enabled: true,
            textColor: processColor(theme.textColor),
            textSize: theme.aligned(14),
            form: 'SQUARE',
            formSize: theme.aligned(16),
            xEntrySpace: 10,
            yEntrySpace: 5,
            formToTextSpace: 5,
            wordWrapEnabled: true,
            maxSizePercent: 0.5,
            custom: {
              colors: [processColor(theme.postedVideoChart), processColor(theme.numberOfMeetingsChart), processColor(theme.numberOfTouchesChart)],
              labels: ['Продано видео', 'Проведенные встречи', 'Количество касаний']
            }
          }
        }
        marker={
          {
            enabled: true,
            markerColor: processColor(theme.bgBtnOrange),
            textColor: processColor(theme.textColor),
            textSize: theme.aligned(14),
          }
        }
        xAxis={
          {
            enabled: true,
            granularity: 1,
            drawLabels: true,
            position: 'BOTTOM',
            drawAxisLine: true,
            drawGridLines: false,
            textSize: theme.aligned(14),
            textColor: processColor(theme.textLabelColor),
            valueFormatter: ['', ...cfg.statistics.chartDatesList],
          }
        }
        yAxis={
          {
            left: {
              enabled: true,
              textColor: processColor(theme.textLabelColor),
              drawGridLines: true,
              gridLineWidth: 1,
              drawAxisLine: false,
              drawLabels: true,
              yOffset: -5,
              textSize: theme.aligned(14),
            },
            right: {
              enabled: false,
            },
          }
        }
        autoScaleMinMaxEnabled={true}
        animation={{
          durationX: 0,
          durationY: 1500,
          easingY: 'EaseInOutQuart',
        }}
        drawGridBackground={false}
        drawBorders={false}
        touchEnabled={true}
        dragEnabled={false}
        scaleEnabled={false}
        scaleXEnabled={false}
        scaleYEnabled={false}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        dragDecelerationEnabled={true}
        dragDecelerationFrictionCoef={0.99}
        keepPositionOnRotation={false}
      />
    );
  };

  const renderResult = () => {
    return (
      <View style={styles.statisticsResultWrap}>
        <View style={styles.statisticsResultStr}>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResultTitle}>Продано видео</Text>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResulValue}>{cfg.statistics.postedVideo}</Text>
        </View>
        <View style={styles.statisticsResultStr}>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResultTitle}>Проведенные встречи</Text>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResulValue}>{cfg.statistics.numberOfMeetings}</Text>
        </View>
        <View style={styles.statisticsResultStr}>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResultTitle}>Количество касаний</Text>
          <Text maxFontSizeMultiplier={1.5} style={styles.statisticsResulValue}>{cfg.statistics.numberOfTouches}</Text>
        </View>
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Statistics} />
      </View>
    );
  };

  const renderDateTimePicker = () => {
    return (
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        locale="ru_RU"
        cancelTextIOS="Закрыть"
        confirmTextIOS="Выбрать"
        onConfirm={onChangeDate}
        onCancel={hideDatePicker}
        date={new Date(date)}
      />
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Статистика'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderIntervals()}
        {renderContent()}
      </View>
      {renderMainMenu()}
      {renderDateTimePicker()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  intervalsContent: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: theme.aligned(47),
  },
  intervaBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: theme.borderStatisticsIntervalsBtn,
    borderRightWidth: 1,
    height: theme.aligned(47),
  },
  intervaBtnActive: {
    borderRightWidth: 0,
    borderRadius: theme.aligned(8),
    backgroundColor: theme.bgBtnOrange,
  },
  intervaBtnLast: {
    borderRightWidth: 0,
  },
  intervaBtnText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: theme.aligned(215),
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingVertical: theme.aligned(25),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  scroll: {
    alignSelf: 'stretch'
  },
  statisticsResultContainer: {
    width: '100%',
  },
  statisticsResultWrap: {
    flex: 1
  },
  statisticsResultStr: {
    flex: 1,
    marginTop: 20,
  },
  statisticsResultTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLabelColor
  },
  statisticsResulValue: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor
  },
  statisticsChartWrap: {
    flex: 1
  },
  statisticsChartTitleWrap: {
    flex: 1,
    marginBottom: theme.aligned(12),
  },
  statisticsChartTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor
  },
  statisticsChartDatesWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  statisticsChartDateWrap: {
    height: theme.aligned(56),
    flexGrow: 1,
    marginBottom: theme.aligned(30),
  },
  statisticsChartDate: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.bgColorLightGray,
    borderRadius: theme.aligned(8),
    height: theme.aligned(56),
  },
  statisticsChartDateText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLabelColor
  },
  statisticsChartDateSeparator: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLabelColor,
    marginHorizontal: theme.aligned(12),
    height: theme.aligned(56),
  },
  statisticsChart: {
    height: theme.aligned(300),
    marginBottom: theme.aligned(10),
  },
  spacer: {
    height: theme.aligned(30),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default StatisticsScreen;
