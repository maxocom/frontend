import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, View, FlatList} from 'react-native';
import {appProvider} from '../appProvider';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {Lesson} from '../models/lesson';
import {MainMenu} from '../components/MainMenu';
import {LessonListItem} from '../components/LessonListItem';

type Props = NavigationScreenProps<AppScreens.Training>

const SpiritualityScreen: React.FC<Props> = observer((props: Props) => {
  const app = appProvider.application;
  const lessonsList: any = [
    {
      description: null,
      id: 1,
      loading: false,
      name: 'Аффирмация на богатство и изобилие',
      seen_by_me: true,
      video: "Pip2CSmDwf4",
    },
    {
      description: null,
      id: 2,
      loading: false,
      name: 'Медитация после спорта или йоги',
      seen_by_me: true,
      video: "2Abtw0rocWA",
    },
    {
      description: null,
      id: 3,
      loading: false,
      name: 'Вечерняя медитация',
      seen_by_me: true,
      video: "NRtzdPBHxuI",
    },
    {
      description: null,
      id: 4,
      loading: false,
      name: 'Утренняя медитация',
      seen_by_me: true,
      video: "0wDsPv_GQBs",
    },
    {
      description: null,
      id: 5,
      loading: false,
      name: 'Утренняя дыхательная практика',
      seen_by_me: true,
      video: "zASySFfDaw4",
    },
    {
      description: null,
      id: 6,
      loading: false,
      name: 'Медитация для концентрации на день',
      seen_by_me: true,
      video: "g-rg889JhTE",
    },
    {
      description: null,
      id: 7,
      loading: false,
      name: 'Практика благодарности',
      seen_by_me: true,
      video: "FOr4eJX5g9w",
    },
    {
      description: null,
      id: 8,
      loading: false,
      name: 'Привлечение счастливых событий',
      seen_by_me: true,
      video: "_qfhAFfLTmE",
    },
    {
      description: null,
      id: 9,
      loading: false,
      name: 'Исцеляющая медитация',
      seen_by_me: true,
      video: "-ZQiZbmISEo",
    }
  ];

  const onPressBack = () => {
    if (props.route.params?.backTo) {
      app.navigate(props.route.params?.backTo, props.route.params.backToParams || {});
    } else {
      app.navigate(AppScreens.Main);
    }
  };

  const renderContent = () => {
    return (
      <View style={styles.flex}>
        <FlatList<Lesson>
          showsVerticalScrollIndicator={false}
          data={lessonsList}
          keyExtractor={(item) => `lesson-${item.id}`}
          renderItem={(data) => <LessonListItem lesson={data.item} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Training} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Духовность'} left={{icon: 'backArr', onPress: onPressBack}} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default SpiritualityScreen;
