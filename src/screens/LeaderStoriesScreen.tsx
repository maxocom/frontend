import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, FlatList} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {LeaderStories, LeaderStory} from '../models/leaderStories';
import {MainMenu} from '../components/MainMenu';
import {LeaderStoriesListItem} from '../components/LeaderStoriesListItem';

type Props = NavigationScreenProps<AppScreens.LeaderStories>

const LeaderStoriesScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: LeaderStories = props.route.params?.cfg;

  useFocusEffect(
    React.useCallback(() => {
      cfg.loadLeaderStories();
    }, [cfg]),
  );

  if (!cfg) return null;

  const renderContent = () => {
    if (cfg.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <FlatList<LeaderStory>
          showsVerticalScrollIndicator={false}
          data={cfg.leaderStoriesList}
          keyExtractor={(item) => `leaderStories-${item.id}`}
          renderItem={(data) => <LeaderStoriesListItem leaderStory={data.item} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Истории лидеров'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default LeaderStoriesScreen;
