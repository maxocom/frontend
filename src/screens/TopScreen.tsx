import React from 'react';
import {observer} from 'mobx-react-lite';
import {ActivityIndicator, StyleSheet, View, FlatList} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

import theme from '../styles/theme';
import {NavigationScreenProps} from '../Routes';
import {AppScreens} from '../constants/screens';
import {ScreenHeader} from '../elements/ScreenHeader';
import {Screen} from '../elements/Screen';
import {TopUser} from '../models/topUser';
import {MainMenu} from '../components/MainMenu';
import {TopListItem} from '../components/TopListItem';
import {TopListsConfig} from '../types';

type Props = NavigationScreenProps<AppScreens.Top>

const TopScreen: React.FC<Props> = observer((props: Props) => {
  const cfg: TopListsConfig = props.route.params?.cfg;

  useFocusEffect(
    React.useCallback(() => {
      cfg.top.loadTop();
    }, [cfg]),
  );

  if (!cfg) return null;

  const renderContent = () => {
    if (cfg.top.loading) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <FlatList<TopUser>
          showsVerticalScrollIndicator={false}
          data={cfg.top.topList}
          keyExtractor={(item) => `top-${item.id}`}
          renderItem={(data) => <TopListItem top={data.item} teams={cfg.teams.teamList} index={data.index} />}
        />
        <View style={styles.bottomSpacer} />
      </View>
    );
  };

  const renderMainMenu = () => {
    return (
      <View>
        <MainMenu activeScreen={AppScreens.Top} />
      </View>
    );
  };

  return (
    <Screen>
      <ScreenHeader title={'Новости'} right={{icon: 'contextMenu'}} />
      <View style={styles.container}>
        {renderContent()}
      </View>
      {renderMainMenu()}
    </Screen>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    backgroundColor: theme.bgColor,
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  spacer: {
    height: theme.aligned(30),
  },
  bottomSpacer: {
    height: theme.aligned(30),
  },
});

export default TopScreen;
