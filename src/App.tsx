import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {NativeModules, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';
import {observer} from 'mobx-react-lite';

import {processNotification} from './services/notification.service';
import Routes from './Routes';
import {appProvider} from './appProvider';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import theme from './styles/theme';

const App: React.FC = observer(() => {
  const app = appProvider.application;

  useEffect(() => {
    if (Platform.OS === 'android') {
      NativeModules.SplashScreenModule.hide();
    }
    return () => {
      app.model.appCtrl?.destroy();
    };
  }, [app.model.appCtrl]);

  useEffect(() => {
    messaging().onNotificationOpenedApp((remoteMessage) => {
      processNotification(remoteMessage).catch((e) => {
        console.error('Can not process notification', e);
      });
    });
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        app.model.setInitialMessage(remoteMessage ? remoteMessage : null);
      });
  }, []);

  const safeAreaStyle = {backgroundColor: theme.bgColor};

  return (
    <SafeAreaProvider style={safeAreaStyle}>
      <NavigationContainer
        ref={appProvider.application.navigationRef}
        onReady={() => {
          appProvider.application.model.setNavigationReady();
        }}>
        <Routes />
      </NavigationContainer>
    </SafeAreaProvider>
  );
});

export default App;
