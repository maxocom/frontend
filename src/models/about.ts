import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import {API_DOMAIN_URL} from '../constants/general';

export class About {
  text: string = '';
  loading: boolean = false;

  constructor(data?: api.About) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.About) {
    if (!data) return;

    const text = data.text ? data.text.replace(/src="/i, `src="${API_DOMAIN_URL}`) : '';
    this.text = text;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadAbout() {
    this.clear();
    const app = appProvider.application;

    app.getAbout(this);
  }

  clear() {
    this.text = '';
    this.loading = false;
  }

  serialize(): any {
    return {
      text: this.text,
    };
  }
}
