import {makeAutoObservable} from 'mobx';
import {Alert} from 'react-native';

import {Statistic} from './statistic';
import {isEqual} from '../utils/isEqual';
import {IFormField, ProxyField, ProxyFieldValidator} from './formField';
import {appProvider} from '../appProvider';
import {ApiError} from '../api/error';

export class SendStatisticsForm {
  statistic: Statistic = appProvider.application.model.statistic.clone();
  postedVideoField: IFormField;
  numberOfMeetingsField: IFormField;
  numberOfTouchesField: IFormField;
  loading: boolean = false;

  constructor() {
    makeAutoObservable(this);
    this.postedVideoField = new ProxyField({
      getter: () => this.statistic.postedVideo,
      setter: (val: number) => this.statistic.setPostedVideo(val),
      validator: this.validatePostedVideo,
    });
    this.numberOfMeetingsField = new ProxyField({
      getter: () => this.statistic.numberOfMeetings,
      setter: (val: number) => this.statistic.setNumberOfMeetings(val),
      validator: this.validateNumberOfMeetings,
    });
    this.numberOfTouchesField = new ProxyField({
      getter: () => this.statistic.numberOfTouches,
      setter: (val: number) => this.statistic.setNumberOfTouches(val),
      validator: this.validateNumberOfTouches,
    });
  }

  get isValid() {
    return (
      this.postedVideoField.isValid &&
      this.numberOfMeetingsField.isValid &&
      this.numberOfTouchesField.isValid
    );
  }

  get hasChanges() {
    const s = appProvider.application.model.statistic;
    if (!isEqual(s.postedVideo, this.statistic.postedVideo)) return true;
    if (!isEqual(s.numberOfMeetings, this.statistic.numberOfMeetings)) return true;
    if (!isEqual(s.numberOfTouches, this.statistic.numberOfTouches)) return true;
    return false;
  }

  markIsDirty() {
    this.postedVideoField.markAsDirty();
    this.numberOfMeetingsField.markAsDirty();
    this.numberOfTouchesField.markAsDirty();
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  onSave = () => {
    if (!this.isValid) {
      this.markIsDirty();
      return;
    }

    this.setLoading(true);
    console.log('statistic.export', this.statistic.export())
    this.sendStatistic().finally(() => {
      this.setLoading(false);
      this.showToolTip();
      appProvider.application.getProfile();
    });
  };

  sendStatistic(): Promise<void> {
    return appProvider.application
      .sendStatistic(this.statistic.export())
      .catch((e: ApiError) => {
        throw e;
      });
  }

  showToolTip = () => {
    Alert.alert(
      `${this.statistic.postedVideo < 3 ? 'Мы можем лучше' : 'Отличные результаты!'}`,
      `${this.statistic.postedVideo < 3 ? 'Сосредоточься на процессе!' : 'Так держать!'}`,
      [
        {text: 'Закрыть', onPress: () => appProvider.application.navigateToStatistics()},
      ],
      {cancelable: false},
    );
  };

  validatePostedVideo: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  validateNumberOfMeetings: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  validateNumberOfTouches: ProxyFieldValidator = (val: string) => {
    return undefined;
  };
}
