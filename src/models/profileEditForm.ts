import {makeAutoObservable} from 'mobx';

import {Profile} from './profile';
import {isEqual} from '../utils/isEqual';
import {IFormField, ProxyField, ProxyFieldValidator} from './formField';
import {validateEmail, validateInstagram} from '../utils/validators';
import {appProvider} from '../appProvider';
import * as api from '../api';
// import {AppScreens} from '../constants/screens';
import {ApiError} from '../api/error';

export const MAX_LENGTH = 20;
export const MAX_DESCRIPTION_LENGTH = 300;

type UserApiField = keyof api.User4Export;
const USER_API_ERROR_KEYS: UserApiField[] = ['email'];

export class ProfileEditForm {
  profile: Profile = appProvider.application.model.profile.clone();
  firstNameField: IFormField;
  lastNameField: IFormField;
  avatarField: IFormField;
  instagramField: IFormField;
  emailField: IFormField;
  phoneField: IFormField;
  loading: boolean = false;

  constructor() {
    makeAutoObservable(this);
    this.firstNameField = new ProxyField({
      getter: () => this.profile.firstName,
      setter: (val: string) => this.profile.setFirstName(val),
      validator: this.validateFirstName,
    });
    this.lastNameField = new ProxyField({
      getter: () => this.profile.lastName,
      setter: (val: string) => this.profile.setLastName(val),
      validator: this.validateLastName,
    });
    this.avatarField = new ProxyField({
      getter: () => this.profile.avatar,
      setter: (val: string) => this.profile.setAvatar(val),
      validator: this.validateAvatar,
    });
    this.instagramField = new ProxyField({
      getter: () => this.profile.instagramName || '',
      setter: (val: string) => this.profile.setInstagramName(val),
      validator: this.validateInstagram,
    });
    this.emailField = new ProxyField({
      getter: () => this.profile.email || '',
      setter: (val: string) => this.profile.setEmail(val),
      validator: this.validateEmail,
    });
    this.phoneField = new ProxyField({
      getter: () => this.profile.phone,
      setter: (val: string) => this.profile.setPhone(val),
      validator: this.validatePhone,
    });
  }

  get isValid() {
    return (
      this.firstNameField.isValid &&
      this.lastNameField.isValid &&
      this.avatarField.isValid &&
      this.instagramField.isValid &&
      this.emailField.isValid &&
      this.phoneField.isValid
    );
  }

  get hasChanges() {
    const p = appProvider.application.model.profile;
    if (!isEqual(p.firstName, this.profile.firstName)) return true;
    if (!isEqual(p.lastName, this.profile.lastName)) return true;
    if (!isEqual(p.avatar, this.profile.avatar)) return true;
    if (!isEqual(p.instagramName, this.profile.instagramName)) return true;
    if (!isEqual(p.email, this.profile.email)) return true;
    if (!isEqual(p.phone, this.profile.phone)) return true;
    return false;
  }

  markIsDirty() {
    this.firstNameField.markAsDirty();
    this.lastNameField.markAsDirty();
    this.avatarField.markAsDirty();
    this.instagramField.markAsDirty();
    this.emailField.markAsDirty();
    this.phoneField.markAsDirty();
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  onSave = () => {
    if (!this.isValid) {
      this.markIsDirty();
      return;
    }
    this.profile.setFirstName(this.profile.firstName.trim());
    this.profile.setLastName(this.profile.lastName.trim());
    this.setLoading(true);

    const oldAvatar = appProvider.application.model.profile.avatar;

    if (!isEqual(oldAvatar, this.profile.avatar)) {
      this.uploadFile().then(() => {
        this.updateUser().finally(() => {
          this.setLoading(false);
        });
      });
    } else {
      this.updateUser().finally(() => {
        this.setLoading(false);
      });
    }
  };

  updateUser(): Promise<void> {
    return appProvider.application
      .updateUser(this.profile.export())
      .catch((e: ApiError) => {
        this.handleUserError(e);
        throw e;
      });
  }

  uploadFile(): Promise<void> {
    return appProvider.application
      .uploadFile(this.profile.avatarExport())
      .then((mediaFile: api.MediaFile) => {
        this.profile.setAvatar(mediaFile.url)
      })
      .catch((e: ApiError) => {
        throw e;
      });
  }

  validateFirstName: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  validateLastName: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  validateAvatar: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  validateInstagram: ProxyFieldValidator = (val: string) => {
    if (val && !validateInstagram(val)) return 'Никнейм в Instagram обязателен для заполнения';
    return undefined;
  };

  validateEmail: ProxyFieldValidator = (val: string) => {
    if (val && !validateEmail(val)) return 'Email обязателен для заполнения';
    return undefined;
  };

  validatePhone: ProxyFieldValidator = (val: string) => {
    return undefined;
  };

  handleErrorDetails(e: ApiError): boolean {
    const app = appProvider.application;
    if (typeof e.details !== 'object') {
      app.alertError('', e);
      return true;
    }
    return false;
  }

  handleUserError = (e: ApiError) => {
    if (this.handleErrorDetails(e)) return;
    let errorProcessed = false;
    for (const errorKey of USER_API_ERROR_KEYS) {
      if (e.details && e.details[errorKey]) {
        this.processUserApiErrors(errorKey, e.details[errorKey]);
        errorProcessed = true;
      }
    }
    if (!errorProcessed) {
      appProvider.application.alertError('', e);
    }
  };

  processUserApiErrors(errorKey: UserApiField, message) {
    switch (errorKey) {
      case 'first_name':
        this.firstNameField.setError(message);
        break;
      case 'last_name':
        this.firstNameField.setError(message);
        break;
      case 'email':
        this.emailField.setError(message);
        break;
    }
  }
}
