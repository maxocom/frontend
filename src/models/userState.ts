import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {Root} from './root';

const DEFAULT_DISTANCE = 100;
const DEFAULT_MEDIA_MAX_WIDTH = 640;
const DEFAULT_MEDIA_MAX_HEIGHT = 480;
const DEFULT_MEDIA_DIMENTIONS: api.MediaDimentions = {
  maxHeight: DEFAULT_MEDIA_MAX_HEIGHT,
  maxWidth: DEFAULT_MEDIA_MAX_WIDTH,
};

export class UserState {
  model: Root;
  complaintsCount: number = 0;
  distanceForUpdate: number = DEFAULT_DISTANCE;
  photoDimentions: api.MediaDimentions = DEFULT_MEDIA_DIMENTIONS;
  videoDimentions: api.MediaDimentions = DEFULT_MEDIA_DIMENTIONS;

  constructor(root: Root) {
    makeAutoObservable(this);
    this.model = root;
  }

  setFromApi(data?: api.UserState) {
    if (!data) return;
    if (data.notifications) this.setNotifications(data.notifications);
    if (data.location) this.setLocation(data.location);
    if (data.media) this.setMedia(data.media);
  }

  setNotifications(notifications: api.UserNotifications) {
    if (notifications.complaints) this.complaintsCount = notifications.complaints;
  }

  increaseComplainsCount() {
    this.complaintsCount++;
  }

  setLocation(location: api.UserLocationProps) {
    if (location.distance_for_update) this.distanceForUpdate = location.distance_for_update;
  }

  setMedia(media: api.UserMedia) {
    if (media.photo) this.photoDimentions = media.photo;
    if (media.video) this.videoDimentions = media.video;
  }

  clear() {
    this.complaintsCount = 0;
    this.distanceForUpdate = DEFAULT_DISTANCE;
    this.photoDimentions = DEFULT_MEDIA_DIMENTIONS;
    this.videoDimentions = DEFULT_MEDIA_DIMENTIONS;
  }

  serialize(): any {
    return {
      notifications: {complaints: this.complaintsCount},
      location: {distance_for_update: this.distanceForUpdate},
      media: {
        photo: this.photoDimentions,
        video: this.videoDimentions,
      },
    };
  }
}
