import {makeAutoObservable} from 'mobx';

import * as api from '../api';
import {appProvider} from '../appProvider';
import {TopUser} from './topUser';

export class Top {
  byId: Map<number, TopUser> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(members?: api.TopUser[]) {
    makeAutoObservable(this);
    this.setFromApi(members);
  }

  get(id: number): TopUser | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get topList(): TopUser[] {
    const list: TopUser[] = [];
    this.ids.map((id: number) => {
      const user = this.byId.get(id);
      if (user) list.push(user);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.TopUser): TopUser {
    const id = p.id;
    let user = this.byId.get(id);
    if (user) {
      user.setFromApi(p);
    } else {
      user = new TopUser(p);
      this.byId.set(id, user);
      this.ids.push(id);
    }
    return user;
  }

  setFromApi(users?: api.TopUser[]) {
    this.clear();
    if (!Array.isArray(users)) return;
    const newUsers = users.filter((user) => user.amount);
    for (const p of newUsers) {
      const user = new TopUser(p);
      this.byId.set(p.id, user);
      this.ids.push(p.id);
    }
  }

  loadTop() {
    const app = appProvider.application;
    const params: api.TopUserRequestParams = {
      limit: 30,
      offset: 0,
    };
    app.getTop(this, params);
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.topList.map((s) => s.serialize());
  }
}
