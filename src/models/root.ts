import {makeAutoObservable} from 'mobx';

import {clearFirstRunFlag, getFirstRunFlag, clearAll} from '../storage/asyncStorage';
import {Session} from './session';
import {hasProp} from '../utils/typeUtils';
import {Profile} from './profile';
import {AppController} from './AppController';
import {Teams} from './teams';
import {UserState} from './userState';
import {Trainings} from './trainings';
import {Top} from './top';
import {Statistics} from './statistics';
import {Statistic} from './statistic';
import {Events} from './events';
import {Faq} from './faq';
import {About} from './about';
import {LeaderStories} from './leaderStories';
import {Galleries} from './galleries';
import {FirebaseMessagingTypes} from '@react-native-firebase/messaging';

export class Root {
  installTime: number;
  navigationIsReady: boolean = false;
  initialMessage: FirebaseMessagingTypes.RemoteMessage | null = null;
  processedInitialMessage: boolean = false;
  appCtrl: AppController | null = null;
  readonly session: Session = new Session();
  readonly profile: Profile = new Profile();
  readonly userState: UserState = new UserState(this);
  readonly teams: Teams = new Teams();
  readonly trainings: Trainings = new Trainings();
  readonly top: Top = new Top();
  readonly statistics: Statistics = new Statistics();
  readonly statistic: Statistic = new Statistic();
  readonly events: Events = new Events();
  readonly faq: Faq = new Faq();
  readonly about: About = new About();
  readonly leaderStories: LeaderStories = new LeaderStories();
  readonly galleries: Galleries = new Galleries();

  constructor() {
    this.installTime = 0;
    makeAutoObservable(this);
    this.fillFromAsyncStorage();
  }

  setInstallTime() {
    this.installTime = Date.now();
  }

  setNavigationReady() {
    this.navigationIsReady = true;
  }

  setInitialMessage(remoteMessage: FirebaseMessagingTypes.RemoteMessage | null) {
    this.processedInitialMessage = true;
    this.initialMessage = remoteMessage;
  }

  setAppController() {
    this.appCtrl = new AppController(this);
  }

  /**
   * Prepare data to be saved to disk
   *
   * @returns Some JS structure suitable for JSON.stringify
   */
  serialize(): any {
    return {installTime: this.installTime, session: this.session.serialize(), profile: this.profile.serialize()};
  }

  fillFromAsyncStorage() {
    // TODO remove when will have ready common migrations from asyncStorage
    getFirstRunFlag().then((value) => {
      if (value === 'true') {
        this.setInstallTime();
        clearFirstRunFlag();
      }
    });
  }

  /**
   * Load previously stored data
   *
   * @param data JS structure returned by serialize
   */
  load(data: any) {
    if (!data) {
      // First run!
      this.setInstallTime();
      return;
    }
    if (hasProp(data, 'installTime')) this.installTime = Number(data.installTime);
    if (hasProp(data, 'session')) this.session.load(data.session);
    if (hasProp(data, 'profile')) this.profile.load(data.profile);
  }

  clear() {
    this.session.clear();
    this.profile.clear();
    this.userState.clear();
    this.teams.clear();
    this.trainings.clear();
    this.top.clear();
    this.statistics.clear();
    this.statistic.clear();
    this.events.clear();
    this.faq.clear();
    this.about.clear();
    this.leaderStories.clear();
    this.galleries.clear();

    return clearAll()
  }
}
