import {makeAutoObservable} from 'mobx';

import {CountryWithLangs, getCountry} from '../../mocks/countries';
import {appProvider} from '../appProvider';
import {ApiError} from '../api/error';
import {ConfirmCodeResponse} from '../api/codecs.session';
import {TimerController} from './timerController';

type PhoneParts = {
  dialCode: string;
  unmaskedPhoneNumber: string;
  countryCode: string;
};

export enum LoginWizardStep {
  PhoneNumber,
  ConfirmationCode,
}

export class LoginController {
  dialCode: string = '';
  countryCode: string = '';
  unmaskedPhoneNumber: string = '';
  step: LoginWizardStep = LoginWizardStep.PhoneNumber;
  defaultCountry: CountryWithLangs | null = getCountry('RU');
  confirmCode: string = '';
  error: string = '';
  timer: TimerController = new TimerController();
  isLoading: boolean = false;
  hasChanges: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  get phoneNumber(): string {
    return `${this.dialCode}${this.unmaskedPhoneNumber}`;
  }

  get codeReady(): boolean {
    return this.confirmCode.length === 4;
  }

  get isError(): boolean {
    return Boolean(this.error);
  }

  setConfirmCodeStep() {
    this.step = LoginWizardStep.ConfirmationCode;
  }

  setPhoneParts = (parts: PhoneParts) => {
    this.countryCode = parts.countryCode;
    this.dialCode = parts.dialCode;
    this.unmaskedPhoneNumber = parts.unmaskedPhoneNumber;
    this.hasChanges = parts.unmaskedPhoneNumber ? true : false;
  };

  setError(error: string) {
    this.error = error;
  }

  cleanError() {
    this.error = '';
  }

  setConfirmCode = (code: string) => {
    this.confirmCode = code;
    if (this.codeReady) {
      this.onConfirmCode();
    }
  };

  setLoading() {
    this.isLoading = true;
  }

  stopLoading() {
    this.isLoading = false;
  }

  resetConfirmCode() {
    this.confirmCode = '';
  }

  deferredReset() {
    setTimeout(() => {
      this.cleanError();
      this.resetConfirmCode();
    }, 1500);
  }

  onLogIn = () => {
    this.setLoading();
    appProvider.application
      .logIn(this)
      .then(() => {
        this.timer.start();
      })
      .finally(() => this.stopLoading());
  };

  onResendCode = () => {
    const app = appProvider.application;
    this.setLoading();
    app
      .resendLoginCode(this)
      .then((res) => {
        app.logger.log('reSendOtp', res);
      })
      .catch((e: ApiError) => {
        app.logger.error('reSendOtp', e);
        this.setError(e.message || String(e));
      })
      .finally(() => this.stopLoading());
  };

  onConfirmCode = () => {
    const app = appProvider.application;
    this.setLoading();
    app
      .confirmLoginCode(this)
      .then(() => {
        this.cleanError();
        if (app.model.session.isAuthorized) {
          app.navigateToStartScreens()
        } else {
          this.setError('Authorization failed');
        }
      })
      .catch((e: ApiError) => {
        this.setError(e.message || String(e));
        this.deferredReset();
        app.logger.error(e.message);
      })
      .finally(() => {
        this.stopLoading();
      });
  };
}
