import {makeAutoObservable} from 'mobx';
import {Alert} from 'react-native';
import Moment from 'moment-timezone';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import {ApiError} from '../api/error';
import {API_DOMAIN_URL} from "../constants/general";

export class Event {
  id: number = 0;
  name: string = '';
  date: string = '';
  time: string = '';
  image: string = '';
  speaker: string = '';
  link: string = '';
  description: string = '';
  subscribeEvent: boolean = false;
  loading: boolean = false;

  constructor(data?: api.Event) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  get eventOutdated(): boolean {
    const now = Moment.tz().utc();
    return now > Moment(this.time).tz('UTC').utc();
  }

  get eventReady(): boolean {
    const now = Moment.tz('UTC').subtract(1, 'hours').utc();
    return now < Moment(this.time).tz('UTC').utc();
  }

  setFromApi(data?: api.Event) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name || '';
    this.date = data.date || '';
    this.time = data.date || '';
    this.image = data.image || '';
    this.speaker = data.speaker || '';
    this.link = data.link || '';
    this.description = data.description ? data.description.replace(/src="/i, `src="${API_DOMAIN_URL}`) : '';
    this.subscribeEvent = data.subscribe_event || false;
  }

  loadEvent() {
    const app = appProvider.application;
    app.getEvent(this.id);
  }

  requestEvent() {
    this.setLoading(true);

    this.sendRequestEvent().finally(() => {
      this.setLoading(false);
      this.subscribeEvent = true;
      this.showAlert();
    });
  }

  sendRequestEvent(): Promise<void> {
    return appProvider.application
      .sendRequestEvent(this.id)
      .catch((e: ApiError) => {
        throw e;
      });
  }

  showAlert = () => {
    Alert.alert(
      'Поздравляем!',
      'Вы зарегистрированы на мероприятие',
      [
        {text: 'Закрыть'},
      ],
      {cancelable: false},
    );
  };

  setLoading(val: boolean) {
    this.loading = val;
  }

  serialize(): any {
    return {
      id: this.id,
      name: this.name,
      date: this.date,
      time: this.time,
      image: this.image,
      speaker: this.speaker,
      link: this.link,
      description: this.description,
      subscribe_event: this.subscribeEvent,
    };
  }
}
