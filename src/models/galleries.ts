import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';

const DEMO_CONTENT:any[] = [
  {
    media: 'http://game.annaniks.com/media/static/images/2021/07/11/image.jpg',
    type: 'image',
    description: 'Подпись под фото'
  },
  {
    media: 'http://game.annaniks.com/media/static/images/2021/07/11/image.jpg',
    type: 'image',
    description: 'Подпись под фото'
  },
  {
    media: 'sl_pxCAcJz4',
    type: 'video',
    description: 'Подпись под видео'
  },
  {
    media: 'http://game.annaniks.com/media/static/images/2021/07/11/image.jpg',
    type: 'image',
    description: 'Подпись под фото'
  },
];

const DEMO_GALLERIS:api.Gallery[] = [
  {
    "id": 34,
    "name": "Встреча лидеров",
    "date": "2021-07-18T10:36:31.346685Z",
    "link": "https://www.youtube.com/",
    "speaker": "Иван Иванов",
    "image": "string",
    "created_at": "2021-07-18T10:36:31.346751Z",
    "send_30": true,
    "send_15": true,
    "send": true,
    "team": 1
  },
  {
    "id": 35,
    "name": "Тимбилдинг",
    "date": "2021-07-18T10:36:31.346685Z",
    "link": "https://www.youtube.com/",
    "speaker": "Иван Иванов",
    "image": "string",
    "created_at": "2021-07-18T10:36:31.346751Z",
    "send_30": true,
    "send_15": true,
    "send": true,
    "team": 1
  },
];

export interface GalleryContentType {
  media: string;
  type: string;
  description?: string;
  video_id: string;
  active?: boolean;
}

export class GalleryContent {
  media: string = '';
  type: string = '';
  description?: string = '';
  videoId: string = '';
  active: boolean = false;

  constructor(data?: GalleryContentType) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: GalleryContentType) {
    if (!data) return;
    this.media = data.media || '';
    this.type = data.type || '';
    this.description = data.description || '';
    this.videoId = data.video_id || '';
    this.active = false;
  }

  setActive(val: boolean) {
    this.active = val;
  }

}

export class Gallery {
  id: number | null = null;
  name: string = '';
  date: string = '';
  image: string = '';
  content: GalleryContent[] = [];
  loading: boolean = false;

  constructor(data?: api.Gallery) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  get contentList(): GalleryContent[] {
    const list: GalleryContent[] = [];
    this.content.forEach((item) => {
      if (item) list.push(item);
    });
    return list;
  }

  setFromApi(data?: api.Gallery) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name || '';
    this.date = data.date || '';
    this.image = data.image || '';
    this.setContent(data.gallery_content);
  }

  setContent(content) {
    if (!content) return;
    const list: GalleryContent[] = [];
    content.forEach((item) => {
      if (item) {
        const galleryContent = new GalleryContent(item);
        list.push(galleryContent);
      }
    });
    this.content = list;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadGallery() {
    const app = appProvider.application;
    app.getGallery(this.id);
  }

  serialize(): any {
    return {
      id: this.id,
      name: this.name,
      date: this.date,
      image: this.image,
      content: this.content,
    };
  }
}

export class Galleries {
  byId: Map<number, Gallery> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(gallery?: api.Gallery[]) {
    makeAutoObservable(this);
    this.setFromApi(gallery);
  }

  get(id: number): Gallery | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get galleriesList(): Gallery[] {
    const list: Gallery[] = [];
    this.ids.forEach((id) => {
      const gallery = this.byId.get(id);
      if (gallery) list.push(gallery);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Gallery): Gallery | null {
    return this.addGallery(p);
  }

  setFromApi(gallery?: api.Gallery[]) {
    this.clear();
    if (!Array.isArray(gallery)) return;
    for (const p of gallery) {
      this.addGallery(p);
    }
  }

  private addGallery(p: api.Gallery) {
    const id = p.id;
    let gallery = this.byId.get(id);
    if (gallery) {
      gallery.setFromApi(p);
    } else {
      gallery = new Gallery(p);
      this.byId.set(id, gallery);
      this.ids.push(id);
    }
    return gallery;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadGalleries() {
    const app = appProvider.application;

    const params: api.PageParams = {
      limit: 100,
      offset: 0,
    };

    app.getGalleries(this, params);
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.galleriesList.map((gallery) => gallery.serialize());
  }
}
