import {makeAutoObservable} from 'mobx';

const INITIAL_SECONDS = 60;

export class TimerController {
  initialSeconds: number = INITIAL_SECONDS;
  currentSeconds: number = 0;
  intervalId: ReturnType<typeof setInterval> | null = null;

  constructor(initialSeconds?: number) {
    makeAutoObservable(this);
    if (initialSeconds) this.setInitialSeconds(initialSeconds);
  }

  start() {
    this.clear();
    this.setCurrentSeconds(this.initialSeconds);
    this.intervalId = setInterval(() => {
      this.decrementClock();
    }, 1000);
  }

  clear() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
    this.setCurrentSeconds(0);
  }

  private setInitialSeconds(val: number) {
    this.initialSeconds = val;
  }

  private decrementClock() {
    const s = this.currentSeconds - 1;
    if (s < 1) {
      this.clear();
    } else {
      this.setCurrentSeconds(s);
    }
  }

  private setCurrentSeconds(value: number) {
    this.currentSeconds = value;
  }
}
