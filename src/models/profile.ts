import {makeAutoObservable} from 'mobx';
import * as api from 'src/api';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {STORAGE_KEYS} from '../constants/storage_keys.constants';

export class Profile {
  id: number = 0;
  firstName: string = '';
  lastName: string = '';
  avatar: string = '';
  instagramName: string = '';
  email: string = '';
  phone: string = '';
  isActive: boolean = false;
  isVerified: boolean = false;
  isFilled: boolean = false;
  team: number | null = 1;
  document: string = '';
  statisticIsWriten: boolean = false;
  loading: boolean = false;

  constructor(data?: api.Profile) {
    makeAutoObservable(this);
    if (data) this.setFromApi(data);
  }

  get isValid(): boolean {
    return Boolean(this.id);
  }

  setFromApi(data?: api.Profile) {
    if (!data) return;
    this.id = data.id;
    this.firstName = data.first_name || '';
    this.lastName = data.last_name || '';
    this.avatar = data.image || '';
    this.instagramName = data.instagram_name;
    this.email = data.email || '';
    this.phone = data.phone_number;
    this.isActive = data.is_active || false;
    this.isVerified = data.verification_status === 'approved'
    this.isFilled = data.verification_status === 'filed'
    this.team = data.team || null;
    this.statisticIsWriten = data.statistic_is_writen ||false;
    this.document = data.document || '';
    this.saveToAsyncStorage();
  }

  serialize(): any {
    return {
      id: this.id,
      first_name: this.firstName,
      last_name: this.lastName,
      image: this.avatar,
      instagram_name: this.instagramName,
      email: this.email,
      phone_number: this.phone,
      is_active: this.isActive,
      team: this.team,
      document: this.document,
      statistic_is_writen: this.statisticIsWriten,
    };
  }

  load(data: any) {
    if (!data) return;
    this.id = data.id;
    this.firstName = data.first_name;
    this.lastName = data.last_name;
    this.avatar = data.image;
    this.instagramName = data.instagram_name;
    this.email = data.email;
    this.phone = data.phone_number;
    this.isActive = data.is_active;
    this.isVerified = false;
    this.isFilled = false;
    this.statisticIsWriten = false;
    this.team = data.team;
    this.document = data.document;
    this.saveToAsyncStorage();
  }

  clear() {
    this.id = 0;
    this.firstName = '';
    this.lastName = '';
    this.avatar = '';
    this.instagramName = '';
    this.email = '';
    this.phone = '';
    this.isActive = false;
    this.isVerified = false;
    this.isFilled = false;
    this.team = null;
    this.document = '';
    this.statisticIsWriten = false;

  }

  clone(): Profile {
    const profile = new Profile();
    profile.id = this.id;
    profile.firstName = this.firstName;
    profile.lastName = this.lastName;
    profile.avatar = this.avatar;
    profile.instagramName = this.instagramName;
    profile.email = this.email;
    profile.phone = this.phone;
    profile.isActive = this.isActive;
    profile.isVerified = this.isVerified;
    profile.isFilled = this.isFilled;
    profile.statisticIsWriten = this.statisticIsWriten;
    profile.team = this.team;
    profile.document = this.document;
    return profile;
  }

  export(): api.User4Export {
    return {
      first_name: this.firstName,
      last_name: this.lastName,
      image: this.avatar,
      instagram_name: this.instagramName || '',
      email: this.email,
    };
  }

  verificationExport(): api.Verification4Export {
    return {
      email: this.email,
      team: this.team,
      instagram_name: this.instagramName,
      document: this.document,
    };
  }

  documentExport(): string {
    return this.document;
  }

  avatarExport(): string {
    return this.avatar;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  saveToAsyncStorage() {
    // TODO remove this when this model will have used everywhere instead STORAGE_KEYS.PROFILE
    AsyncStorage.setItem(STORAGE_KEYS.PROFILE, JSON.stringify(this.serialize()));
  }

  setFirstName(val: string) {
    this.firstName = val;
  }

  setLastName(val: string) {
    this.lastName = val;
  }

  setAvatar(val: string) {
    this.avatar = val;
  }

  setInstagramName(val: string) {
    this.instagramName = val;
  }

  setEmail(val: string) {
    this.email = val;
  }

  setPhone(val: string) {
    this.phone = val;
  }

  setTeam(val: number) {
    this.team = val;
  }

  setDoc(val: string) {
    this.document = val;
  }
}
