import {makeAutoObservable} from 'mobx';

import * as api from '../api';
import {Lesson} from './lesson';

export class Lessons {
  byId: Map<number, Lesson> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(lessons?: api.Lesson[]) {
    makeAutoObservable(this);
    this.setFromApi(lessons);
  }

  get(id: number): Lesson | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get lessonsList(): Lesson[] {
    const list: Lesson[] = [];
    this.ids.forEach((id) => {
      const lesson = this.byId.get(id);
      if (lesson) list.push(lesson);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Lesson): Lesson | null {
    return this.addLesson(p);
  }

  setFromApi(lessons?: api.Lesson[]) {
    this.clear();
    if (!Array.isArray(lessons)) return;
    for (const p of lessons) {
      this.addLesson(p);
    }
  }

  private addLesson(p: api.Lesson) {
    const id = p.id;
    let lesson = this.byId.get(id);
    if (lesson) {
      lesson.setFromApi(p);
    } else {
      lesson = new Lesson(p);
      this.byId.set(id, lesson);
      this.ids.push(id);
    }
    return lesson;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.lessonsList.map((lesson) => lesson.serialize());
  }
}
