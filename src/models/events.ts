import {makeAutoObservable} from 'mobx';
import * as _ from 'lodash';

import * as api from '../api';
import {appProvider} from '../appProvider';
import {Event} from './event';
import Moment from 'moment-timezone';

export interface IntervalDates {
  minDate: string;
  maxDate: string;
}

export class Events {
  byId: Map<number, Event> = new Map();
  ids: number[] = [];
  minDate: string = '';
  maxDate: string = '';
  upcomingEvents: Event[] = [];
  dateEvents: Event[] = [];
  selectedDateEvents: string = '';
  initial: boolean = false;
  loading: boolean = false;

  constructor(event?: api.Event[]) {
    makeAutoObservable(this);
    this.setFromApi(event);
  }

  get(id: number): Event | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get eventsList(): Event[] {
    const list: Event[] = [];
    this.ids.forEach((id) => {
      const event = this.byId.get(id);
      if (event) list.push(event);
    });
    return list;
  }

  get eventsDates(): any {
    const dates: any = {};
    const events: any[] = [];

    this.ids.forEach((id) => {
      const event = this.byId.get(id);
      if (event) {
        const date = new Date(event.date);
        event.date = Moment(date).format('YYYY-MM-DD');
        events.push(event);
      };
    });

    if (events.length) {
      _.uniqBy(events, 'date').map((event) => {
        dates[event.date] = {
          selected: true
        }
      });
    };

    return dates;
  }

  get upcomingEventsList(): Event[] {
    const list: Event[] = [];

    this.upcomingEvents.forEach((event) => {
      if (event) list.push(event);
    });
    return list;
  }

  get dateEventsList(): Event[] {
    const list: Event[] = [];

    this.dateEvents.forEach((event) => {
      if (event) list.push(event);
    });
    return list;
  }

  get dateEventsInterval(): IntervalDates {
    const currentYear = Moment.tz().format('YYYY');
    const currentMonth = Moment.tz().format('MM');
    const currentDay = Moment.tz().format('DD');
    const lastDay = Moment(`${currentYear}-${currentMonth}-01`, 'YYYY-MM-DD').endOf('month').format('DD');

    if (!this.minDate || !this.maxDate) {
      this.minDate = `${currentYear}-${currentMonth}-${currentDay}`;
      this.maxDate = `${currentYear}-${currentMonth}-${lastDay}`;
    }

    return {
        minDate: this.minDate,
        maxDate: this.maxDate,
      }
  }

  get dateFrom(): string {
    return this.minDate;
  }

  get dateTo(): string {
    return this.maxDate;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Event): Event | null {
    return this.addEvent(p);
  }

  setFromApi(events?: api.Event[]) {
    this.clear();
    if (!Array.isArray(events)) return;

    for (const p of events) {
      this.addEvent(p);
    }
  }

  private addEvent(p: api.Event) {
    const id = p.id;
    let event = this.byId.get(id);
    if (event) {
      event.setFromApi(p);
    } else {
      event = new Event(p);
      this.byId.set(id, event);
      this.ids.push(id);
    }
    return event;
  }

  setUpcomingEvents(events: api.Event[]) {
    console.log('setUpcomingEvents', events);
    const upcomingEvents: Event[] = [];
    const firstDay = Moment.tz().utc();
    const lastDay = Moment.tz().add(7, 'days').utc();

    events.forEach((ev) => {
      const event = new Event(ev);
      const date = Moment(new Date(event.date)).utc();
      if ((date >= firstDay) && (date <= lastDay)) {
        upcomingEvents.push(event);
      }
    });

    this.upcomingEvents = upcomingEvents.sort((a, b) => a.date < b.date ? -1 : a.date < b.date ? 1 : 0);
  }

  setDateEvents(day: string) {
    const dateEvents: Event[] = [];
    const selectedDate = new Date(day).getTime();

    this.ids.forEach((id) => {
      const event = this.byId.get(id);
      if (event) {
        const date = new Date(event.date).getTime();
        if (selectedDate === date) {
          dateEvents.push(event);
        }
      };
    });

    this.selectedDateEvents = day;
    this.dateEvents = dateEvents;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadEvents(startDate?: string, lastDate?: string) {
    if (startDate && lastDate) {
      this.minDate = startDate;
      this.maxDate = lastDate;
    } else {
      const dateEventsInterval = this.dateEventsInterval;
      this.minDate = dateEventsInterval.minDate;
      this.maxDate = dateEventsInterval.maxDate;
    }

    const app = appProvider.application;
    const params: api.EventsRequestParams = {
      limit: 100,
      offset: 0,
      from_date: this.minDate,
      to_date: this.maxDate,
    };
    this.clear();
    app.getEvents(this, params);
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.dateEvents = [];
    this.selectedDateEvents = '';
    this.loading = false;
  }

  serialize() {
    return this.eventsList.map((event) => event.serialize());
  }
}
