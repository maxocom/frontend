import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import {API_DOMAIN_URL} from "../constants/general";

export class LeaderStory {
  id: number | null = null;
  firstName: string = '';
  lastName: string = '';
  image: string = '';
  description: string = '';
  link: string = '';
  loading: boolean = false;

  constructor(data?: api.LeaderStory) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.LeaderStory) {
    if (!data) return;
    this.id = data.id;
    this.firstName = data.first_name || '';
    this.lastName = data.last_name || '';
    this.image = data.image || '';
    this.description = data.description ? data.description.replace(/src="/i, `src="${API_DOMAIN_URL}`) : '';
    this.link = data.link || '';
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadLeaderStory() {
    const app = appProvider.application;
    app.getLeaderStory(this.id);
  }

  serialize(): any {
    return {
      id: this.id,
      first_name: this.firstName,
      last_name: this.lastName,
      image: this.image,
      description: this.description,
      link: this.link,
    };
  }
}

export class LeaderStories {
  byId: Map<number, LeaderStory> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(leaderStory?: api.LeaderStory[]) {
    makeAutoObservable(this);
    this.setFromApi(leaderStory);
  }

  get(id: number): LeaderStory | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get leaderStoriesList(): LeaderStory[] {
    const list: LeaderStory[] = [];
    this.ids.forEach((id) => {
      const leaderStory = this.byId.get(id);
      if (leaderStory) list.push(leaderStory);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.LeaderStory): LeaderStory | null {
    return this.addLeaderStory(p);
  }

  setFromApi(leaderStory?: api.LeaderStory[]) {
    this.clear();
    if (!Array.isArray(leaderStory)) return;
    for (const p of leaderStory) {
      this.addLeaderStory(p);
    }
  }

  private addLeaderStory(p: api.LeaderStory) {
    const id = p.id;
    let leaderStory = this.byId.get(id);
    if (leaderStory) {
      leaderStory.setFromApi(p);
    } else {
      leaderStory = new LeaderStory(p);
      this.byId.set(id, leaderStory);
      this.ids.push(id);
    }
    return leaderStory;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadLeaderStories() {
    const app = appProvider.application;

    const params: api.PageParams = {
      limit: 100,
      offset: 0,
    };

    app.getLeaderStories(this, params);
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.leaderStoriesList.map((leaderStory) => leaderStory.serialize());
  }
}
