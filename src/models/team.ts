import {makeAutoObservable} from 'mobx';
import * as api from 'src/api';

export class Team {
  id: number | null = null;
  name: string = '';

  constructor(data?: api.Team) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.Team) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name || '';
  }

  serialize(): any {
    return {
      id: this.id,
      name: this.name,
    };
  }
}
