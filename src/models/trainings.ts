import {makeAutoObservable} from 'mobx';

import * as api from '../api';
import {appProvider} from '../appProvider';
import {Training} from './training';

export class Trainings {
  byId: Map<number, Training> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(trainings?: api.Training[]) {
    makeAutoObservable(this);
    this.setFromApi(trainings);
  }

  get(id: number): Training | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get trainingsList(): Training[] {
    const list: Training[] = [];
    this.ids.forEach((id) => {
      const training = this.byId.get(id);
      if (training) list.push(training);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Training): Training | null {
    return this.addTraining(p);
  }

  setFromApi(trainings?: api.Training[]) {
    this.clear();
    if (!Array.isArray(trainings)) return;
    for (const p of trainings) {
      this.addTraining(p);
    }
  }

  private addTraining(p: api.Training) {
    const id = p.id;
    let training = this.byId.get(id);
    if (training) {
      training.setFromApi(p);
    } else {
      training = new Training(p);
      this.byId.set(id, training);
      this.ids.push(id);
    }
    return training;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadTrainings() {
    const app = appProvider.application;
    app.getTrainings(this);
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.trainingsList.map((training) => training.serialize());
  }
}