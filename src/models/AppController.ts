import {makeAutoObservable} from 'mobx';
import {AppState, AppStateStatus} from 'react-native';
import {IDisposer} from 'mobx-utils';

import {Root} from './root';
import {appProvider} from '../appProvider';

export class AppController {
  private disposers: IDisposer[] = [];
  model: Root;
  app = appProvider.application;

  constructor(root: Root) {
    makeAutoObservable(this);
    this.model = root;
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  private handleAppStateChange = (status: AppStateStatus) => {
    this.app.logger.log(`App State: ${status}`);
    switch (true) {
      case status === 'active':
        if (this.model.session.isAuthorized) {
          this.app.syncUserData();
        }
        break;
      case Boolean(status.match(/inactive|background/)):
        break;
    }
  };

  dispose() {
    for (const disposer of this.disposers) {
      disposer();
    }
  }

  destroy() {
    AppState.removeEventListener('change', this.handleAppStateChange);
    this.dispose();
  }
}
