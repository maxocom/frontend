import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import {Lessons} from './lessons';
import {Lesson} from './lesson';

const DISABLED_TRAINING_IDS: number[] = [6];

export class Training {
  id: number = 0;
  name: string = '';
  isDisabled: boolean = false;
  lessons: Lessons = new Lessons();
  loading: boolean = false;

  constructor(data?: api.Training) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.Training) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name || '';
    this.isDisabled = DISABLED_TRAINING_IDS.includes(data.id);
  }

  setLessons(lessons?: api.Lesson[], count?: number ) {
    const newCount = count ? count : 1;
    if (!Array.isArray(lessons)) return;
    if ((this.lessons.lessonsList.length + lessons.length) < newCount) {
      this.lessons.setFromApi(lessons);
      this.loadLessons((Math.ceil(newCount / 10) - Math.ceil(this.lessons.ids.length / 10) + 1));
    } else {
      const oldLessons: api.Lesson[] = this.lessons.lessonsList.map((lesson: Lesson) => {
        return {
          id: lesson.id,
          name: lesson.name,
          seen_by_me: lesson.seen_by_me,
          description: lesson.description,
          video: lesson.video,
        }
      });
      const allLessons = [ ...oldLessons, ...lessons];
      this.lessons.setFromApi(allLessons);
    }
  }

  get lessonsList(): Lesson[] {
    const list: Lesson[] = [];
    this.lessons.ids.forEach((id) => {
      const lesson = this.lessons.byId.get(id);
      if (lesson) list.push(lesson);
    });
    return list;
  }

  loadLessons(page?: number) {
    const app = appProvider.application;
    app.getTraining(this, page);
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  serialize(): any {
    return {
      id: this.id,
      name: this.name,
      isDisabled: this.isDisabled,
      lessons: this.lessons.serialize(),
    };
  }
}
