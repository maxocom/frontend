import {makeAutoObservable} from 'mobx';
import * as api from 'src/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {STORAGE_KEYS} from '../constants/storage_keys.constants';

export class Session {
  uuid: string = '';
  expiredAt?: string = '';
  accessToken: string = '';
  refreshToken: string = '';
  fcmToken: string = '';
  fcmTokenId: number | null = null;

  constructor(data?: api.Session) {
    makeAutoObservable(this);
    if (data) this.setFromApi(data);
  }

  get isAuthorized(): boolean {
    return Boolean(this.accessToken);
  }

  setTokens(tokens?: api.AccessTokens) {
    if (!tokens) return;
    this.accessToken = tokens.access_token;
    this.refreshToken = tokens.refresh_token;
    this.saveToAsyncStorage();
  }

  setAccessToken(tokens?: api.AccessTokens) {
    if (!tokens?.access_token) return;
    this.accessToken = tokens.access_token;
    AsyncStorage.setItem(STORAGE_KEYS.ACCESS_TOKEN, this.accessToken);
  }

  setFcmToken(data: api.FcmRegistrationResponse) {
    this.fcmToken = data.push_token;
    this.fcmTokenId = data.id;
    AsyncStorage.setItem(STORAGE_KEYS.FCM_TOKEN_ID, ('' + this.fcmTokenId));
  }

  setFromApi(data?: api.Session) {
    if (!data) return;
    this.uuid = data.uuid;
    this.expiredAt = data.expired_at;
    this.saveToAsyncStorage();
  }

  serialize(): any {
    return {
      access_token: this.accessToken,
      refresh_token: this.refreshToken,
      uuid: this.uuid,
      expired_at: this.expiredAt || '',
      fcm_token: this.fcmToken || '',
      fcm_token_id: this.fcmTokenId || '',
    };
  }

  load(data: any) {
    if (!data) return;
    this.accessToken = data.access_token;
    this.refreshToken = data.refresh_token;
    this.uuid = data.uuid;
    this.expiredAt = data.expired_at;
    this.fcmToken = data.fcm_token;
    this.fcmTokenId = data.fcm_token_id;
    this.saveToAsyncStorage();
  }

  clear() {
    this.accessToken = '';
    this.refreshToken = '';
    this.fcmToken = '';
    this.fcmTokenId = null;
  }

  saveToAsyncStorage() {
    AsyncStorage.setItem(STORAGE_KEYS.ACCESS_TOKEN, this.accessToken);
    AsyncStorage.setItem(STORAGE_KEYS.REFRESH_TOKEN, this.refreshToken);
  }
}
