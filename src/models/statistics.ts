import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import Moment from 'moment-timezone';
import * as _ from 'lodash';

export enum TimeIntervals {
  ONE_WEEK = 'ONE_WEEK',
  ONE_MONTH = 'ONE_MONTH',
  THREE_MONTH = 'THREE_MONTH',
  CUSTOM = '',
}

export enum TimeIntervalsValues {
  ONE_WEEK = '1w',
  ONE_MONTH = '1m',
  THREE_MONTH = '3m',
}

export enum TimeIntervalsName {
  ONE_WEEK = 'За неделю',
  ONE_MONTH = 'За месяц',
  THREE_MONTH = 'За 3 месяца',
}

export const TimeIntervalsMoment = {
  ONE_WEEK: { count: 1, value: 'week'},
  ONE_MONTH: { count: 1, value: 'month'},
  THREE_MONTH: { count: 3, value: 'month'},
}

const MAX_CHART_COUNT:number = 7;

export const DefaultTimeInterval: string = TimeIntervals.ONE_WEEK;

export class Statistics {
  postedVideo: number = 0;
  numberOfMeetings: number = 0;
  numberOfTouches: number = 0;
  date: string = '';
  timeInterval: string = TimeIntervalsValues.ONE_WEEK;
  timeStart: any = Moment.tz().subtract(TimeIntervalsMoment[DefaultTimeInterval].count, `${TimeIntervalsMoment[DefaultTimeInterval].value}`);
  timeEnd: any = Moment.tz();
  hasData: boolean = false;
  chart: any = [];
  chartDates: any[] = [];
  loading: boolean = false;

  constructor(data?: api.Statistics[]) {
    makeAutoObservable(this);
    if (data) this.setFromApi(data);
  }

  setFromApi(data?: api.Statistics[]) {
    if (data.length < 3) return;
    this.setChartData(data);
    this.hasData = true;
  }

  loadStatistics() {
    const app = appProvider.application;

    const params: api.StatisticParams = {
      limit: 100,
      offset: 0,
      from_date: Moment(this.timeStart).format('YYYY-MM-DD'),
      to_date: Moment(this.timeEnd).format('YYYY-MM-DD'),
    };
    app.getStatistics(this, params);
  }

  setTimeInterval(timeStart: any, timeEnd: any, timeInterval: TimeIntervals) {
    this.timeInterval = TimeIntervalsValues[timeInterval];
    this.timeStart = timeStart;
    this.timeEnd = timeEnd;

    this.loadStatistics();
  }

  setEmptyData() {
    this.hasData = false;
  }

  setChartData(chartData: api.Statistics[]) {

    const sortedChartData: any[] = this.getNewChartArray(chartData);
    const chartDates: any[] = sortedChartData.map((chartItem:any) => chartItem.date);
    const values:any = this.getNewChartValues(sortedChartData)

    this.chartDates = chartDates;
    this.chart = values;
  }

  getNewChartArray(chartData: any) {
    const chartArray:any = [];
    const newArr: any[] = _.cloneDeep(chartData).map((item) => {
      item.date = Moment(new Date(item.date), 'DD.MM.YYYY').unix()
      return item
    }).sort((a, b) => a.date < b.date ? -1 : a.date < b.date ? 1 : 0);

    const newFirst = newArr[0];
    const newLast = newArr[newArr.length -1];
    newArr.shift();
    newArr.pop();
   
    chartArray.push(newFirst);

    let tempArray: any[] = [];
    const chunk = Math.ceil(newArr.length / (MAX_CHART_COUNT - 2));
    for (let i = 0; i < newArr.length; i += chunk) {
      tempArray = newArr.slice(i, i + chunk);
      chartArray.push(tempArray[tempArray.length - 1]);
    }

    chartArray.push(newLast);

    return chartArray;
  }

  getNewChartArrayByDates(chartData: any, chartDates: any) {
    const newArr: any[] = _.cloneDeep(chartData).map((item) => {
      item.date = Moment(item.date, 'DD.MM.YYYY').unix()
      return item
    }).sort((a, b) => a.date < b.date ? -1 : a.date < b.date ? 1 : 0);

    return newArr.filter((item) => chartDates.includes(item.date));;
  }

   getNewChartValues(chartData: any) {
    const chartArray = (data, arg) => {
      return data.map((item: any, index: number) => {
        return {
          x: index + 1,
          y: item[arg],
          marker: Moment.unix(item.date).format('DD.MM')
        }
      });
    };

    const values:any = {
      postedVideoChart: chartArray(chartData, 'posted_video') || [],
      numberOfMeetingsChart: chartArray(chartData, 'number_of_meetings') || [],
      numberOfTouchesChart: chartArray(chartData, 'number_of_touches') || [],
    };

    return values;
  }

  get chartDatesList(): string[] {
    const list: string[] = [];
    this.chartDates.forEach((date) => {
      const dateStr = Moment.unix(date).format('DD.MM');
      if (dateStr) list.push(dateStr);
    });
    return list;
  }

  get chartValuesList(): any {
    console.log('chartValuesList', this.chart);

    return {
      postedVideoChart: this.returnChartObj(_.get(this.chart, 'postedVideoChart.length', false) ? this.chart.postedVideoChart : []),
      numberOfMeetingsChart: this.returnChartObj(_.get(this.chart, 'numberOfMeetingsChart.length', false) ? this.chart.numberOfMeetingsChart : []),
      numberOfTouchesChart: this.returnChartObj(_.get(this.chart, 'numberOfTouchesChart.length', false) ? this.chart.numberOfTouchesChart : []),
    }
  }

  returnChartObj(arr: any) {
    console.log('returnChartObj', arr);
    return arr.map((item) => {
      return {
        x: item.x,
        y: item.y,
        marker: item.marker,
      }
    })
  }

  clear() {
    this.postedVideo = 0;
    this.numberOfMeetings = 0;
    this.numberOfTouches = 0;
    this.date = '';
    this.loading = false;
    this.timeInterval = '';
    this.timeStart = '';
    this.timeEnd = '';
    this.hasData = false;
    this.chart = [];
    this.chartDates = [];
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  serialize(): any {
    return {
      posted_video: this.postedVideo,
      number_of_meetings: this.numberOfMeetings,
      number_of_touches: this.numberOfTouches,
      date: this.date,
    };
  }
}
