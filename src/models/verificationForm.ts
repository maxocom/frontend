import {makeAutoObservable} from 'mobx';

import {Profile} from './profile';
import {isEqual} from '../utils/isEqual';
import {IFormField, ProxyField, ProxyFieldValidator} from './formField';
import {validateEmail, validateInstagram} from '../utils/validators';
import {appProvider} from '../appProvider';
import * as api from '../api';
import {ApiError} from '../api/error';

export const MAX_LENGTH = 20;
export const MAX_DESCRIPTION_LENGTH = 300;

type VerificationApiField = keyof api.Verification4Export;
type UserApiField = keyof api.User4Export;
const VERIFICATION_API_ERROR_KEYS: VerificationApiField[] = ['email', 'team', 'instagram_name', 'doc'];
const USER_API_ERROR_KEYS: UserApiField[] = ['email'];

export class VerificationForm {
  profile: Profile = appProvider.application.model.profile.clone();
  emailField: IFormField;
  teamField: IFormField;
  instagramField: IFormField;
  docField: IFormField;
  loading: boolean = false;

  constructor() {
    makeAutoObservable(this);
    this.emailField = new ProxyField({
      getter: () => this.profile.email || '',
      setter: (val: string) => this.profile.setEmail(val),
      validator: this.validateEmail,
    });
    this.teamField = new ProxyField({
      getter: () => this.profile.team,
      setter: (val: number) => this.profile.setTeam(val),
      validator: this.validateTeam,
    });
    this.instagramField = new ProxyField({
      getter: () => this.profile.instagramName || '',
      setter: (val: string) => this.profile.setInstagramName(val),
      validator: this.validateInstagram,
    });
    this.docField = new ProxyField({
      getter: () => this.profile.document,
      setter: (val: string) => this.profile.setDoc(val),
      validator: this.validateDoc,
    });
  }

  get isValid() {
    return (
      this.emailField.isValid &&
      // this.teamField.isValid &&
      this.instagramField.isValid &&
      this.docField.isValid
    );
  }

  get hasChanges() {
    const p = appProvider.application.model.profile;
    if (!isEqual(p.email, this.profile.email)) return true;
    if (!isEqual(p.team, this.profile.team)) return true;
    if (!isEqual(p.instagramName, this.profile.instagramName)) return true;
    if (!isEqual(p.document, this.profile.document)) return true;
    return false;
  }

  markIsDirty() {
    this.emailField.markAsDirty();
    this.teamField.markAsDirty();
    this.instagramField.markAsDirty();
    this.docField.markAsDirty();
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  onSave = () => {
    console.log('onSave');
    if (!this.isValid) {
      this.markIsDirty();
      return;
    }
    this.setLoading(true);

    this.uploadFile().then(() => {
      this.sendVerification().finally(() => {
        this.setLoading(false);
        appProvider.application.navigateToTrainings();
      });
    });
  };

  sendVerification(): Promise<void> {
    return appProvider.application
      .sendVerification(this.profile.verificationExport())
      .catch((e: ApiError) => {
        this.handleVerificationError(e);
        throw e;
      });
  }

  uploadFile(): Promise<void> {
    return appProvider.application
      .uploadFile(this.profile.documentExport())
      .then((mediaFile: api.MediaFile) => {
        this.profile.setDoc(mediaFile.url)
      })
      .catch((e: ApiError) => {
        throw e;
      });
  }

  validateEmail: ProxyFieldValidator = (val: string) => {
    if (!val) return 'Email обязателен для заполнения';
    if (val && !validateEmail(val)) return 'Введите валидный Email';
    return undefined;
  };

  validateTeam: ProxyFieldValidator = (val: string) => {
    if (!val) return 'Выберите вашу команду';
    return undefined;
  };

  validateInstagram: ProxyFieldValidator = (val: string) => {
    if (!val) return 'Никнейм в Instagram обязателен для заполнения';
    if (val && !validateInstagram(val)) return 'Введите валидный никнейм в Instagram';
    return undefined;
  };

  validateDoc: ProxyFieldValidator = (val: string) => {
    if (!val) return 'Прикрепите документ';
    return undefined;
  };

  handleErrorDetails(e: ApiError): boolean {
    const app = appProvider.application;
    if (typeof e.details !== 'object') {
      app.alertError('', e);
      return true;
    }
    return false;
  }

  handleVerificationError = (e: ApiError) => {
    if (this.handleErrorDetails(e)) return;
    let errorProcessed = false;
    for (const errorKey of VERIFICATION_API_ERROR_KEYS) {
      if (e.details && e.details[errorKey]) {
        this.processVerificationApiErrors(errorKey, e.details[errorKey]);
        errorProcessed = true;
      }
    }
    if (!errorProcessed) {
      appProvider.application.alertError('', e);
    }
  };

  processVerificationApiErrors(errorKey: VerificationApiField, message) {
    switch (errorKey) {
      case 'email':
        this.emailField.setError(message);
        break;
      case 'team':
        this.teamField.setError(message);
        break;
      case 'instagram_name':
        this.instagramField.setError(message);
        break;
      case 'document':
        this.docField.setError(message);
        break;
    }
  }

  handleUserError = (e: ApiError) => {
    if (this.handleErrorDetails(e)) return;
    let errorProcessed = false;
    for (const errorKey of USER_API_ERROR_KEYS) {
      if (e.details && e.details[errorKey]) {
        this.processUserApiErrors(errorKey, e.details[errorKey]);
        errorProcessed = true;
      }
    }
    if (!errorProcessed) {
      appProvider.application.alertError('', e);
    }
  };

  processUserApiErrors(errorKey: UserApiField, message) {
    switch (errorKey) {
      case 'email':
        this.emailField.setError(message);
        break;
    }
  }
}
