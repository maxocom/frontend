import {makeAutoObservable} from 'mobx';
import * as api from 'src/api';

export class Statistic {
  postedVideo: number | null | string = '';
  numberOfMeetings: number | null| string = '';
  numberOfTouches: number | null| string = '';
  userId: number | null = null;

  constructor(data?: api.Profile) {
    makeAutoObservable(this);
  }

  serialize(): any {
    return {
      posted_video: this.postedVideo,
      number_of_meetings: this.numberOfMeetings,
      number_of_touches: this.numberOfTouches,
      user: this.userId,
    };
  }

  clear() {
    this.postedVideo = null;
    this.numberOfMeetings = null;
    this.numberOfTouches = null;
    this.userId = null;
  }

  clone(): Statistic {
    const statistic = new Statistic();
    statistic.postedVideo = this.postedVideo;
    statistic.numberOfMeetings = this.numberOfMeetings;
    statistic.numberOfTouches = this.numberOfTouches;
    statistic.userId = this.userId;

    return statistic;
  }

  export(): api.Statistic4Export {
    return {
      posted_video: Number(this.postedVideo),
      number_of_meetings: Number(this.numberOfMeetings),
      number_of_touches: Number(this.numberOfTouches),
      user: this.userId,
    };
  }

  setPostedVideo(val: number) {
    this.postedVideo = val;
  }

  setNumberOfMeetings(val: number) {
    this.numberOfMeetings = val;
  }

  setNumberOfTouches(val: number) {
    this.numberOfTouches = val;
  }

  setUserId(val: number | null) {
    this.userId = val;
  }
}
