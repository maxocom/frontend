import {makeAutoObservable} from 'mobx';

import * as api from 'src/api';
import {appProvider} from '../appProvider';
import {API_DOMAIN_URL} from "../constants/general";

export class FaqItem {
  id: number | null = null;
  title: string = '';
  answer: string = '';

  constructor(data?: api.Faq) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.Faq) {
    if (!data) return;
    this.id = data.id;
    this.title = data.title || '';
    this.answer = data.answer ? data.answer.replace(/src="/i, `src="${API_DOMAIN_URL}`) : '';
  }

  serialize(): any {
    return {
      id: this.id,
      title: this.title,
      answer: this.answer,
    };
  }
}

export class Faq {
  byId: Map<number, FaqItem> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(faq?: api.Faq[]) {
    makeAutoObservable(this);
    this.setFromApi(faq);
  }

  get(id: number): FaqItem | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get faqList(): FaqItem[] {
    const list: FaqItem[] = [];
    this.ids.forEach((id) => {
      const faq = this.byId.get(id);
      if (faq) list.push(faq);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Faq): FaqItem | null {
    return this.addFaq(p);
  }

  setFromApi(faq?: api.Faq[]) {
    this.clear();
    if (!Array.isArray(faq)) return;
    for (const p of faq) {
      this.addFaq(p);
    }
  }

  private addFaq(p: api.Faq) {
    const id = p.id;
    let faq = this.byId.get(id);
    if (faq) {
      faq.setFromApi(p);
    } else {
      faq = new FaqItem(p);
      this.byId.set(id, faq);
      this.ids.push(id);
    }
    return faq;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  loadFaq() {
    const app = appProvider.application;

    const params: api.PageParams = {
      limit: 100,
      offset: 0,
    };

    app.getFaq(this, params);
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.faqList.map((faq) => faq.serialize());
  }
}
