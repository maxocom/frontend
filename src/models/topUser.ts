import {makeAutoObservable} from 'mobx';

import * as api from '../api';

export class TopUser {
  id: number | null = null;
  firstName: string = '';
  lastName: string = '';
  avatar: string = '';
  instagramName: string = '';
  team: number | null = null;
  amount: number | null = null;

  constructor(data?: api.TopUser) {
    makeAutoObservable(this);
    if (data) this.setFromApi(data);
  }

  setFromApi(data?: api.TopUser) {
    if (!data) return;
    this.id = data.id;
    this.firstName = data.first_name || '';
    this.lastName = data.last_name || '';
    this.avatar = data.image || '';
    this.instagramName = data.instagram_name;
    this.team = data.team;
    this.amount = data.amount;
  }

  serialize(): any {
    return {
      id: this.id,
      first_name: this.firstName,
      last_name: this.lastName,
      image: this.avatar,
      instagram_name: this.instagramName,
      team: this.team,
      amount: this.amount,
    };
  }
}
