import {makeAutoObservable} from 'mobx';
import * as api from 'src/api';
import {API_DOMAIN_URL} from "../constants/general";

export class Lesson {
  id: number | null = null;
  name: string = '';
  seen_by_me: boolean = true;
  description: string = '';
  video: string = '';
  loading: boolean = false;

  constructor(data?: api.Lesson) {
    makeAutoObservable(this);
    this.setFromApi(data);
  }

  setFromApi(data?: api.Lesson) {
    if (!data) return;
    this.id = data.id;
    this.name = data.name || '';
    this.seen_by_me = !!data.seen_by_me;
    this.description = data.description ? data.description.replace(/src="/i, `src="${API_DOMAIN_URL}`) : '';
    this.video = data.video || '';
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  serialize(): any {
    return {
      id: this.id,
      name: this.name,
      seen_by_me: this.seen_by_me,
      description: this.description,
      video: this.video,
    };
  }
}
