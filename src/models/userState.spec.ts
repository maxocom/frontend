import React from 'react';
import {NavigationContainerRef} from '@react-navigation/core';

import * as api from '../api';
import {MemFs} from '../storage/memFs';
import {appProvider} from '../appProvider';
import Application from '../application';

const navigation = React.createRef<NavigationContainerRef>();

const c1: api.Complaint = {
  id: '1',
  type: api.ComplaintType.Spam,
  ad_id: 'ad1',
  inserted_at: '2020-07-03T17:40:48',
};

const userState: api.UserState = {
  notifications: {
    complaints: 2,
  },
  location: {
    distance_for_update: 200,
  },
  media: {
    photo: {
      maxHeight: 1,
      maxWidth: 2,
    },
    video: {
      maxHeight: 3,
      maxWidth: 4,
    },
  },
};

describe('UserState', () => {
  const fs = new MemFs();
  appProvider.setApplication(new Application());
  appProvider.application.bootstrap({fs, apiBase: 'api_host', navigation});
  const model = appProvider.application.model;
  it('decode user state', () => {
    expect(model.userState.complaintsCount).toBe(0);
    expect(model.userState.distanceForUpdate).toBe(100);
    expect(model.userState.photoDimentions.maxHeight).toBe(480);
    expect(model.userState.photoDimentions.maxWidth).toBe(640);
    expect(model.userState.videoDimentions.maxHeight).toBe(480);
    expect(model.userState.videoDimentions.maxWidth).toBe(640);
    model.userState.setFromApi(userState);
    expect(model.userState.complaintsCount).toBe(2);
    expect(model.userState.distanceForUpdate).toBe(200);
    expect(model.userState.photoDimentions.maxHeight).toBe(1);
    expect(model.userState.photoDimentions.maxWidth).toBe(2);
    expect(model.userState.videoDimentions.maxHeight).toBe(3);
    expect(model.userState.videoDimentions.maxWidth).toBe(4);
    appProvider.application.addComplaint(c1);
    expect(model.userState.complaintsCount).toBe(3);
  });
});
