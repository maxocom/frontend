import {makeAutoObservable} from 'mobx';

import * as api from '../api';
import {Team} from './team';

export class Teams {
  byId: Map<number, Team> = new Map();
  ids: number[] = [];
  loading: boolean = false;

  constructor(teams?: api.Team[]) {
    makeAutoObservable(this);
    this.setFromApi(teams);
  }

  get(id: number): Team | undefined {
    return this.byId.get(id);
  }

  get empty(): boolean {
    return this.ids.length === 0;
  }

  get teamList(): Team[] {
    const list: Team[] = [];
    this.ids.forEach((id) => {
      const team = this.byId.get(id);
      if (team) list.push(team);
    });
    return list;
  }

  has(id: number): boolean {
    return this.byId.has(id);
  }

  addFromApi(p: api.Team): Team | null {
    return this.addTeam(p);
  }

  setFromApi(teams?: api.Team[]) {
    this.clear();
    if (!Array.isArray(teams)) return;
    for (const p of teams) {
      this.addTeam(p);
    }
  }

  private addTeam(p: api.Team) {
    const id = p.id;
    let team = this.byId.get(id);
    if (team) {
      team.setFromApi(p);
    } else {
      team = new Team(p);
      this.byId.set(id, team);
      this.ids.push(id);
    }
    return team;
  }

  setLoading(val: boolean) {
    this.loading = val;
  }

  clear() {
    this.byId.clear();
    this.ids = [];
    this.loading = false;
  }

  serialize() {
    return this.teamList.map((team) => team.serialize());
  }
}
