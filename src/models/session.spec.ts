import 'jest';

import {Session} from './session';

describe('Session', () => {
  const session: Session = new Session();

  it('decode session', () => {
    session.setFromApi({
      uuid: 'uuid',
      expired_at: '2020-07-03T17:40:48',
    });
    expect(session.uuid).toBe('uuid');
    expect(session.expiredAt).toBe('2020-07-03T17:40:48');
    expect(session.isAuthorized).toBeFalsy();
    session.setTokens({refresh_token: 'token1', access_token: 'token2'});
    expect(session.refreshToken).toBe('token1');
    expect(session.accessToken).toBe('token2');
    expect(session.isAuthorized).toBeTruthy();
    expect(session.serialize()).toEqual({
      access_token: 'token2',
      expired_at: '2020-07-03T17:40:48',
      refresh_token: 'token1',
      uuid: 'uuid',
      fcm_token: '',
    });
  });

  it('load session', () => {
    session.load({
      uuid: 'uuid',
      expired_at: '2020-07-03T17:40:48',
      refresh_token: 'token1',
      access_token: 'token2',
      fcm_token: 'token3',
    });
    expect(session.uuid).toBe('uuid');
    expect(session.expiredAt).toBe('2020-07-03T17:40:48');
    expect(session.refreshToken).toBe('token1');
    expect(session.accessToken).toBe('token2');
    expect(session.fcmToken).toBe('token3');
    expect(session.isAuthorized).toBeTruthy();
  });

  it('clear session', () => {
    session.load({
      uuid: 'uuid',
      expired_at: '2020-07-03T17:40:48',
      refresh_token: 'token1',
      access_token: 'token2',
    });
    expect(session.isAuthorized).toBeTruthy();
    session.clear();
    expect(session.serialize()).toEqual({
      access_token: '',
      expired_at: '',
      refresh_token: '',
      uuid: '',
      fcm_token: '',
    });
    expect(session.isAuthorized).toBeFalsy();
  });
});
