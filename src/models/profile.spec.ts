import {Profile} from './profile';
import {Space} from './space';
import * as api from '../api';
import {SpaceAccessType, SpaceType} from '../api';
import {SpaceMemebershipRole, SpaceMemebershipStatus} from '../api/codecs.spaces';

const p: api.Profile = {
  id: '1',
  first_name: 'u1',
  last_name: 'u2',
  address: 'addr',
  birthdate: '2009-01-01',
  email: 'email',
  phone: 'phone',
  profile_space_id: '2',
  verified: false,
  country_code: 'ru',
  avatar: 'ava',
  getstream_token: 'token',
};

const s: api.Space = {
  id: '1',
  name: 'name',
  type: SpaceType.SelfEmployed,
  description: 'descr',
  address: 'addr',
  access_type: SpaceAccessType.Verified,
  cover: 'cover',
  lat: 0,
  lng: 0,
  distance: 0,
  tabs: [],
  is_banned: false,
  is_favorite: false,
  enabled: true,
  profession: 'prof',
  website: 'site',
  user_id: 'u1',
  chats: [],
  membership: {
    id: 'm1',
    user: {
      id: 'u1',
      first_name: 'name1',
      last_name: 'name2',
      avatar: 'ava1',
    },
    role: SpaceMemebershipRole.Owner,
    status: SpaceMemebershipStatus.Accepted,
  },
  members: [],
};

describe('Profile', () => {
  const profile: Profile = new Profile();
  const space = new Space(s);

  it('decode profile', () => {
    expect(profile.isValid).toBeFalsy();
    profile.setFromApi(p);
    expect(profile.isValid).toBeTruthy();
    expect(profile.isPublic).toBeFalsy();
    expect(space.enabled).toBeTruthy();
    profile.setSpace(space);
    expect(profile.isPublic).toBeTruthy();
    expect(profile.serialize()).toEqual(p);
  });

  it('load profile', () => {
    profile.load(p);
    expect(profile.serialize()).toEqual(p);
  });
});
