import {Space} from './space';
import * as api from '../api';
import {FAKE_GEOPOSITION} from '../services/geoposition.service';
import {MemFs} from '../storage/memFs';
import {appProvider} from '../appProvider';
import Application from '../application';
import {clock} from '../../testing/setupFakeTimers';
import React from 'react';
import {NavigationContainerRef} from '@react-navigation/core';

const s: api.Space = {
  id: '1',
  name: 'name',
  type: api.SpaceType.SelfEmployed,
  description: 'descr',
  address: 'addr',
  access_type: api.SpaceAccessType.Verified,
  cover: 'cover',
  lat: FAKE_GEOPOSITION.latitude,
  lng: FAKE_GEOPOSITION.longitude,
  distance: 0,
  tabs: [],
  is_banned: false,
  is_favorite: false,
  enabled: true,
  profession: 'prof',
  website: 'site',
  user_id: 'u1',
  chats: [],
  membership: {
    role: api.SpaceMemebershipRole.Owner,
    status: api.SpaceMemebershipStatus.Accepted,
  },
  members: [],
};

const ch1: api.Chat = {
  id: 'ch1',
  type: api.ChatType.Direct,
  name: 'name',
  updated_at: '2020-07-03T17:40:48',
};

const ch2: api.Chat = {
  id: 'ch2',
  type: api.ChatType.MainSpace,
  name: 'name',
  updated_at: '2020-07-03T17:40:48',
};

const navigation = React.createRef<NavigationContainerRef>();

beforeEach(() => {
  const fs = new MemFs();
  appProvider.setApplication(new Application());
  appProvider.application.bootstrap({fs, apiBase: 'api_host', navigation});
  clock.setSystemTime(Date.parse('2021-04-20T12:00:00Z'));
  clock.tick(10);
});
afterEach(() => {
  jest.clearAllMocks();
  clock.runToLast();
  appProvider.setApplication(undefined);
  clock.reset();
});

const space: Space = new Space(s);

it('decode space', () => {
  expect(space.serialize()).toEqual(s);
  expect(space.chats.empty).toBeTruthy();
  space.setChats([ch1, ch2]);
  expect(space.chats.empty).toBeFalsy();
});
