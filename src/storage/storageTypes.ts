export interface IFs {
  readonly ApplicationDataDir: string;
  readonly CachesDir: string;

  saveJsonFile(path: string, data: any): Promise<void>;
  loadJsonFile(path: string): Promise<any>;
}
