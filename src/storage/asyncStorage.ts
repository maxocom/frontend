// this methods saved for compability from old storage
// It's more situable to write migrations, but some later, when we'll have finished with MobX model structure
import AsyncStorage from '@react-native-async-storage/async-storage';
import {STORAGE_KEYS} from '../constants/storage_keys.constants';

type ValuePromise = Promise<string | undefined>;

const getSavedValue = (storageKey: STORAGE_KEYS): ValuePromise => {
  return AsyncStorage.getItem(storageKey)
    .then((value) => value || undefined)
    .catch(() => undefined);
};

const clearValue = (storageKey: STORAGE_KEYS): Promise<void> => {
  return AsyncStorage.removeItem(storageKey).catch(handleReadValueError);
};

export function getFirstRunFlag(): ValuePromise {
  return getSavedValue(STORAGE_KEYS.FIRST_RUN);
}

export function clearFirstRunFlag() {
  clearValue(STORAGE_KEYS.FIRST_RUN).catch(handleClearValueError);
}

export function getAccessToken(): ValuePromise {
  return getSavedValue(STORAGE_KEYS.ACCESS_TOKEN);
}

export function clearAccessToken() {
  clearValue(STORAGE_KEYS.ACCESS_TOKEN).catch(handleClearValueError);
}

export function getAccessTokenExpiredAt(): ValuePromise {
  return getSavedValue(STORAGE_KEYS.ACCESS_TOKEN_EXPIRED_AT);
}

export function clearAccessTokenExpiredAt() {
  clearValue(STORAGE_KEYS.ACCESS_TOKEN_EXPIRED_AT).catch(handleClearValueError);
}

export function getRefreshToken(): ValuePromise {
  return getSavedValue(STORAGE_KEYS.REFRESH_TOKEN);
}

export function clearRefreshToken() {
  clearValue(STORAGE_KEYS.REFRESH_TOKEN).catch(handleClearValueError);
}

export function clearAll() {
  return AsyncStorage.clear().catch(handleReadValueError);
}

const handleClearValueError = () => {
  //TODO (may be not need)
  return undefined;
};

const handleReadValueError = () => {
  //TODO
  return undefined;
};

