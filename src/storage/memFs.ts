import {IFs} from './storageTypes';

export class MemFs implements IFs {
  readonly ApplicationDataDir: string = '/app/Application Support';
  readonly CachesDir: string = '/app/Caches';

  private data: Map<string, string> = new Map();

  /**
   * Set content of file in provided path immediately. (For tests)
   * @param path File path
   * @param data File content
   */
  setFileContent(path: string, data: string) {
    this.data.set(path, data);
  }

  isFileExists(path: string): boolean {
    return this.data.has(path);
  }

  getFileContent(path: string): any {
    return this.data.get(path);
  }

  saveJsonFile(path: string, data: any): Promise<void> {
    this.data.set(path, JSON.stringify(data));
    return Promise.resolve();
  }
  loadJsonFile(path: string): Promise<any> {
    return Promise.resolve(this.data.get(path))
      .then((data) => (data ? JSON.parse(data) : undefined))
      .catch(() => undefined);
  }
}
