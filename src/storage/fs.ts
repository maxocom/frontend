import {Platform} from 'react-native';
import * as fs from 'react-native-fs';

import {dirname} from './pathUtils';

import {IFs} from './storageTypes';

export class Fs implements IFs {
  get ApplicationDataDir(): string {
    if (Platform.OS === 'ios') {
      return fs.LibraryDirectoryPath + '/Application Support';
    } else {
      return fs.DocumentDirectoryPath + '/AppData';
    }
  }
  get CachesDir(): string {
    return fs.CachesDirectoryPath;
  }
  saveJsonFile(path: string, data: any): Promise<void> {
    return fs.mkdir(dirname(path)).then(() => fs.writeFile(path, JSON.stringify(data)));
  }
  loadJsonFile(path: string): Promise<any> {
    return fs
      .readFile(path)
      .then((content) => JSON.parse(content))
      .catch(() => undefined);
  }
}
