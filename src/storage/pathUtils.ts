/**
 * Returns the directory name of a path, similar to the Unix dirname command.
 * @param path path delimited with '/' symbol
 */
export function dirname(path: string): string {
  // NOTE: this is not generic, but will satisfy our needs:
  // - it is not crossplatform
  // - it has various edge cases, for ex '/some/dir/' will return '/some/dir' instead of '/some'
  // For correct implementations see sources of Node.js
  const idx = path.lastIndexOf('/');
  if (idx === -1) return '';
  return path.slice(0, idx);
}
