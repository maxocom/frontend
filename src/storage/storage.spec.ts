import 'jest';

import {MemFs} from './memFs';
import {Storage} from './storage';

describe('Persistent Storage', () => {
  let fs: MemFs;
  let storage: Storage;
  beforeEach(() => {
    fs = new MemFs();
    storage = new Storage(fs);
  });
  describe('persistent data (session)', () => {
    const appDataPath = '/app/Application Support/appData.json';
    it('can be stored', () => {
      return storage
        .store({
          eula: true,
          sessionKey: 'abc',
        })
        .then(() => {
          expect(fs.isFileExists(appDataPath)).toBeTruthy();
          expect(JSON.parse(fs.getFileContent(appDataPath))).toEqual({
            eula: true,
            sessionKey: 'abc',
          });
        });
    });
    it('returns empty data, if no data', () => {
      return storage.load().then((data) => {
        expect(data).toBeUndefined();
      });
    });
    it('returns empty data, if data broken', () => {
      fs.setFileContent(appDataPath, '}}} broken json {{{');
      return storage.load().then((data) => {
        expect(data).toBeUndefined();
      });
    });
    it('returns correct data', () => {
      fs.setFileContent(appDataPath, '{"sessionKey": "sk"}');
      return storage.load().then((data) => {
        expect(data).toEqual({sessionKey: 'sk'});
      });
    });
  });
});
