import 'jest';
import React from 'react';
import {NavigationContainerRef} from '@react-navigation/core';

import {clock} from '../testing/setupFakeTimers';
import {appProvider} from './appProvider';
import {MemFs} from './storage/memFs';
import Application from './application';

const navigation = React.createRef<NavigationContainerRef>();

beforeEach(() => {
  const fs = new MemFs();
  appProvider.setApplication(new Application());
  appProvider.application.bootstrap({fs, apiBase: 'api_host', navigation});
  clock.setSystemTime(Date.parse('2021-04-20T12:00:00Z'));
  clock.tick(10);
});
afterEach(() => {
  jest.clearAllMocks();
  clock.runToLast();
  appProvider.setApplication(undefined);
  clock.reset();
});
it('model init propertly', () => {
  expect(appProvider.application.model).toBeDefined();
});
