import React from 'react';
import {NavigationContainerRef} from '@react-navigation/core';
import {AppRegistry} from 'react-native';

import {Fs} from './storage/fs';
import {appProvider} from './appProvider';
import Application from './application';
import {name as appName} from '../app.json';
import App from './App';
import {API_DOMAIN_URL} from './constants/general';
import {configure} from 'mobx';

export function main() {
  //TODO update to react native 0.64 with proxy support for hermes (p.s: Hermes enabled in android)
  configure({
    useProxies: 'never',
  });

  const fs = new Fs();
  const app = new Application();
  appProvider.setApplication(app);
  appProvider.application.bootstrap({
    fs,
    navigation: React.createRef<NavigationContainerRef>(),
    console: __DEV__ ? console : undefined,
    apiBase: API_DOMAIN_URL,
  });
  AppRegistry.registerComponent(appName, () => App);
}
