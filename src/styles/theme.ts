import {Dimensions, Insets, PixelRatio, StyleSheet, ViewStyle} from 'react-native';

const mockupWidth = 375;
const windowSize = Dimensions.get('window');

const PT = windowSize.width / mockupWidth;
const slopD = PT * 15;
class Theme {
  /**
   * Point size in design document
   */
  pt: number = PT;

  /**
   * Window width
   */
  windowWidth: number = windowSize.width;

  /**
   * Window heigth
   */
  windowHeight: number = windowSize.height;

  /**
   *  Screen width padding
   */
  screenWidthPadding = this.aligned(12);

  /**
   * Window width
   */
  windowWidthWithPadding: number = this.windowWidth - (this.screenWidthPadding * 2);

  /**
   *  Header Height
   */
  headerHeight = this.aligned(155);

  /**
   *  Header Back Arrow Width
   */
  headerBackArrowWidth = this.aligned(46);

  /**
   *  Header Back Arrow Width
   */
  headerContextMenuWidth = this.aligned(46);

  /**
   * Main background color #232323
   */
  bgColor = 'rgb(35, 35, 35)';

  /**
   * Dark gray background color #302F2F
   */
  bgColorDarkGray = 'rgb(48, 47, 47)';

  /**
   * Vary Dark gray background color #272727
   */
  bgColorVaryDarkGray = 'rgb(39, 39, 39)';

  /**
   * Light gray background color #525252;
   */
  bgColorLightGray = 'rgb(82, 82, 82)';

  /**
   * Btn background color #747474
   */
  bgBtnLightGray = 'rgb(116, 116, 116)';

  /**
   * Btn background color #E49B0F
   */
  bgBtnOrange = 'rgb(228, 155, 15)';

  /**
   * Btn background color #393939
   */
  bgBtnInactive = 'rgb(57, 57, 57)';

  /**
   * DD Area background color #514F4F
   */
  bgDDArea = 'rgb(81, 79, 79)';

  /**
   * Lock content background color #33436E
   */
  bgLockContent = 'rgb(51, 67, 110)';

  /**
   * Spinner background color #E49B0F
   */
  bgSpinner = 'rgb(228, 155, 15)';

  /**
   * Check background color #606060
   */
  bgCheck = 'rgb(96, 96, 96)';

  /**
   * Bg input error #FBDBDB
   */
  lightPink = 'rgb(255, 232, 232)';

  /**
   * Border background color #ffffff
   */
  borderDDArea = 'rgb(255, 255, 255)';

  /**
   * Border background color #DCDCDC
   */
  borderStatisticsIntervalsBtn = 'rgba(220, 220, 220, 0.2)';

  /**
   * Border background color #F0F3FC
   */
  borderEducationArea = 'rgb(240, 243, 252)';

  /**
   * Border Calendar Inactive color #636363
   */
  borderCalendarInactive = 'rgb(99, 99, 99)';

  /**
   * Posted Video Chart color #DEBC93
   */
  postedVideoChart = 'rgb(222, 188, 147)';

  /**
   * Number Of Meet Chart color #CA9658
   */
  numberOfMeetingsChart = 'rgb(202, 150, 88)';

  /**
   * Number Of Touches Chart color #F7BE79
   */
  numberOfTouchesChart = 'rgb(246, 190, 121)';

  /**
   * Border background color #D6D6D6
   */
  borderCalendarArr = 'rgb(214, 214, 214)';

  /**
   * Border background color #575757
   */
  unknownBg = 'rgb(87, 87, 87)';

  /**
   * Border background color #474747
   */
  eventAvatarBg = 'rgb(71, 71, 71)';

  /**
   * Normal text color #fff
   */
  textColor = 'rgb(255, 255, 255)';

  /**
   * Description text color #C6C6C6
   */
  descColor = 'rgb(198, 198, 198)';

  /**
   * Title text color #fff
   */
  textTitleColor = 'rgb(255, 255, 255)';

  /**
   * Label text color #B8BECD
   */
  textLabelColor = 'rgb(184, 190, 205)';

  /**
   * Link text color #E49B0F
   */
  textLinkColor = 'rgb(228, 155, 15)';

   /**
   * Btn inactive text color #393939
   */
  textBtnInactive = 'rgb(118, 118, 118)';

  /**
   * Color for error text #e64646
   */
  textErrorColor = 'rgb(230,70,70)';

  /**
   * Color for error text #767676
   */
  textLockedColor = 'rgb(118,118,118)';

  /**
   * Placeholder color #909090
   */
  placeholderColor = 'rgb(144, 144, 144)';

  /**
   * Dark blue text H900 color #454246
   */
  textDarkBlueH900 = 'rgb(69, 66, 70)';

  /**
   * Secondary text gray color #BEBEBE
   */
  textSecondaryGray = 'rgb(190, 190, 190)';

  /**
   * Calendar Disabled Date Text Color text #FFFFFF
   */
  textDisabledDateTextColor = 'rgba(255, 255, 255, 0.2)';

  /**
   * Calendar Inactive Text Color text #585858
   */
  textInactiveColor = 'rgb(88, 88, 88)';

  /**
   * Map Pin/Business light blue color #8AE1EC
   */
  mapPinBusiness = 'rgb(138, 225, 236)';

  /**
   *  Border radius size for like card UI elements
   */
  cardBorderRadius = this.aligned(10);

  /**
   * Font-family for regular texts
   */
  fontFamilyRegular = 'EuclidCircularB-Regular';

  /**
   * Font-family for bold texts
   */
  fontFamilyBold = 'EuclidCircularB-SemiBold';

  /**
   * Font-family for medium texts
   */
  fontFamilyMedium = 'Poppins-Medium';

  /**
   * Font-family for check buttons texts
   */
  fontFamilyPoppins = 'Poppins-SemiBold';

  /**
   * shadow color #181818
   */
  shadowColor = '#181818';


  /**
   *  Space sizes
   */
  space8 = this.aligned(8);
  space10 = this.aligned(10);
  space12 = this.aligned(12);
  space14 = this.aligned(14);
  space16 = this.aligned(16);
  space18 = this.aligned(18);
  space20 = this.aligned(20);
  space22 = this.aligned(22);
  space24 = this.aligned(24);
  space26 = this.aligned(26);
  space28 = this.aligned(28);
  space30 = this.aligned(30);
  space42 = this.aligned(42);

  /**
   *  Font sizes
   */
  fontSize10 = this.pt * 10;
  fontSize12 = this.pt * 12;
  fontSize14 = this.pt * 14;
  fontSize16 = this.pt * 16;
  fontSize18 = this.pt * 18;
  fontSize20 = this.pt * 20;
  fontSize22 = this.pt * 22;
  fontSize24 = this.pt * 24;
  fontSize32 = this.pt * 32;

  /**
   *  start color for some gradients TODO sync with UI-Kit
   */
  bottomGradientStartColor = 'rgba(68, 68, 68, 0.12)';
  /**
   *  end color for some gradients TODO sync with UI-Kit
   */
  bottomGradientEndColor = 'rgba(68, 68, 68, 0.0)';

  /**
   * Screen header height
   */
  screenHeaderHeight: number = this.aligned(56);

  /**
   *  Function for more simpler convert from size in points to size in pixels
   */
  aligned(pt: number): number {
    return PixelRatio.roundToNearestPixel(this.pt * pt);
  }

  /**
   * Hitslop for small toouchable elements (icons, etc)
   */
  smallElementsHitSlop: Insets = {top: slopD, left: slopD, bottom: slopD, right: slopD};
}

const theme = new Theme();

export function withShadow(style: ViewStyle, dimensions: {x, y, r, o}, color?: string): ViewStyle {
  /** Shadow style object */
  const shadow = {
    shadowColor: color ? color : theme.shadowColor,
    shadowOffset: {
      width: dimensions.x ? theme.aligned(dimensions.x) : 0,
      height: dimensions.y ? theme.aligned(dimensions.y) : 0,
    },
    shadowRadius: dimensions.r ? theme.aligned(dimensions.r) : 0,
    shadowOpacity: dimensions.o ? dimensions.o : 1,
  };
  return StyleSheet.flatten([style, shadow]);
}
export default theme;
