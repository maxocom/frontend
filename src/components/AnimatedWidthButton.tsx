import React, {Component} from 'react';
import {
  Animated,
  Easing,
  GestureResponderEvent,
  Image,
  NativeScrollEvent,
  NativeSyntheticEvent,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import images from '../images';
import theme from '../styles/theme';

type Props = {
  text: string;
  onPress: (event: GestureResponderEvent) => void;
};
type State = {
  width: Animated.Value | null;
  viewText: boolean;
};

export class AnimatedWidthButton extends Component<Props, State> {
  private readonly animateDuration = 100;
  private readonly toWidth = theme.aligned(46);
  private fromWidth: number = 0;

  constructor(props) {
    super(props);
    this.state = {width: null, viewText: true};
  }

  componentDidMount() {}

  expand = () => {
    this.toggle(false);
  };

  collapse = () => {
    this.toggle(true);
  };

  private toggle(collapse: boolean) {
    if (collapse) {
      this.setState({viewText: false});
    }
    if (this.state.width != null) {
      Animated.timing(this.state.width, {
        toValue: collapse ? this.toWidth : this.fromWidth,
        duration: this.animateDuration,
        easing: Easing.linear,
        useNativeDriver: false,
      }).start();
    }
  }

  private onLayout = (event) => {
    const {width} = event.nativeEvent.layout;
    if (this.fromWidth == 0) {
      this.fromWidth = width;
      const animatedWidth = new Animated.Value(width);
      animatedWidth.addListener(({value}) => {
        if (value > width - 20) {
          this.setState({viewText: true});
        }
      });
      this.setState({width: animatedWidth});
    }
  };

  private onPress = (event: GestureResponderEvent) => {
    if (this.props.onPress) {
      this.props.onPress(event);
    }
  };

  render() {
    const {width} = this.state;

    const animatedStyle = width ? {width} : undefined;

    return (
      <Animated.View style={animatedStyle} onLayout={this.onLayout}>
        <TouchableOpacity style={styles.addBtn} onPress={this.onPress}>
          <Image source={images.add_space_btn} style={styles.addIcon} />
          {this.state.viewText && <Text style={styles.addText}>{this.props.text}</Text>}
        </TouchableOpacity>
      </Animated.View>
    );
  }

  static processOnScrollForCollapse(btn: AnimatedWidthButton | null, event: NativeSyntheticEvent<NativeScrollEvent>) {
    if (btn) {
      const nativeEvent = event.nativeEvent;
      if (nativeEvent.contentSize.height <= nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y) {
        return;
      }
      if (nativeEvent.contentOffset.y > theme.aligned(20)) {
        btn.collapse();
      } else {
        btn.expand();
      }
    }
  }
}

const styles = StyleSheet.create({
  addBtn: {
    backgroundColor: theme.tintColor,
    flexDirection: 'row',
    elevation: 5,
    height: theme.aligned(44),
    borderRadius: theme.aligned(60),
    alignItems: 'center',
    paddingRight: theme.space16,
  },
  addIcon: {
    resizeMode: 'contain',
    marginLeft: theme.space16,
    width: theme.aligned(14),
  },
  addText: {
    marginLeft: theme.space14,
    color: theme.lightGrayColor,
    fontFamily: theme.fontFamilyButton,
    fontSize: theme.fontSize14,
  },
});
