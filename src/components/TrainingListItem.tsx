import React from 'react';
import {observer} from 'mobx-react-lite';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import images from '../images';
import {Training} from '../models/training';
import {appProvider} from '../appProvider';
import Icon from "../elements/OwnIcons";

type Props = {
  training: Training;
};

export const TrainingListItem: React.FC<Props> = observer((props) => {
  const onPress = () => {
    if (!props.training.isDisabled) {
      const app = appProvider.application;
      app.navigateToTraining(props.training);
    }
  };

  const renderBlock = () => {
    if (props.training.isDisabled) {
      return (
        <View
          style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
        >
          <View style={styles.trainingsBlock}>
            <View style={styles.trainingsBlockText}>
              <Text style={styles.trainingsTitle}>{props.training.name}</Text>
            </View>
            <View style={styles.trainingsBlockImages}>
              <Image source={images.trainingBg} style={styles.trainingsBg} />
            </View>
            <View style={[styles.trainingsBlockBtn, styles.trainingsBlockBtnDisabled]}>
              <Icon style={styles.lockIcon} name={'lock'} size={theme.aligned(20)} />
            </View>
          </View>
        </View>
      );
    }

    return (
      <TouchableOpacity
        onPress={() => onPress()}
        style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
      >
        <View style={styles.trainingsBlock}>
          <View style={styles.trainingsBlockText}>
            <Text style={styles.trainingsTitle}>{props.training.name}</Text>
          </View>
          <View style={styles.trainingsBlockImages}>
            <Image source={images.trainingBg} style={styles.trainingsBg} />
          </View>
          <View style={styles.trainingsBlockBtn}>
            <Text style={styles.trainingsBtn}>Смотреть курс</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  console.log('TrainingListItem', props);

  return renderBlock();
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    height: theme.aligned(287),
    overflow: 'hidden',
    marginBottom: theme.aligned(30),
  },
  trainingsBlock: {
    alignSelf: 'stretch',
    position: 'relative',
    width: '100%',
    height: theme.aligned(287),
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  trainingsBlockText: {
    width: theme.aligned(200),
    marginTop: theme.aligned(62),
    marginLeft: theme.aligned(25),
  },
  trainingsTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize24,
    lineHeight: theme.space30,
    color: theme.textColor,
    width: theme.aligned(200),
  },
  trainingsBlockImages: {
    position: 'absolute',
    width: '100%',
    right: 0,
    left: 0,
    bottom: 0,
    height: theme.aligned(183),
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  trainingsBg: {
    height: theme.aligned(183),
    width: '100%',
  },
  trainingsBlockBtn: {
    marginTop: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: theme.aligned(64),
    backgroundColor: theme.bgBtnOrange,
  },
  trainingsBlockBtnDisabled: {
    backgroundColor: theme.bgLockContent,
  },
  trainingsBtn: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  lockIcon: {
    color: theme.textColor,
  },
  bottomSpacer: {
    height: theme.aligned(32),
  },
});
