import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Moment from 'moment-timezone';
import 'moment/min/locales';

import theme, {withShadow} from '../styles/theme';
import Icon from '../elements/OwnIcons';
import {Event} from '../models/event';
import {appProvider} from '../appProvider';

Moment.locale('ru');

type Props = {
  event: Event;
  key: string;
};

export const EventListItem: React.FC<Props> = observer((props) => {

  const onPress = () => {
    const app = appProvider.application;
    app.navigateToEvent(props.event);
  };

  return (
    <TouchableOpacity
      onPress={() => onPress()}
      style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}
    >
      <View style={styles.eventBlock}>
        <View style={styles.eventImageWrap}>
          <View style={styles.eventImageEmptyWrap}>
            <Icon name="emptyEvent" size={theme.aligned(32)} style={styles.eventImageEmptyIcon} />
          </View>
        </View>

        <View style={styles.eventContent}>
          <View style={styles.eventTitleWrap}>
            <Text style={styles.eventTitle}>{props.event.name}</Text>
          </View>

          <View style={styles.eventTimeWrap}>
            <Icon name="clock" size={theme.aligned(22)} style={styles.eventTimeIcon} />
            <Text style={styles.eventTime}>{Moment(props.event.time).tz('Europe/Moscow').format('DD MMM YYYY в HH:mm')}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    minHeight: theme.aligned(104),
    overflow: 'hidden',
    marginBottom: theme.aligned(30),
    padding: 12
  },
  eventBlock: {
    alignSelf: 'stretch',
    position: 'relative',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  eventImageWrap: {
    width: theme.aligned(80),
    height: theme.aligned(80),
    marginRight: theme.aligned(27),
  },
  eventImageEmptyWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.eventAvatarBg,
    width: theme.aligned(80),
    height: theme.aligned(80),
    borderRadius: theme.aligned(40),
  },
  eventImageEmptyIcon: {
    color: theme.borderDDArea,
  },
  eventImage: {
    width: theme.aligned(80),
    height: theme.aligned(80),
    borderRadius: theme.aligned(40),
  },
  eventContent: {
    justifyContent: 'flex-start',
    maxWidth: theme.aligned(220),
    flexGrow: 1,
  },
  eventTitleWrap: {
    marginBottom: theme.aligned(16),
    width: '100%',
  },
  eventTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor,
  },
  eventTimeWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  eventTimeIcon: {
    color: theme.bgBtnOrange,
    marginRight: theme.aligned(14),
  },
  eventTime: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLockedColor,
  },
});
