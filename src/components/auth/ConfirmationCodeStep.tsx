import React, {useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {CodeField} from 'react-native-confirmation-code-field';
import {observer, Observer} from 'mobx-react-lite';

import theme, {withShadow} from '../../styles/theme';
import {LoginController} from '../../models/loginController';
import {Screen} from '../../elements/Screen';
import {ScreenHeader} from '../../elements/ScreenHeader';
import {Button} from '../../elements/Button';

type Props = {
  ctrl: LoginController;
};

export const ConfirmationCodeStep: React.FC<Props> = observer((props: Props) => {
  useEffect(() => {
    return () => {
      props.ctrl.timer.clear();
    };
  }, [props.ctrl]);

  return (
    <Screen>
      <ScreenHeader title={'Регистрация'} />
      <View style={styles.container}>
        <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
          <View style={styles.codeInputBlock}>
            <CodeField
              autoFocus
              value={props.ctrl.confirmCode}
              onChangeText={props.ctrl.setConfirmCode}
              cellCount={4}
              rootStyle={styles.codeInput}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({index, symbol}) => (
                <Observer key={index}>{() => <CodeCell isError={props.ctrl.isError} symbol={symbol} first={index === 0} />}</Observer>
              )}
            />
          </View>
          <ConfirmationCodeButtonBlock ctrl={props.ctrl} />
          <Text style={styles.codeInputText}>{props.ctrl.isError ? 'Код неверный!' : 'Введите 4-х значный код из смс'}</Text>
        </View>
        <TimerBlock ctrl={props.ctrl} />
      </View>
    </Screen>
  );
});

type CellProps = {
  symbol: string;
  isError?: boolean;
  first: boolean;
};

const CodeCell: React.FC<CellProps> = (props) => {
  const style = [styles.symbol, props.isError ? styles.error : null];
  return (
    <View style={[styles.codeCell, props.first ? styles.firstCodeCell : null]}>
      <Text style={style}>{props.symbol}</Text>
    </View>
  );
};

const onPressNext = (props) => {
  if (props.ctrl.timer.currentSeconds > 0) {
    if (props.ctrl.codeReady) {
      props.ctrl.setConfirmCode();
    }
  } else {
    props.ctrl.onLogIn()
  }
};

const ConfirmationCodeButtonBlock: React.FC<Props> = observer((props) => {
  return (
    <View style={styles.buttonBlock}>
      <Button
        onPress={() => onPressNext(props)}
        text={`${(props.ctrl.timer.currentSeconds > 0) ? ((props.ctrl.codeReady && !props.ctrl.isError) ? 'Продолжить' : '0:' + props.ctrl.timer.currentSeconds) : 'Отправить повторно'}`}
        loading={props.ctrl.isLoading}
        disabled={props.ctrl.isLoading}
      />
    </View>
  );
});

const TimerBlock: React.FC<Props> = observer((props) => {
  if (props.ctrl.timer.currentSeconds > 0) {
    return null;
  } else {
    return (
      <TouchableOpacity onPress={props.ctrl.onResendCode} style={styles.resendLabel}>
        <Text style={styles.resendLabelText}>{'Не пришел код?'}</Text>
        <View style={styles.resendLabelLinkBlock}>
          <Text style={styles.resendLabelLink}>{'Отправить повторно'}</Text>
        </View>
      </TouchableOpacity>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
    width: theme.windowWidth,
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingVertical: theme.aligned(22),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: theme.aligned(25),
  },
  codeInputBlock: {
    alignSelf: 'stretch',
    width: '100%',
    marginBottom: theme.aligned(40),
    marginTop: theme.aligned(15),
  },
  codeInput: {
    alignItems: 'center',
    justifyContent: 'center',
    height: theme.aligned(40),
  },
  codeCell: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    width: theme.aligned(46),
    height: theme.aligned(69),
    borderBottomWidth: 3,
    borderBottomColor: theme.bgColorLightGray,
    marginLeft: theme.aligned(40),
  },
  firstCodeCell: {
    marginLeft: 0,
  },
  symbol: {
    textAlign: 'center',
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.aligned(40),
    lineHeight: theme.aligned(51),
    color: theme.textColor
  },
  error: {
    color: theme.textErrorColor
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
    marginBottom: theme.aligned(16),
  },
  codeInputText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLabelColor,
  },
  resendLabel: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: theme.aligned(19),
  },
  resendLabelText: {
    fontFamily: theme.fontFamilyRegular,
    color: theme.textColor,
    textAlign: 'center',
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
  },
  resendLabelLinkBlock: {
    borderBottomWidth: 1,
    borderBottomColor: theme.textLinkColor,
    paddingBottom: 3
  },
  resendLabelLink: {
    position: 'relative',
    fontFamily: theme.fontFamilyRegular,
    color: theme.textLinkColor,
    textAlign: 'center',
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,

  },

});
