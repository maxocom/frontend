import React from 'react';
import {observer} from 'mobx-react-lite';

import {LoginController} from '../../models/loginController';
import {StyleSheet, Text, View} from 'react-native';
import IntlPhoneInput from '../IntlPhoneInput';
import {appProvider} from '../../appProvider';
import {AppScreens} from '../../constants/screens';
import theme, {withShadow} from '../../styles/theme';
import {Screen} from '../../elements/Screen';
import {Button} from '../../elements/Button';
import {ScreenHeader} from '../../elements/ScreenHeader';

type Props = {
  ctrl: LoginController;
};

export const LoginFormStep: React.FC<Props> = observer((props: Props) => {
  return (
    <Screen>
      <ScreenHeader title={'Регистрация'} />
      <View style={styles.container}>
        <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
          <LoginInputBlock ctrl={props.ctrl} />
          <LoginButtonBlock ctrl={props.ctrl} />
        </View>
      </View>
      {renderTermsAndConditions()}
    </Screen>
  );
});

const LoginInputBlock: React.FC<Props> = observer((props) => {
  return (
    <View style={styles.inputBlock}>
      <Text style={styles.hintText}>Введите свой номер телефона</Text>
      <View style={styles.inputItem}>
        <IntlPhoneInput
          onChangeText={props.ctrl.setPhoneParts}
          defaultCountry={props.ctrl.defaultCountry}
        />
      </View>
    </View>
  );
});

const LoginButtonBlock: React.FC<Props> = observer((props) => {
  return (
    <View style={styles.buttonBlock}>
      <Button
        onPress={props.ctrl.onLogIn}
        text={'Отправить код'}
        disabled={!props.ctrl.hasChanges}
        loading={props.ctrl.isLoading}
      />
    </View>
  );
});

const renderTermsAndConditions = () => {
  return <View style={withShadow(styles.termsAndConditionsBlock, {x: 0, y: 0, r: 40, o: 1})}>
    <Text style={styles.termsAndConditionsText}>
      <Text>При регистрации вы принимаете </Text>
      <Text onPress={() => appProvider.application.navigate(AppScreens.Agreement)} style={styles.termsAndConditionsLink}>условия пользовательского соглашения</Text>
      <Text> и </Text>
      <Text onPress={() => appProvider.application.navigate(AppScreens.Rules)} style={styles.termsAndConditionsLink}>политику обработки персональных данных</Text>
    </Text>
  </View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.screenWidthPadding,
    justifyContent: 'flex-start',
  },
  content: {
    backgroundColor: theme.bgColorDarkGray,
    paddingHorizontal: theme.aligned(20),
    paddingVertical: theme.aligned(25),
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  inputBlock: {
    alignSelf: 'stretch',
    width: '100%',
    marginBottom: theme.aligned(16),
  },
  hintText: {
    width: '100%',
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor,
    textAlign: 'left',
    marginBottom: theme.aligned(12),
  },
  inputItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: theme.aligned(56),
  },
  buttonBlock: {
    width: '100%',
    height: theme.aligned(64),
  },
  termsAndConditionsBlock: {
    bottom: 0,
    left: 0,
    right: 0,
    paddingTop: theme.aligned(40),
    paddingBottom: theme.aligned(57),
    paddingHorizontal: theme.aligned(30),
    backgroundColor: theme.bgColorDarkGray,
    borderTopLeftRadius: theme.aligned(20),
    borderTopRightRadius: theme.aligned(20),
  },
  termsAndConditionsText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    fontWeight: 'normal',
    color: theme.textColor,
  },
  termsAndConditionsLink: {
    color: theme.textLinkColor,
  }
});
