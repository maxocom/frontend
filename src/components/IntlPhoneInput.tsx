import React, {Component} from 'react';
import {
  View,
  Text,
  Modal,
  FlatList,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import * as _ from 'lodash';

import {COUNTRIES, getCountry, CountryWithLangs} from '../../mocks/countries';
import Icon from '../elements/OwnIcons';

import theme from '../styles/theme';

type PhoneParts = {
  dialCode: string;
  unmaskedPhoneNumber: string;
  countryCode: string;
};

type Props = {
  lang?: string;
  defaultCountry: any;
  onChangeText: (part: PhoneParts) => void;
  disableCountryChange?: boolean;
  inputRef?: any,
  phoneNumber?: string,
  phoneInputStyle?: object,
  containerStyle?: object,
  inputProps?: any,
  placeholder?: string,
};

type State = {
  defaultCountry: any;
  flag: string;
  modalVisible: boolean;
  dialCode: string;
  countryCode: string;
  phoneNumber: string;
  mask: string;
  countryData: CountryWithLangs[],
};



export default class IntlPhoneInput extends Component<Props, State> {
  public filterCountriesInput: any;

  public state = {
    defaultCountry: {
      flag: '',
      code: '',
      dialCode: '',
      mask: '',
    },
    flag: '',
    modalVisible: false,
    dialCode: '',
    countryCode: '',
    phoneNumber: '',
    mask: '',
    countryData: COUNTRIES,
  };

  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    const {defaultCountry, phoneNumber} = this.props;

    const countryObj = _.get(defaultCountry, 'code', '') ? defaultCountry : getCountry('RU');
    let maskedPhoneNumber = '';

    if (phoneNumber) {
      maskedPhoneNumber = countryObj.mask.replace(/9/g, '_');

      for (let index = 0; index < phoneNumber.length; index += 1) {
        maskedPhoneNumber = maskedPhoneNumber.replace('_', phoneNumber[index]);
      }
    }

    this.setState({
      defaultCountry: countryObj,
      flag: countryObj.flag,
      dialCode: countryObj.dialCode,
      mask: countryObj.mask,
      phoneNumber: maskedPhoneNumber,
      countryCode: countryObj.code,
    });
  }

  onChangePropText = (unmaskedPhoneNumber: any, phoneNumber: any) => {
    const {dialCode, mask, countryCode} = this.state;
    const {onChangeText} = this.props;

    const countOfNumber = mask.match(/9/g).length;
    if (onChangeText) {
      const isVerified = countOfNumber === unmaskedPhoneNumber?.length && phoneNumber?.length > 0;
      onChangeText({
        dialCode,
        unmaskedPhoneNumber,
        phoneNumber,
        isVerified,
        countryCode,
      });
    }
  };

  onChangeText = (value: any) => {
    const {mask} = this.state;

    let unmaskedPhoneNumber = (value.match(/\d+/g) || []).join('');

    if (unmaskedPhoneNumber.length === 0) {
      this.setState({phoneNumber: ''});
      this.onChangePropText('', '');
      return;
    }

    let phoneNumber = mask.replace(/9/g, '_');
    for (let index = 0; index < unmaskedPhoneNumber.length; index += 1) {
      phoneNumber = phoneNumber.replace('_', unmaskedPhoneNumber[index]);
    }
    let numberPointer = 0;
    for (let index = phoneNumber.length; index > 0; index -= 1) {
      if (phoneNumber[index] !== ' ' && !isNaN(phoneNumber[index])) {
        numberPointer = index;
        break;
      }
    }
    phoneNumber = phoneNumber.slice(0, numberPointer + 1);
    unmaskedPhoneNumber = (phoneNumber.match(/\d+/g) || []).join('');

    this.onChangePropText(unmaskedPhoneNumber, phoneNumber);
    this.setState({phoneNumber});
  };

  showModal = () => {
    const {disableCountryChange} = this.props;

    if (!disableCountryChange) {
      this.setState({modalVisible: true}, () => {
        this.filterCountriesInput.blur();

        setTimeout(() => {
          this.filterCountriesInput.focus();
        }, 100);
      });
    }
  };

  hideModal = () => this.setState({modalVisible: false});

  onCountryChange = async (code: any) => {
    const {defaultCountry} = this.state;

    const countryData = await COUNTRIES;
    try {
      const country = await countryData.filter((obj) => obj.code === code)[0];
      this.setState({
        dialCode: country.dialCode,
        countryCode: country.code,
        flag: country.flag,
        mask: country.mask,
        phoneNumber: '',
      });
      this.hideModal();
    } catch (err) {
      this.setState({
        dialCode: defaultCountry.dialCode,
        countryCode: defaultCountry.code,
        flag: defaultCountry.flag,
        mask: defaultCountry.mask,
        phoneNumber: '',
      });
    }
  };

  filterCountries = (value: string) => {
    const {lang} = this.props;

    const countryData = COUNTRIES.filter(
      (obj: any) =>
        obj[lang?.toLowerCase() ?? 'ru']?.toLowerCase().indexOf(value.toLowerCase()) > -1 ||
        obj.dialCode.indexOf(value.toLowerCase()) > -1,
    );
    this.setState({countryData});
  };

  focus() {
    const {inputRef} = this.props;

    inputRef.current.focus();
  }

  renderModal = () => {
    const {modalVisible, countryData} = this.state;
    const {lang} = this.props;

    return (
      <Modal animationType="slide" transparent={false} visible={modalVisible}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.modalContainer}>
            <View style={styles.filterInputStyleContainer}>
              <TextInput
                ref={(ref) => {
                  this.filterCountriesInput = ref;
                }}
                onChangeText={this.filterCountries}
                placeholder={'Поиск'}
                style={styles.filterInputStyle}
              />
              <Text style={styles.searchIconStyle}>
                🔍
              </Text>
            </View>
            <FlatList
              style={{flex: 1}}
              data={countryData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <TouchableWithoutFeedback onPress={() => this.onCountryChange(item.code)}>
                  <View style={styles.countryModalStyle}>
                    <Text style={styles.modalFlagStyle}>
                      {item.flag}
                    </Text>
                    <View style={styles.modalCountryItemContainer}>
                      <Text style={styles.modalCountryItemCountryNameStyle}>
                        {item[lang?.toLowerCase() ?? 'ru']}
                      </Text>
                      <Text
                       
                        style={styles.modalCountryItemCountryDialCodeStyle}>{`  ${item.dialCode}`}</Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
            />
          </View>
          <TouchableOpacity onPress={() => this.hideModal()} style={styles.closeButtonStyle}>
            <Text style={styles.closeTextStyle}>
              {'Закрыть'}
            </Text>
          </TouchableOpacity>
        </SafeAreaView>
      </Modal>
    );
  };

  render() {
    const {mask, dialCode, phoneNumber} = this.state;
    const {containerStyle, phoneInputStyle, inputProps, placeholder} = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.showModal()}>
          <View style={[styles.openDialogView, containerStyle]}>
            <Text style={[styles.dialCodeTextStyle, phoneInputStyle]}>
              {dialCode}
            </Text>
            <Icon name="bottomArr" size={16} style={styles.openDialogViewIcon} />
          </View>
        </TouchableOpacity>
        {this.renderModal()}
        <View style={[styles.containerStyle, containerStyle]}>
          <TextInput
            {...inputProps}
            style={[styles.phoneInputStyle, phoneInputStyle]}
            placeholderTextColor={_.get(phoneInputStyle, 'color', styles.phoneInputStyle.color)}
            placeholder={placeholder || mask.replace(/9/g, '_')}
            autoCorrect={false}
            keyboardType="number-pad"
            secureTextEntry={false}
            value={phoneNumber}
            onChangeText={this.onChangeText}
           
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  closeTextStyle: {
    padding: 5,
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
  modalCountryItemCountryDialCodeStyle: {
    fontFamily: theme.fontFamilyRegular,
    color: '#000',
    fontSize: 16,
    lineHeight: 18,
    letterSpacing: 0.17,
  },
  modalCountryItemCountryNameStyle: {
    flex: 1,
    fontSize: 16,
  },
  modalCountryItemContainer: {
    flex: 1,
    paddingLeft: 5,
    flexDirection: 'row',
  },
  modalFlagStyle: {
    fontSize: 25,
  },
  modalContainer: {
    paddingTop: 15,
    paddingLeft: 25,
    paddingRight: 25,
    flex: 10,
    backgroundColor: 'white',
  },
  openDialogViewFlagStyle: {
    fontSize: 25,
  },
  openDialogViewIcon: {
    marginLeft: theme.aligned(13),
    color: theme.bgBtnOrange
  },
  flagStyle: {
    fontSize: 35,
  },
  dialCodeTextStyle: {
    fontFamily: theme.fontFamilyRegular,
    color: theme.textColor,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
  },
  countryModalStyle: {
    flex: 1,
    borderColor: 'black',
    borderTopWidth: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  openDialogView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: theme.aligned(77),
    backgroundColor: theme.bgColorLightGray,
    borderRadius:  theme.aligned(10),
  },
  filterInputStyle: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    color: '#424242',
  },
  searchIcon: {
    padding: 10,
  },
  filterInputStyleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
    backgroundColor: theme.bgColorLightGray,
    borderRadius:  theme.aligned(10),
    height: theme.aligned(56),
    width: theme.aligned(220),
  },
  phoneInputStyle: {
    fontFamily: theme.fontFamilyRegular,
    color: theme.textColor,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    width: '100%',
    height: '100%',
    paddingHorizontal: theme.aligned(16),
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: '100%',
  },
  searchIconStyle: {
    color: 'black',
    fontSize: 15,
    marginLeft: 15,
  },
  buttonStyle: {
    alignItems: 'center',
    padding: 14,
    marginBottom: 10,
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  countryStyle: {
    flex: 1,
    borderColor: 'black',
    borderTopWidth: 1,
    padding: 12,
  },
  closeButtonStyle: {
    padding: 12,
    alignItems: 'center',
  },
});
