import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View} from 'react-native';

import theme, {withShadow} from '../styles/theme';
import {TopUser} from '../models/topUser';
import Icon from '../elements/OwnIcons';
import {Team} from "../models/team";
import * as _ from 'lodash';

type Props = {
  top: TopUser;
  teams: Team[];
  index: number;
};

export const TopListItem: React.FC<Props> = observer((props) => {
  const getTeamName = (id: number | null) => {
    const team = _.find(props.teams, { id });
    console.log('getTeamName', team);
    return team?.name
  };

  return (
    <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})} >
      <View style={styles.topBlock}>
        <View style={styles.topBlockText}>
          <View style={styles.topTitle}>
            <View style={styles.topCount}>
              <Text style={styles.topCountNumber}>{props.index + 1}</Text>
            </View>
            <Text
              style={styles.topTitleName}
            >
              {props.top.lastName} {props.top.firstName}
            </Text>
          </View>
          <View style={styles.topInst}>
            <Icon name={'instagram'} size={theme.aligned(22)} style={styles.topInstIcon} />
            <Text style={styles.topInstName}>{props.top.instagramName}</Text>
          </View>
        </View>
        <View>
          {/*<Image source={{uri: props.top.avatar}} style={styles.topAvatar} />*/}
          <Icon name={'unknown'} size={theme.aligned(126)} style={styles.topAvatarIcon} />
        </View>
      </View>
      <View style={withShadow(styles.topBlockStat, {x: 0, y: 0, r: 40, o: 1}, theme.shadowColor)}>
        <View style={styles.topBlockStatWrap}>
          <Text style={styles.topBlockStatTitle}>Квалификация</Text>
          <Text style={styles.topBlockStatText}>RS</Text>
        </View>
        <View style={styles.topBlockStatWrap}>
          <Text style={styles.topBlockStatTitle}>Оборот</Text>
          <Text style={styles.topBlockStatText}>{props.top.amount} тыс. р.</Text>
        </View>
        <View style={styles.topBlockStatWrap}>
          <Text style={styles.topBlockStatTitle}>Команда</Text>
          <Text style={styles.topBlockStatText}>{getTeamName(props.top.team)}</Text>
        </View>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: theme.aligned(183),
    overflow: 'hidden',
    marginBottom: theme.aligned(30),
  },
  topBlock: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: theme.aligned(119),
    paddingLeft: theme.aligned(27),
  },
  topBlockText: {
    width: theme.aligned(200),
  },
  topTitle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: theme.aligned(13),
    width: theme.aligned(200),
  },
  topCount: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.aligned(28),
    height: theme.aligned(28),
    borderRadius: theme.aligned(14),
    backgroundColor: theme.bgBtnOrange,
    marginRight: theme.aligned(11),
  },
  topCountNumber: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  topTitleName: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize18,
    lineHeight: theme.space24,
    color: theme.textColor,
    width: theme.aligned(200),
  },
  topInst: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: theme.aligned(13),
    width: theme.aligned(200),
  },
  topInstIcon: {
    color: theme.bgBtnOrange,
    marginLeft: theme.aligned(5),
    marginRight: theme.aligned(14),
  },
  topInstName: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.placeholderColor,
  },
  topAvatar: {
    height: theme.aligned(119),
    width: undefined,
    resizeMode: 'contain',
    aspectRatio: 1,
  },
  topAvatarIcon: {
    color: theme.unknownBg,
  },
  topBlockStat: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    height: theme.aligned(64),
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    paddingVertical: theme.aligned(13),
    paddingHorizontal: theme.aligned(27),
  },
  topBlockStatWrap: {

  },
  topBlockStatTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLabelColor,
  },
  topBlockStatText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space22,
    color: theme.textColor,
  }
});
