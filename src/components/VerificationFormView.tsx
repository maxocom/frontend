import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, View} from 'react-native';

import {VerificationForm} from '../models/verificationForm';
import {FormInputLabel} from '../elements/FormInputLabel';
import {FormInputBlock} from '../elements/FormInputBlock';
import {FormInputImage} from '../elements/FormInputImage';
import {FormInputList} from '../elements/FormInputList';
import {FormInputDescription} from '../elements/FormInputDescription';
import {TeamsListsConfig} from '../types';

import theme from '../styles/theme';

type Props = {
  form: VerificationForm;
  teams: TeamsListsConfig;
};

export const VerificationFormView: React.FC<Props> = observer((props) => {
  const teamsList = props.teams.teams.teamList.map((team) => {
    return {
      label: team.name,
      value: team.id
    }
  });
  return (
    <>
      <FormInputLabel
        labelText={'Введите свой email'}
      />
      <FormInputBlock
        placeholder={'ivanovivan@gmail.com'}
        field={props.form.emailField}
        type="text"
        icon={'mail'}
      />
      <View style={styles.spacer} />
      {/*<FormInputLabel*/}
      {/*  labelText={'Название команды в структуре RS'}*/}
      {/*/>*/}
      {/*<FormInputList*/}
      {/*  field={props.form.teamField}*/}
      {/*  data={teamsList}*/}
      {/*  placeholder={'Выберите команду'}*/}
      {/*/>*/}
      {/*<View style={styles.spacer} />*/}
      <FormInputLabel
        labelText={'Никнейм в Instagram'}
      />
      <FormInputBlock
        placeholder={'ivanov'}
        field={props.form.instagramField}
        type="text"
        icon={'instagram'}
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Подтверждение  регистрации в структуре'}
      />
      <FormInputImage
        field={props.form.docField}
        icon={'plus'}
      />
      <FormInputDescription
        descriptionText={'Прикрепите скриншот оплаченной лицензии с вашими фио'}
      />
    </>
  );
});

const styles = StyleSheet.create({
  spacer: {
    height: theme.space20,
  },
});
