import React, {useState, useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import RenderHtml, { HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';

import theme, {withShadow} from '../styles/theme';
import Icon from '../elements/OwnIcons';
import {FaqItem} from '../models/faq';

type Props = {
  faqItem: FaqItem;
};

const customHTMLElementModels = {
  p: defaultHTMLElementModels.p.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
    },
    contentModel: HTMLContentModel.block
  }),
  a: defaultHTMLElementModels.a.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
    },
    contentModel: HTMLContentModel.block
  })
};

const tagsStyles = {
  body: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
};

const linkRenderer = ({
  TDefaultRenderer,
  ...props
}) => {
  const onPress = () => {
    console.log('linkRenderer');
  };
  return (
    <TDefaultRenderer
      {...props}
      onPress={onPress}
    />
  );
};

const renderers = {
  a: linkRenderer
};

export const FaqListItem: React.FC<Props> = observer((props) => {
  const faqItem = props.faqItem

  const [faqItemnOpened, setOpened] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setOpened(false);
    };
  }, []);

  const onPress = () => {
    setOpened(!faqItemnOpened);
  };

  const renderHeader = () => {
    return (
      <TouchableOpacity
        onPress={() => onPress()}
        style={styles.lessonBlock}
      >
        <View style={styles.lessonBlockText}>
          <Text style={styles.trainingsTitle}>{faqItem.title}</Text>
        </View>
        <View style={styles.lessonBlockImages}>
          {
            !!faqItemnOpened ? (
              <Icon style={styles.icon} name={'bottomArr'} size={theme.aligned(20)} />
            ) : (
              <Icon style={styles.icon} name={'nextArr'} size={theme.aligned(20)} />
            )
          }
        </View>
      </TouchableOpacity>
    );
  };

  const renderContent = () => {
    if (!faqItemnOpened) return null;

    return (
      <View style={styles.lessonWrap}>
        {/*<View style={styles.lessonWrapText}>*/}
        {/*  <Text style={styles.lessonWrapTitle}>Ответ</Text>*/}
        {/*</View>*/}
        <View style={styles.lessonWrapDesc}>
          <RenderHtml
            source={{html: faqItem.answer}}
            customHTMLElementModels={customHTMLElementModels}
            tagsStyles={tagsStyles}
          />
        </View>
      </View>
    );
  };

  return (
    <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
      {renderHeader()}
      {renderContent()}
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    minHeight: theme.aligned(72),
    overflow: 'hidden',
    marginBottom: theme.aligned(15),
  },
  lessonBlock: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  lessonBlockText: {
    width: theme.aligned(279),
    paddingHorizontal: theme.aligned(25),
  },
  trainingsTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  trainingsTitleLocked: {
    color: theme.textLockedColor,
  },
  lessonBlockImages: {
    width: theme.aligned(72),
    height: theme.aligned(72),
    backgroundColor: theme.bgBtnOrange,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: theme.aligned(8),
  },
  lessonBlockImagesLocked: {
    backgroundColor: theme.bgLockContent,
  },
  icon: {
    color: theme.textColor,
  },
  lessonWrap: {
    alignSelf: 'stretch',
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: theme.borderEducationArea,
    paddingVertical: theme.aligned(25),
    paddingHorizontal: theme.aligned(25),
  },
  lessonWrapText: {
    alignSelf: 'stretch',
    marginBottom: theme.aligned(12),
  },
  lessonWrapTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor,
  },
  lessonWrapDesc: {
    alignSelf: 'stretch',
    marginBottom: theme.aligned(20),
    color: theme.textColor,
  },
  lessonWrapDescText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
});
