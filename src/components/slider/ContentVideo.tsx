import React, {useState, useCallback, useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import YoutubePlayer from "react-native-youtube-iframe";
import { useIsFocused } from "@react-navigation/native";

import theme from '../../styles/theme';
import {GalleryContent} from '../../models/galleries';

type Props = {
  item: GalleryContent;
  index: number;
};

export const ContentVideo: React.FC<Props> = observer((props: Props) => {
  const item: GalleryContent = props.item;
  const index: number = props.index;
  const isFocused = useIsFocused();

  const [playing, setPlaying] = useState<boolean>(true);

  useEffect(() => {
    return () => {
      setPlaying(false);
    };
  }, [isFocused]);

  const onStateChange = useCallback((state) => {
    if (state === "ended") {
      setPlaying(false);
    }
  }, []);

  if (!item) return null;

  if (!item.active) {
      return (
        <View style={styles.loadingBlock}>
          <ActivityIndicator size="large" color={theme.bgSpinner} />
        </View>
      );
    }

  return (
    <View
        key={`galleries-video-${index}`}
        style={styles.slideWrap}
      >
        <View
          style={styles.imageItemWrap}
        >
          <YoutubePlayer
            height={theme.aligned(247)}
            play={playing}
            forceAndroidAutoplay={true}
            videoId={item.videoId}
            initialPlayerParams={{
              controls: false,
              showClosedCaptions: false
            }}
            onChangeState={onStateChange}
          />
        </View>

        <View
          style={styles.slideTextWrap}
        >
          <Text style={styles.slideText}>{item.description}</Text>
        </View>
      </View>
  );
});

const styles = StyleSheet.create({
  slideWrap: {
    width: theme.windowWidthWithPadding,
  },
  imageItemWrap: {
    height: theme.aligned(247),
    borderRadius: theme.aligned(8),
    marginBottom: theme.aligned(23),
  },
  slideTextWrap: {
  },
  slideText: {
    fontFamily: theme.fontFamilyMedium,
    fontSize: theme.fontSize14,
    lineHeight: theme.space24,
    color: theme.textColor,
    textAlign: 'center',
  },
  loadingBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    height: theme.aligned(247),
    backgroundColor: theme.bgColor,
  },
});
