import React from 'react';
import {observer} from 'mobx-react-lite';
import {View, StyleSheet, Text, ImageBackground} from 'react-native';

import theme from '../../styles/theme';
import {GalleryContent} from '../../models/galleries';

type Props = {
  item: GalleryContent;
  index: number;
};

export const ContentImage: React.FC<Props> = observer((props: Props) => {
  const item: GalleryContent = props.item;
  const index: number = props.index;

  if (!item) return null;

  return (
    <View
      key={`galleries-image-${index}`}
      style={styles.slideWrap}
    >
      <View
        style={styles.imageItemWrap}
      >
        <ImageBackground
          source={{uri: item.media, cache: 'force-cache'}}
          style={styles.imageItem}></ImageBackground>
      </View>

      <View
        style={styles.slideTextWrap}
      >
        <Text style={styles.slideText}>{item.description}</Text>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  slideWrap: {
    width: theme.windowWidthWithPadding,
  },
  imageItemWrap: {
    height: theme.aligned(247),
    borderRadius: theme.aligned(8),
    marginBottom: theme.aligned(23),
  },
  imageItem: {
    width: '100%',
    height: theme.aligned(247),
    resizeMode: 'cover',
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    overflow: 'hidden',
  },
  slideTextWrap: {
  },
  slideText: {
    fontFamily: theme.fontFamilyMedium,
    fontSize: theme.fontSize14,
    lineHeight: theme.space24,
    color: theme.textColor,
    textAlign: 'center',
  }
});
