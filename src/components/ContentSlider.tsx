import React, {useState, useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import {View, StyleSheet} from 'react-native';
import Carousel from 'react-native-snap-carousel';

import theme from '../styles/theme';
import {GalleryContent} from '../models/galleries';
import {ContentImage} from './slider/ContentImage';
import {ContentVideo} from './slider/ContentVideo';

type Props = {
  content: GalleryContent[];
};

export const ContentSlider: React.FC<Props> = observer((props: Props) => {
  const content: GalleryContent[] = props.content;
  const firstSlide = content[0];
  firstSlide.setActive(true);
  const [activeSlide, setActiveSlide] = useState<number>(0);

  if (!content) return null;

  const onChangeSlide = (index) => {
    const beforeSlide = content[activeSlide];
    beforeSlide.setActive(false);

    const slide = content[index];
    slide.setActive(true);

    setActiveSlide(index)
  };

  useEffect(() => {
    return () => {
      setActiveSlide(0);
      firstSlide.setActive(true);
    };
  }, []);

  const renderSlider = () => {
    return (
      <View>
        <Carousel
          data={content}
          renderItem={renderSliderItem}
          sliderWidth={theme.windowWidthWithPadding}
          itemWidth={theme.windowWidthWithPadding}
          onSnapToItem={(index) => onChangeSlide(index) }
          loop={false}
        />
      </View>
    );
  };

  const renderSliderItem = ({item, index}) => {
    if (!item) return null;
    switch (item.type) {
      case 'image':
        return <ContentImage item={item} index={index} />;
        break;

      case 'video':
        return <ContentVideo item={item} index={index} />;
        break;

      default:
        return null;
        break;
    }
  };

  return (
    <View style={styles.container} >
      {renderSlider()}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    marginBottom: theme.aligned(30),
  },
});
