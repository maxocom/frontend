import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, View} from 'react-native';

import {SendStatisticsForm} from '../models/sendStatisticsForm';
import {FormInputLabel} from '../elements/FormInputLabel';
import {FormInputBlock} from '../elements/FormInputBlock';

import theme from '../styles/theme';

type Props = {
  form: SendStatisticsForm;
};

export const StatisticFormView: React.FC<Props> = observer((props) => {
  return (
    <>
      <FormInputLabel
        labelText={'Отправлено видео'}
      />
      <FormInputBlock
        placeholder={'10'}
        field={props.form.postedVideoField}
        type="number"
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Количество встреч'}
      />
      <FormInputBlock
        placeholder={'10'}
        field={props.form.numberOfMeetingsField}
        type="number"
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Количество касаний'}
      />
      <FormInputBlock
        placeholder={'10'}
        field={props.form.numberOfTouchesField}
        type="number"
      />
      <View style={styles.spacer} />
    </>
  );
});

const styles = StyleSheet.create({
  spacer: {
    height: theme.space20,
  },
});
