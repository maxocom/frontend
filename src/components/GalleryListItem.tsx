import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Moment from 'moment-timezone';
import 'moment/min/locales';

import {appProvider} from '../appProvider';
import theme, {withShadow} from '../styles/theme';
import Icon from '../elements/OwnIcons';
import {Gallery} from '../models/galleries';

Moment.locale('ru');

type Props = {
  gallery: Gallery;
};

export const GalleryListItem: React.FC<Props> = observer((props) => {
  const gallery: Gallery = props.gallery;

  const onPress = () => {
    const app = appProvider.application;
    app.navigateToGallery(gallery);
  };

  const renderContent = () => {
    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View style={styles.eventBlock}>
          <View style={styles.eventImageWrap}>
            <View style={styles.eventImageEmptyWrap}>
              {/*<Icon name="clock" size={12} style={styles.eventImageEmptyIcon} />*/}
            </View>
          </View>

          <View style={styles.eventContent}>
            <View style={styles.eventTitleWrap}>
              <Text style={styles.eventTitle}>{gallery.name}</Text>
            </View>

            <View style={styles.eventTimeWrap}>
              <Icon name="clock" size={22} style={styles.eventTimeIcon} />
              <Text style={styles.eventTime}>{Moment(gallery.date).tz('Europe/Moscow').format('DD MMM YYYY в HH:mm')}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
      {renderContent()}
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    minHeight: theme.aligned(104),
    overflow: 'hidden',
    marginBottom: theme.aligned(30),
    padding: theme.aligned(12),
  },
  eventBlock: {
    alignSelf: 'stretch',
    position: 'relative',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  eventImageWrap: {
    width: theme.aligned(80),
    height: theme.aligned(80),
    marginRight: theme.aligned(27),
  },
  eventImageEmptyWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.bgCheck,
    width: theme.aligned(80),
    height: theme.aligned(80),
    borderRadius: theme.aligned(40),
  },
  eventImageEmptyIcon: {
    color: theme.bgBtnOrange,
  },
  eventImage: {
    width: theme.aligned(80),
    height: theme.aligned(80),
    borderRadius: theme.aligned(40),
  },
  eventContent: {
    justifyContent: 'flex-start',
    maxWidth: theme.aligned(220),
    flexGrow: 1,
  },
  eventTitleWrap: {
    marginBottom: theme.aligned(16),
    width: theme.aligned(200),
  },
  eventTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor,
  },
  eventTimeWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  eventTimeIcon: {
    color: theme.bgBtnOrange,
    marginRight: theme.aligned(14),
  },
  eventTime: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textLockedColor,
  },
});
