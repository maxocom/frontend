import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

import {appProvider} from '../appProvider';
import theme, {withShadow} from '../styles/theme';
import {LeaderStory} from '../models/leaderStories';
import Icon from "../elements/OwnIcons";

type Props = {
  leaderStory: LeaderStory;
};

export const LeaderStoriesListItem: React.FC<Props> = observer((props) => {
  const leaderStory = props.leaderStory;

  const onPress = () => {
    const app = appProvider.application;
    app.navigateToLeaderStory(leaderStory);
  };

  const renderContent = () => {
    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View style={styles.leaderBlock}>
          <View style={styles.leaderBlockText}>
            <View style={styles.leaderTitleWrap}>
              <Text style={styles.leaderTitle}>{leaderStory.firstName} {leaderStory.lastName}</Text>
            </View>
            <View style={styles.leaderBtnWrap}>
              <View style={styles.leaderBtn}>
                <Text style={styles.leaderBtnText}>Посмотреть</Text>
              </View>
            </View>
          </View>
          <View>
            {
              leaderStory.image ? (
                <View style={styles.leaderAvatarWrap}>
                  <Image source={{uri: leaderStory.image}} style={styles.leaderAvatar} />
                </View>
              ) : (
                <Icon name={'unknown'} size={theme.aligned(126)} style={styles.leaderAvatarIcon} />
              )
            }
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
      {renderContent()}
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    minHeight: theme.aligned(151),
    overflow: 'hidden',
    marginBottom: theme.aligned(30),
  },
  leaderBlock: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: theme.aligned(27),
    paddingRight: theme.aligned(13),
  },
  leaderBlockText: {
    width: theme.aligned(200),
  },
  leaderTitleWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: theme.aligned(22),
    width: theme.aligned(200),
  },
  leaderTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  leaderBtnWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: theme.aligned(200),
    height: theme.aligned(50),
  },
  leaderBtn: {
    paddingHorizontal: theme.aligned(32),
    paddingVertical: theme.aligned(15),
    borderRadius: theme.aligned(8),
    backgroundColor: theme.bgBtnOrange,
  },
  leaderBtnText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor,
  },
  leaderAvatarWrap: {
    height: theme.aligned(131),
    width: theme.aligned(111),
    borderRadius: theme.aligned(8),
    overflow: 'hidden',
  },
  leaderAvatar: {
    height: theme.aligned(131),
    width: undefined,
    resizeMode: 'contain',
    aspectRatio: 1,
  },
  leaderAvatarIcon: {
    color: theme.unknownBg,
  },
});
