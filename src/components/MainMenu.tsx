import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import * as _ from 'lodash';

import {AppScreens} from '../constants/screens';

import {appProvider} from '../appProvider';
import theme from '../styles/theme';

import Icon from '../elements/OwnIcons';

type Props = {
  activeScreen?: string;
};

export class MainMenu extends React.Component<Props> {
  public activeScreen: any;
  public spaceId: any;
  private app = appProvider.application;

  public state = {
    activeScreen: '',
  };

  constructor(props: any) {
    super(props);

    this.activeScreen = props.activeScreen;
    this.state.activeScreen = _.cloneDeep(this.activeScreen);
  }

  goToScreen = (screen: AppScreens, params?: any) => {
    this.app.navigate(screen, params);
  };

  render() {
    return (
      <View style={styles.block}>
        <TouchableOpacity style={styles.item} onPress={() => this.app.navigateToStatistics()}>
          <Icon name={'stat'} size={26} style={[styles.icon, (this.activeScreen === AppScreens.Statistics) && styles.activeIcon]} />
          <View style={styles.captionWrapper}>
            <Text
              maxFontSizeMultiplier={1.5}
              style={[styles.caption, (this.activeScreen === AppScreens.Statistics) && styles.activeCaption]}
            >
              Статистика
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item} onPress={() => this.app.navigateToTop()}>
          <Icon name={'top'} size={26} style={[styles.icon, (this.activeScreen === AppScreens.Top) && styles.activeIcon]} />
          <View style={styles.captionWrapper}>
            <Text
              maxFontSizeMultiplier={1.5}
              style={[styles.caption, (this.activeScreen === AppScreens.Top) && styles.activeCaption]}
            >
              Новости
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item} onPress={() => this.app.navigateToTrainings()}>
          <Icon name={'education'} size={26} style={[styles.icon, (this.activeScreen === AppScreens.Training) && styles.activeIcon]} />
          <View style={styles.captionWrapper}>
            <Text
              maxFontSizeMultiplier={1.5}
              style={[styles.caption, (this.activeScreen === AppScreens.Training) && styles.activeCaption]}
            >
              Обучение
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item} onPress={() => this.app.navigateToCalendar()}>
          <Icon name={'calendar'} size={26} style={[styles.icon, (this.activeScreen === AppScreens.Calendar) && styles.activeIcon]} />
          <View style={styles.captionWrapper}>
            <Text
              maxFontSizeMultiplier={1.5}
              style={[styles.caption, (this.activeScreen === AppScreens.Calendar) && styles.activeCaption]}
            >
              Календарь
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.item} onPress={() => this.app.navigateToProfileSettings()}>
          <Icon name={'settings'} size={26} style={[styles.icon, (this.activeScreen === AppScreens.ProfileSettings) && styles.activeIcon]} />
          <View style={styles.captionWrapper}>
            <Text
              maxFontSizeMultiplier={1.5}
              style={[styles.caption, (this.activeScreen === AppScreens.ProfileSettings) && styles.activeCaption]}
            >
              Профиль
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  block: {
    paddingHorizontal: theme.screenWidthPadding,
    paddingTop: theme.aligned(13),
    paddingBottom: theme.aligned(24),
    backgroundColor: theme.bgColorDarkGray,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: theme.windowWidth,
    height: theme.aligned(80),
  },
  item: {
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    color: theme.bgBtnLightGray,
  },
  activeIcon: {
    color: theme.bgBtnOrange,
  },
  captionWrapper: {
    alignSelf: 'center',
  },
  caption: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize10,
    lineHeight: theme.space14,
    color: theme.bgBtnLightGray,
    textAlign: 'center',
    marginTop: theme.aligned(4),
  },
  activeCaption: {
    color: theme.textLinkColor,
  },
});
