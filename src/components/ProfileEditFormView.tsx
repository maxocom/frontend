import React from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, View} from 'react-native';

import {ProfileEditForm} from '../models/profileEditForm';
import {FormInputLabel} from '../elements/FormInputLabel';
import {FormInputBlock} from '../elements/FormInputBlock';
import {FormInputImage} from '../elements/FormInputImage';
import theme from '../styles/theme';

type Props = {
  form: ProfileEditForm;
};

export const ProfileEditFormView: React.FC<Props> = observer((props) => {
  console.log('ProfileEditFormView', props);
  return (
    <>
      <FormInputLabel
        labelText={'Имя'}
      />
      <FormInputBlock
        placeholder={'Иван'}
        field={props.form.firstNameField}
        type="text"
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Фамилия'}
      />
      <FormInputBlock
        placeholder={'Иванов'}
        field={props.form.lastNameField}
        type="text"
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Почта'}
      />
      <FormInputBlock
        placeholder={'ivavovivan@gmail.com'}
        field={props.form.emailField}
        type="text"
        icon={'mail'}
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Никнейм в Instagram'}
      />
      <FormInputBlock
        placeholder={'ivanov'}
        field={props.form.instagramField}
        type="text"
        icon={'instagram'}
      />
      <View style={styles.spacer} />
      <FormInputLabel
        labelText={'Фотография'}
      />
      <FormInputImage
        field={props.form.avatarField}
        icon={'plus'}
      />
    </>
  );
});

const styles = StyleSheet.create({
  spacer: {
    height: theme.space20,
  },
});
