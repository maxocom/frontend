import React, {useState, useCallback, useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import RenderHtml, { HTMLContentModel, defaultHTMLElementModels } from 'react-native-render-html';
import YoutubePlayer from "react-native-youtube-iframe";

import theme, {withShadow} from '../styles/theme';
import Icon from '../elements/OwnIcons';
import {Lesson} from '../models/lesson';

type Props = {
  lesson: Lesson;
};

const customHTMLElementModels = {
  p: defaultHTMLElementModels.p.extend({
    mixedUAStyles: {
      fontFamily: theme.fontFamilyRegular,
      fontSize: theme.fontSize14,
      lineHeight: theme.space20,
      color: theme.textColor,
    },
    contentModel: HTMLContentModel.block
  })
};

export const LessonListItem: React.FC<Props> = observer((props) => {

  const [lessonOpened, setOpened] = useState<boolean>(false);
  const [playing, setPlaying] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setPlaying(false);
      setOpened(false);
    };
  }, []);

  const onStateChange = useCallback((state) => {
    if (state === "ended") {
      setPlaying(false);
    }
  }, []);

  const startPlaying = useCallback(() => {
    setPlaying(true);
  }, []);

  const onPress = () => {
    setOpened(!lessonOpened);
    setPlaying(false);
  };

  const renderHeader = () => {
    if (!props.lesson.seen_by_me) {
      return (
        <View style={styles.lessonBlock} >
          <View style={styles.lessonBlockText}>
            <Text style={[styles.trainingsTitle, styles.trainingsTitleLocked]}>{props.lesson.name}</Text>
          </View>
          <View style={[styles.lessonBlockImages, styles.lessonBlockImagesLocked]}>
            <Icon style={styles.icon} name={'lock'} size={theme.aligned(20)} />
          </View>
        </View>
      );
    }

    return (
      <TouchableOpacity
        onPress={() => onPress()}
        style={styles.lessonBlock}
      >
        <View style={styles.lessonBlockText}>
          <Text style={styles.trainingsTitle}>{props.lesson.name}</Text>
        </View>
        <View style={styles.lessonBlockImages}>
          {
            !!lessonOpened ? (
              <Icon style={styles.icon} name={'bottomArr'} size={theme.aligned(20)} />
            ) : (
              <Icon style={styles.icon} name={'nextArr'} size={theme.aligned(20)} />
            )
          }
        </View>
      </TouchableOpacity>
    );
  };

  const renderContent = () => {
    if (!lessonOpened) return null;

    return (
      <View style={styles.lessonWrap}>
        {
          props.lesson.description ? (
            <View style={styles.lessonWrapText}>
              <Text style={styles.lessonWrapTitle}>Описание</Text>
            </View>
          ) : null
        }

        {
          props.lesson.description ? (
            <View style={styles.lessonWrapDesc}>
              <RenderHtml
                source={{html: props.lesson.description}}
                customHTMLElementModels={customHTMLElementModels}
              />
            </View>
          ) : null
        }
        <View style={styles.lessonWrapText}>
          <Text style={styles.lessonWrapTitle}>Видеоурок</Text>
        </View>
        <View style={styles.lessonVideoWrap}>
          <YoutubePlayer
            height={theme.aligned(184)}
            play={playing}
            videoId={props.lesson.video}
            initialPlayerParams={{
              controls: playing,
              showClosedCaptions: false
            }}
            onChangeState={onStateChange}
          />
          {
            !playing && (
              <TouchableOpacity
                onPress={() => startPlaying()}
                style={styles.iconVideoPlay}
              >
                <Icon style={styles.icon} name={'videoPlay'} size={theme.aligned(56)} />
              </TouchableOpacity>
            )
          }
        </View>
      </View>
    );
  };

  return (
    <View style={withShadow(styles.content, {x: 0, y: 0, r: 40, o: 1})}>
      {renderHeader()}
      {renderContent()}
    </View>
  );
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.bgColorDarkGray,
    borderRadius: theme.aligned(8),
    width: '100%',
    minHeight: theme.aligned(72),
    overflow: 'hidden',
    marginBottom: theme.aligned(15),
  },
  lessonBlock: {
    alignSelf: 'stretch',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  lessonBlockText: {
    width: theme.aligned(279),
    paddingHorizontal: theme.aligned(25),
  },
  trainingsTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  trainingsTitleLocked: {
    color: theme.textLockedColor,
  },
  lessonBlockImages: {
    width: theme.aligned(72),
    height: theme.aligned(72),
    backgroundColor: theme.bgBtnOrange,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: theme.aligned(8),
  },
  lessonBlockImagesLocked: {
    backgroundColor: theme.bgLockContent,
  },
  icon: {
    color: theme.textColor,
  },
  lessonWrap: {
    alignSelf: 'stretch',
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: theme.borderEducationArea,
    paddingVertical: theme.aligned(25),
    paddingHorizontal: theme.aligned(25),
  },
  lessonWrapText: {
    alignSelf: 'stretch',
    marginBottom: theme.aligned(12),
  },
  lessonWrapTitle: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    color: theme.textLabelColor,
  },
  lessonWrapDesc: {
    alignSelf: 'stretch',
    marginBottom: theme.aligned(20),
  },
  lessonWrapDescText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space20,
    color: theme.textColor,
  },
  lessonVideoWrap: {
    backgroundColor: theme.bgColor,
    alignSelf: 'stretch',
    height: theme.aligned(184),
    borderRadius: theme.aligned(8),
    overflow: 'hidden',
  },
  iconVideoPlay: {
    position: 'absolute',
    alignSelf: 'stretch',
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 99
  }
});
