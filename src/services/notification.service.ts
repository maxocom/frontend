import {FirebaseMessagingTypes} from '@react-native-firebase/messaging';

import {appProvider} from '../appProvider';
import {AppScreens} from '../constants/screens';

export async function processNotification(remoteMessage: FirebaseMessagingTypes.RemoteMessage) {
  const app = appProvider.application;
  if (remoteMessage.data) {
    let screen: AppScreens = AppScreens.Trainings;
    if (remoteMessage.data.screen) {
      screen = remoteMessage.data.screen as AppScreens;

      console.log('Try open screen with data ', screen, JSON.stringify(remoteMessage.data));
      app.navigate(screen, remoteMessage.data);
    } else if (remoteMessage.data.channel) {
      const channel = JSON.parse(remoteMessage.data.channel);
      console.log('Channel ', channel);
    }
  } else {
    console.log('Push notification does not have any data; Open "Spaces". Data: ', JSON.stringify(remoteMessage.data));
    app.navigate(AppScreens.Trainings);
  }
}
