export enum AppScreens {
  Home = 'Home',
  LogIn = 'LogIn',
  Agreement = 'Agreement',
  Rules = 'Rules',
  Main = 'Main',
  Spirituality = 'Spirituality',
  AuthType = 'AuthType',
  Verification = 'Verification',
  Statistics = 'Statistics',
  SendStatistics = 'SendStatistics',
  Top = 'Top',
  Trainings = 'Trainings',
  Training = 'Training',
  Calendar = 'Calendar',
  Event = 'Event',
  ProfileSettings = 'ProfileSettings',
  Faq = 'Faq',
  LeaderStories = 'LeaderStories',
  LeaderStory = 'LeaderStory',
  About = 'About',
  Galleries = 'Galleries',
  Gallery = 'Gallery',
}
