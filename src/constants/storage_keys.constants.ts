export enum STORAGE_KEYS {
  FIRST_RUN = 'first_run',
  PHONE = 'auth_phone',
  SESSION_ID = 'auth_session_id',
  ACCESS_TOKEN = 'auth_access_token',
  REFRESH_TOKEN = 'auth_refresh_token',
  ACCESS_TOKEN_EXPIRED_AT = 'auth_access_token_expired_at',
  FCM_TOKEN_ID = 'fcm_token_id',
  PROFILE = 'me_profile',
  VERIFICATION = 'me_verification',
  SOCKET_URL = 'socket_domain',
}
