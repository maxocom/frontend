import {IDisposer} from 'mobx-utils';
import {reaction, when} from 'mobx';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

import {Storage} from './storage';
import {AppNavigationType, IApplicationConfig, ILogger, TeamsListsConfig, TrainingListsConfig, TopListsConfig, StatisticsConfig, CalendarConfig} from './types';
import {AppScreens} from './constants/screens';
import {Logger} from './logger';
import {Root} from './models/root';
import {Api} from './api/executor';
import {ApiError} from './api/error';
import {LoginController} from './models/loginController';
import {STORAGE_KEYS} from './constants/storage_keys.constants';
import * as api from './api';
import {Teams} from './models/teams';
import {Trainings} from './models/trainings';
import {Training} from './models/training';
import {Events} from './models/events';
import {Event} from './models/event';
import {Top} from './models/top';
import {Statistics} from './models/statistics';
import {Faq} from './models/faq';
import {About} from './models/about';
import {LeaderStories, LeaderStory} from './models/leaderStories';
import {Galleries, Gallery} from './models/galleries';
import {RootStackParamList} from './Routes';

import {ProfileEditForm} from './models/profileEditForm';
import {VerificationForm} from './models/verificationForm';
import {SendStatisticsForm} from './models/sendStatisticsForm';

export default class Application {
  private _bootstrapped: boolean = false;
  private _storage?: Storage;
  private _navigation?: AppNavigationType;
  private _logger: Logger = new Logger();
  private _model: Root;
  private _api?: Api;
  private navigationReadyDisposer?: IDisposer;
  private persistentDataSaveDisposer?: IDisposer;

  constructor() {
    this._model = new Root();
  }

  private createNavigationReadyReaction() {
    this.navigationReadyDisposer = when(
      () => this.model.navigationIsReady && this.model.processedInitialMessage,
      () => this.processAppStart(),
    );
  }

  private initPersistentData(): Promise<void> {
    return this.storage.load().then((data) => {
      this.model.load(data);
      if (this.persistentDataSaveDisposer) {
        this.persistentDataSaveDisposer();
        this.persistentDataSaveDisposer = undefined;
      }
      this.persistentDataSaveDisposer = reaction(
        () => this.model.serialize(),
        (dataForSave) => {
          this.storage.store(dataForSave);
        },
        {delay: 1000, fireImmediately: true},
      );
    });
  }

  get api(): Api {
    if (!this._api) throw new Error('Api not configured');
    return this._api;
  }

  get logger(): ILogger {
    return this._logger;
  }

  get model(): Root {
    return this._model;
  }

  get storage(): Storage {
    if (!this._storage) throw new Error('PersistentStorage not configured');
    return this._storage;
  }

  get navigation() {
    return this._navigation?.current || undefined;
  }

  get navigationRef() {
    return this._navigation;
  }

  bootstrap(config: IApplicationConfig) {
    if (this._bootstrapped) throw new Error('Application.bootstrap called on already bootstrapped instance');
    this._bootstrapped = true;
    this._navigation = config.navigation;
    this._storage = new Storage(config.fs);
    this._logger = new Logger({console: config.console});
    this._api = new Api({base: config.apiBase, onError: this.onApiError});
    this.model.setAppController();
    this.bootstrapAsync();
  }

  bootstrapAsync(): Promise<void> {
    return this.initPersistentData().then(() => {
      this.createNavigationReadyReaction();
    });
  }

  dispose() {
    if (this.persistentDataSaveDisposer) {
      this.persistentDataSaveDisposer();
      this.persistentDataSaveDisposer = undefined;
    }
    if (this.navigationReadyDisposer) {
      this.navigationReadyDisposer();
      this.navigationReadyDisposer = undefined;
    }
    this._modal.dispose();
  }

  processAppStart() {
    if (this.model.session.isAuthorized) {
      this.updateAccessToken().then(() => {
        this.getProfile().then(() => {
          this.navigateToStartScreens();
        });
      });
    } else {
      this.navigateToLogin();
    }
  }

  // Create Lists
  createTeamList(): TeamsListsConfig {
    return {
      teams: new Teams(),
    };
  }

  createStatistics(): StatisticsConfig {
    return {
      statistics: new Statistics(),
      user: this.model.profile,
    };
  }

  createTopList(): TopListsConfig {
    return {
      top: new Top(),
      teams: new Teams(),
    };
  }

  createTrainingsList(): TrainingListsConfig {
    return {
      trainings: new Trainings(),
    };
  }

  createEventsList(): CalendarConfig {
    return {
      events: new Events(),
      user: this.model.profile,
    };
  }

  // Load data
  loadTeams(teams: Teams) {
    const params: api.TeamsRequestParams = {
      limit: 100,
      offset: 0,
    };

    teams.clear();
    this.getTeams(teams, params);
  }

  loadEvents(events: Events) {
    events.dateEventsInterval;
    const params: api.EventsRequestParams = {
      limit: 100,
      offset: 0,
      from_date: events.dateFrom,
      to_date: events.dateTo,
    };
    events.clear();
    this.getEvents(events, params, true);
  }

  // Get data
  getTeams(teams: Teams, params: api.TeamsRequestParams): Promise<void> {
    teams.setLoading(true);
    return this.api
      .call('getTeams', {params})
      .then((res: api.TeamsResponse) => {
        teams.setFromApi(res.results);
        return;
      })
      .finally(() => teams.setLoading(false));
  }

  getStatistics(statistics: Statistics, params: api.StatisticParams): Promise<void> {
    statistics.setLoading(true);

    const user_id =  this.model.profile.id;
    params.user_id = user_id;

    return this.api
      .call('getStatistics', {token: this.model.session.accessToken, params})
      .then((res: api.StatisticsResponse) => {
        console.log('getStatistics', res.results);
        statistics.setFromApi(res.results);
      })
      .catch((e: ApiError) => {
        console.log('error', ApiError);
      })
      .finally(() => statistics.setLoading(false));
  }

  getTop(top: Top, params: api.TopUserRequestParams): Promise<void> {
    top.setLoading(true);
    return this.api
      .call('getTop', {params})
      .then((res: api.TopUserResponse) => {
        if (Array.isArray(res.results)) {
          top.setFromApi(res.results);
        }
        return;
      })
      .finally(() => top.setLoading(false));
  }

  getTrainings(trainings: Trainings): Promise<void> {
    trainings.setLoading(true);

    const params: api.TrainingsRequestParams = {
      limit: 100,
      offset: 0,
    };

    return this.api
      .call('getTrainings', {token: this.model.session.accessToken, params})
      .then((res: api.TrainingsResponse) => {
        if (Array.isArray(res.results)) {
          trainings.setFromApi(res.results);
        }
        return;
      })
      .finally(() => trainings.setLoading(false));
  }

  getTraining(training: Training, page?: number): Promise<void> {
    training?.setLoading(true);

    const params: api.TrainingsRequestParams = {
      limit: 10,
      offset: 0,
      page: page || 1,
      education_id: training.id
    };

    return this.api
      .call('getTraining', {token: this.model.session.accessToken, params})
      .then((res: api.LessonsResponse) => {
        console.log('getTraining', res);
        training.setLessons(res.results, res.count);
      })
      .finally(() => {
        training?.setLoading(false);
      });
  }

  getEvents(events: Events, params: api.EventsRequestParams, setUpcomingEvents?: boolean): Promise<void> {
    events.setLoading(true);
    return this.api
      .call('getEvents', {token: this.model.session.accessToken, params})
      .then((res: api.EventsResponse) => {
        events.setFromApi(res.results);
        if (setUpcomingEvents) {
          events.setUpcomingEvents(res.results);
        }
        return;
      })
      .finally(() => events.setLoading(false));
  }

  getEvent(id: number): Promise<Event | null> {
    const event = this.model.events.byId.get(id);
    event?.setLoading(true);
    return this.api
      .call('getEvent', {token: this.model.session.accessToken, id})
      .then((ev: api.Event) => {
        const loadedEvent = this.model.events.addFromApi(ev);
        return loadedEvent;
      })
      .finally(() => {
        event?.setLoading(false);
      });
  }

  getFaq(faq: Faq, params: api.PageParams): Promise<void> {
    faq.setLoading(true);
    return this.api
      .call('getFaq', {token: this.model.session.accessToken, params})
      .then((res: api.FaqResponse) => {
        faq.setFromApi(res.results);
        return;
      })
      .finally(() => faq.setLoading(false));
  }

  getAbout(about: About): Promise<void> {
    about.setLoading(true);
    return this.api
      .call('getAbout', {token: this.model.session.accessToken})
      .then((res: api.About) => {
        console.log('getAbout', res);
        const aboutRes: api.About = {
          text: res.results[0]?.description
        }
        console.log('setFromApi', aboutRes);
        about.setFromApi(aboutRes);
        return;
      })
      .finally(() => about.setLoading(false));
  }

  getLeaderStories(leaderStories: LeaderStories, params: api.PageParams): Promise<void> {
    leaderStories.setLoading(true);
    return this.api
      .call('getLeaderStories', {token: this.model.session.accessToken, params})
      .then((res: api.LeaderStoriesResponse) => {
        leaderStories.setFromApi(res.results);
        return;
      })
      .finally(() => leaderStories.setLoading(false));
  }

   getLeaderStory(id: number): Promise<LeaderStory | null> {
    const leaderStory = this.model.leaderStories.byId.get(id);
    leaderStory?.setLoading(true);
    return this.api
      .call('getLeaderStory', {token: this.model.session.accessToken, id})
      .then((ev: api.LeaderStory) => {
        const loadedLeaderStory = this.model.leaderStories.addFromApi(ev);
        return loadedLeaderStory;
      })
      .finally(() => {
        leaderStory?.setLoading(false);
      });
  }

  getGalleries(galleries: Galleries, params: api.PageParams): Promise<void> {
    galleries.setLoading(true);
    return this.api
      .call('getGalleries', {token: this.model.session.accessToken, params})
      .then((res: api.GalleriesResponse) => {
        console.log('getGalleries', res);
        galleries.setFromApi(res.results);
        return;
      })
      .finally(() => galleries.setLoading(false));
  }

   getGallery(id: number): Promise<Gallery | null> {
    const gallery = this.model.galleries.byId.get(id);
    gallery?.setLoading(true);
    return this.api
      .call('getGallery', {token: this.model.session.accessToken, id})
      .then((ev: api.Gallery) => {
        const loadedGallery = this.model.galleries.addFromApi(ev);
        return loadedGallery;
      })
      .finally(() => {
        gallery?.setLoading(false);
      });
  }

  // Send data
  sendStatistic(statistic: api.Statistic4Export) {
    return this.api
      .call('sendStatistic', {token: this.model.session.accessToken, statistic})
      .then((res) => {
        console.log('sendStatistic', res);
      });
  }

  sendRequestEvent(id: number) {
    const user_id =  this.model.profile.id;

    const data: api.SendEventsRequestParams = {
      user: user_id,
      events: id
    }

    return this.api
      .call('sendRequestEvent', {token: this.model.session.accessToken, data })
      .then((res) => {
        console.log('sendRequestEvent', res);
    });
  }

  // navigations
  navigate(screen: AppScreens, params?: Record<string, any>) {
    if (this.navigation?.getRootState()) {
      this.navigation.navigate(screen, params);
    } else {
      setTimeout(() => {
        this.navigate(screen, params);
      }, 100);
    }
  }

  navigateToStartScreens = () => {
    // if (this.model.profile?.isVerified) {
    //   this.navigateToTrainings();
    // } else if (this.model.profile?.isFilled) {
    //   this.navigateToTrainings();
    // } else {
    //   this.navigate(AppScreens.Main);
    // }
    this.navigate(AppScreens.Main);
  };

  navigateToBusiness = () => {
    if (this.model.profile?.isVerified) {
      this.navigateToTrainings();
    } else if (this.model.profile?.isFilled) {
      this.navigateToTrainings();
    } else {
      this.navigate(AppScreens.AuthType);
    }
  };

  navigateToLogin() {
    this.navigate(AppScreens.LogIn, {ctrl: new LoginController()});
  }

  navigateToAuthTypeScreen = (authType: string) => {
    switch (authType) {
      case 'demo':
        this.navigateToTrainings();
        break;

      case 'verified':
        this.navigateToVerification()
        break;

      default:
        this.navigateToTrainings();
        break;
    }
  };

  navigateToVerification() {
    const cfg = this.createTeamList();
    this.loadTeams(cfg.teams);
    this.navigate(AppScreens.Verification, {
      form: new VerificationForm(),
      cfg,
    });
  }

  navigateToStatistics() {
    const cfg = this.createStatistics();
    this.navigate(AppScreens.Statistics, {
      cfg
    });
  }

  navigateToStatistic() {
    const userId = this.model.profile.id;
    this.model.statistic.setUserId(userId);

    this.navigate(AppScreens.SendStatistics, {
      form: new SendStatisticsForm(),
      profile: this.model.profile
    });
  }

  navigateToTop() {
    const cfg = this.createTopList();
    this.loadTeams(cfg.teams);
    this.navigate(AppScreens.Top, {
      cfg
    });
  }

  navigateToTrainings() {
    const cfg = this.createTrainingsList();
    this.navigate(AppScreens.Trainings, {
      cfg
    });
  }

  navigateToTraining(training: Training, p?: Partial<RootStackParamList[AppScreens.Training]>) {
    const params: RootStackParamList[AppScreens.Training] = p ? {...p, training} : {training};
    this.navigate(AppScreens.Training, params);
  }

  navigateToCalendar() {
    const cfg = this.createEventsList();
    this.loadEvents(cfg.events);
    this.navigate(AppScreens.Calendar, {
      cfg
    });
  }

  navigateToEvent(event: Event, p?: Partial<RootStackParamList[AppScreens.Event]>) {
    const params: RootStackParamList[AppScreens.Event] = p ? {...p, event} : {event};
    this.navigate(AppScreens.Event, params);
  }

  navigateToProfileSettings() {
    this.getProfile();

    this.navigate(AppScreens.ProfileSettings, {
      form: new ProfileEditForm(),
    });
  }

  navigateToFaq() {
    const cfg = new Faq();
    this.navigate(AppScreens.Faq, {
      cfg
    });
  }

  navigateToLeaderStories() {
    const cfg = new LeaderStories();
    this.navigate(AppScreens.LeaderStories, {
      cfg
    });
  }

  navigateToLeaderStory(leaderStory: LeaderStory, p?: Partial<RootStackParamList[AppScreens.LeaderStory]>) {
    const params: RootStackParamList[AppScreens.LeaderStory] = p ? {...p, leaderStory} : {leaderStory};
    this.navigate(AppScreens.LeaderStory, params);
  }

  navigateToAbout() {
    const cfg = new About();
    this.navigate(AppScreens.About, {
      cfg
    });
  }

  navigateToGalleries() {
    const cfg = new Galleries();
    this.navigate(AppScreens.Galleries, {
      cfg
    });
  }

  navigateToGallery(gallery: Gallery, p?: Partial<RootStackParamList[AppScreens.Gallery]>) {
    const params: RootStackParamList[AppScreens.Gallery] = p ? {...p, gallery} : {gallery};
    this.navigate(AppScreens.Gallery, params);
  }

  logIn(ctrl: LoginController) {
    console.log('logIn');
    // TODO to remove (added for compability with current asyncStorage state)
    AsyncStorage.setItem(STORAGE_KEYS.PHONE, ctrl.phoneNumber);


    const requestParams: api.RequestOtpParams = {phone_number: ctrl.phoneNumber};

    return this.api
      .call('requestOtp', requestParams)
      .then((res) => {
        ctrl.setConfirmCodeStep();
      })
      .catch((e: ApiError) => {
        console.log('e', e.code);
        console.log('e', e.httpStatus);
        console.log('e', e.httpStatusText);
        console.log('e', e.details);
        if (e.details?.message === 'No sms.') {
          ctrl.setConfirmCodeStep();
        } else {
          this.alertError('Ошибка входа', e);
        }
      });
  }

  resendLoginCode(ctrl: LoginController) {
    console.log('resendLoginCode');
    const requestParams: api.RequestOtpParams = {phone_number: ctrl.phoneNumber};
    console.log('requestParams', requestParams);

    return this.api
      .call('resendOtp', requestParams)
      .then((res) => {
        console.log('requestOtp', res);
      })
      .catch((e: ApiError) => {
        this.alertError('Ошибка входа', e);
      });
  }

  confirmLoginCode(ctrl: LoginController) {
    console.log('resendLoginCode');
    const requestParams: api.SessionParams = {phone_number: ctrl.phoneNumber, code: ctrl.confirmCode};

    return this.api
      .call('checkOtp', requestParams)
      .then((res) => {
        console.log('confirmLoginCode', res);
        this.model.session.setTokens({access_token: res.access, refresh_token: res.refresh});
        this.model.profile.setFromApi(res.user);
      })
      .catch((e: ApiError) => {
        throw e;
      });
  }

  updateAccessToken() {
    const requestParams: api.TokenRefreshParams = {refresh: this.model.session.refreshToken};
    return this.api
      .call('refreshToken', requestParams)
      .then((res) => {
        this.model.session.setAccessToken({access_token: res.access});
      })
      .catch((e: ApiError) => {
        this.logout();
        throw e;
      });
  }

  getProfile() {
    return this.api
      .call('getProfile', this.model.session.accessToken)
      .then((profile) => {
        console.log('getProfile', profile);
        this.model.profile.setFromApi(profile);
      })
      .catch((e) => {
        this.logger.error('On get profile', e);
        this.logout();
      });
  }

  updateUser(user: api.User4Export) {
    const userId = this.model.profile.id;

    return this.api
      .call('updateUser', {token: this.model.session.accessToken, user, userId})
      .then((profile) => {
        this.getProfile()
      });
  }

  sendVerification(verification: api.Verification4Export) {
    const userId = this.model.profile.id;

    return this.api
      .call('sendVerification', {token: this.model.session.accessToken, verification, userId})
      .then((profile) => {
        this.model.profile.setFromApi(profile);
        this.getProfile()
      });
  }

  uploadFile(file: string) {
    return this.api
      .call('uploadFile', {token: this.model.session.accessToken, file})
      .then((mediaFile: api.MediaFile) => {
        return mediaFile
      });
  }

  updateNotificationToken() {
    return messaging()
      .requestPermission()
      .then((authStatus) => {
        console.log('AUTHORIZED', authStatus === messaging.AuthorizationStatus.AUTHORIZED);
        if (
          authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
          authStatus === messaging.AuthorizationStatus.PROVISIONAL
        ) {
          console.log('messaging');
          messaging()
            .getToken()
            .then((token) => {
              console.log('token', token);
              const fcmTokenId = this.model.session.fcmTokenId;
              if (fcmTokenId) {
                this.updateFcmRegistration(token);
              } else {
                this.createFcmRegistration(token);
              }
            });
        }
      })
      .catch(() => false);
  }

  createFcmRegistration(fcmToken?: string) {
    if (!fcmToken) {
      this.logger.log('Failed', 'No token received');
      return;
    }
    const user_id:number =  this.model.profile.id;

    const data:api.FcmRegistrationData = {
      user: user_id,
      push_token: fcmToken,
    };

    this.api
      .call('createFcmRegistration', {token: this.model.session.accessToken, data})
      .then((res:api.FcmRegistrationResponse) => {
        if (res?.push_token) {
          this.model.session.setFcmToken(res);
        }
      })
      .catch((e) => {
        //TODO change server (no errors, if token already saved)
        this.logger.error('Can not save token to CoreService', e);
      });
  }

  updateFcmRegistration(fcmToken?: string) {
    if (!fcmToken) {
      this.logger.log('Failed', 'No token received');
      return;
    }
    const user_id:number =  this.model.profile.id;
    const fcmTokenId: number = this.model.session.fcmTokenId;

    const data:api.FcmRegistrationData = {
      user: user_id,
      push_token: fcmToken,
    };

    this.api
      .call('updateFcmRegistration', {token: this.model.session.accessToken, data, fcmTokenId})
      .then((res:api.FcmRegistrationResponse) => {
        console.log('updateFcmRegistration', res);
        if (res?.push_token) {
          this.model.session.setFcmToken(res);
        }
      })
      .catch((e) => {
        //TODO change server (no errors, if token already saved)
        this.logger.error('Can not save token to CoreService', e);
      });
  }

  removeFcmRegistration(fcmTokenId?: number): Promise<void> {
    this.api
      .call('removeFcmRegistration', {token: this.model.session.accessToken, fcmTokenId})
      .then((res:api.FcmRegistrationResponse) => {
        return true;
      })
      .catch(() => {
        return true;
      });
  }

  syncUserData() {
    this.updateNotificationToken();
  }

  alertError(title: string, e: ApiError) {
    Alert.alert(title, e.message ?? e, [{text: 'Закрыть'}], {
      cancelable: false,
    });
  }

  logout() {
    const fcmTokenId: number | null = this.model.session.fcmTokenId;
    if (fcmTokenId) {
      this.api
        .call('removeFcmRegistration', {token: this.model.session.accessToken, fcmTokenId})
        .then(() => {
          this.logoutClear();
        })
        .catch(() => {
          this.logoutClear();
        });
    } else {
      this.logoutClear();
    }
  }

  logoutClear() {
    this.model.clear().then((result) => {
      this.navigateToLogin();
    });
  }

  private onApiError = (e: ApiError) => {
    // TODO
    this.logger.error(e.message);
  };
}
