import accountTypeBg from '../images/account_type_bg/account_type_bg.png';
import accountTypeDemo from '../images/account_type_demo/account_type_demo.png';
import accountTypePartners from '../images/account_type_partners/account_type_partners.png';
import trainingBg from '../images/training_bg/training_bg.png';
import mainBusiness from '../images/main_business/main_business.png';
import mainFinancial from '../images/main_financial/main_financial.png';
import mainSpirituality from '../images/main_spirituality/main_spirituality.png';
import mainTravel from '../images/main_travel/main_travel.png';

const images = {
  accountTypeBg,
  accountTypeDemo,
  accountTypePartners,
  trainingBg,
  mainBusiness,
  mainFinancial,
  mainSpirituality,
  mainTravel,
};

export default images;
