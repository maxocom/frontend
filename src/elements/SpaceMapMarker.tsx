import React from 'react';
import {Marker} from 'react-native-maps';
import {Image, StyleSheet, Text, View} from 'react-native';
import {expr} from 'mobx-utils';
import {observer} from 'mobx-react-lite';

import images from '../images';
import {Space} from '../models/space';
import {Location} from '../types';
import {SpaceType} from '../api';
import theme from '../styles/theme';

type Props = {
  space: Space;
  onSelect: (space: Space) => void;
};

export const SpaceMapMarker: React.FC<Props> = observer((props: Props) => {
  const coordinate: Location = expr(() => ({latitude: props.space.lat, longitude: props.space.lng}));
  const markerImage = expr(() => {
    switch (props.space.type) {
      case SpaceType.Community:
        return images.marker_community;
      case SpaceType.Business:
        return images.marker_business;
      case SpaceType.SelfEmployed:
        return images.marker_self_employed;
      default:
        return null;
    }
  });
  if (!markerImage) return null;

  const onSelect = () => {
    props.onSelect(props.space);
  };

  return (
    <Marker coordinate={coordinate} onPress={onSelect}>
      <View style={styles.container}>
        <Image source={markerImage} style={styles.image} />
        <Text maxFontSizeMultiplier={1.5} style={styles.text} numberOfLines={1} ellipsizeMode="tail">
          {props.space.name}
        </Text>
      </View>
    </Marker>
  );
});

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
    maxWidth: theme.aligned(150),
  },
  image: {
    resizeMode: 'contain',
    width: theme.aligned(25),
    height: undefined,
    aspectRatio: 1,
    alignSelf: 'center',
  },
  text: {
    fontFamily: theme.fontFamilyInter,
    fontWeight: '500',
    fontSize: theme.aligned(10),
    lineHeight: theme.aligned(12),
    color: theme.mainDarkH600,
  },
});
