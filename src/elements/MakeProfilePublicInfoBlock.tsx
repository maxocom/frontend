import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';

import images from '../images';
import Icon from './OwnIcons';
import React from 'react';
import theme from '../styles/theme';

import {Profile} from '../models/profile';
import {Button} from './Button';

type Props = {
  profile: Profile;
  onClose: () => void;
};

export const MakeProfilePublicInfoBlock: React.FC<Props> = observer((props) => {
  const renderCover = () => {
    return <FastImage source={images.make_profile_public_pic} style={styles.modalImage} />;
  };

  const renderHeader = () => {
    return (
      <TouchableOpacity style={styles.header} onPress={props.onClose}>
        <Icon name="close" size={16} style={styles.closeIcon} />
      </TouchableOpacity>
    );
  };

  const renderRow = (text: string) => {
    return (
      <View style={styles.row}>
        <Icon name="done" size={19} style={styles.doneIcon} />

        <Text style={styles.rowText}>{text}</Text>
      </View>
    );
  };

  const renderMainBlock = () => {
    return (
      <View style={styles.mainBlock}>
        {renderCover()}

        <Text style={styles.title}>{strings('common.modals.make_profile_public.title')}</Text>

        <Text style={styles.orderText}>{strings('common.modals.make_profile_public.description')}</Text>

        {renderRow(strings('common.modals.make_profile_public.rows.1'))}
        {renderRow(strings('common.modals.make_profile_public.rows.2'))}
        {renderRow(strings('common.modals.make_profile_public.rows.3'))}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {renderHeader()}

      {renderMainBlock()}

      <Button
        onPress={props.onClose}
        text={strings('common.modals.make_profile_public.continue')}
        style={styles.continueButton}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    paddingVertical: theme.space20,
    paddingHorizontal: theme.space16,
    minHeight: theme.windowHeight * 0.5,
    flex: 1,
    backgroundColor: theme.bgColor,
    borderTopLeftRadius: theme.aligned(12),
    borderTopRightRadius: theme.aligned(12),
  },
  header: {
    marginLeft: 'auto',
  },
  closeIcon: {
    color: theme.grayIconColor,
  },
  mainBlock: {
    paddingVertical: theme.aligned(2),
    paddingHorizontal: theme.aligned(18),
  },
  modalImage: {
    width: '100%',
    height: theme.aligned(194),
  },
  title: {
    fontFamily: theme.fontFamilyInter,
    fontSize: theme.aligned(18),
    lineHeight: theme.aligned(22),
    fontWeight: 'bold',
    color: theme.textColor,
    paddingTop: theme.space20,
  },
  orderText: {
    fontFamily: theme.fontFamilyInter,
    fontSize: theme.aligned(15),
    lineHeight: theme.aligned(19),
    fontWeight: 'normal',
    color: theme.textColor,
    paddingTop: theme.space8,
    paddingBottom: theme.space20,
  },
  row: {
    flexDirection: 'row',
    paddingBottom: theme.space12,
  },
  doneIcon: {
    color: theme.mapPinBusiness,
    marginRight: theme.aligned(15),
  },
  rowText: {
    fontFamily: theme.fontFamilyInter,
    fontSize: theme.aligned(14),
    lineHeight: theme.aligned(17),
    fontWeight: 'normal',
    color: theme.textDarkBlueH900,
  },
  continueButton: {
    marginTop: theme.aligned(28),
    marginBottom: theme.aligned(50),
  },
});
