import React, {ReactNode} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import theme from '../styles/theme';

type Props = {
  subTitle?: string | ReactNode;
  title: string;
  position?: string;
};

export const ScreenHeaderTitle: React.FC<Props> = (props) => {
  if (!props.subTitle) {
    return (
      <Text style={[styles.title, (props.position === 'center') && styles.centerTitle]} numberOfLines={2}>
        {props.title}
      </Text>
    );
  }
  const SubTitleElement: any = !props.subTitle || typeof props.subTitle === 'string' ? Text : View;
  return (
    <View>
      <SubTitleElement style={styles.subTitle}>{props.subTitle}</SubTitleElement>
      <Text style={[styles.title, (props.position === 'center') && styles.centerTitle]}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    color: theme.textTitleColor,
    fontSize: theme.fontSize32,
    lineHeight: theme.space42,
    fontWeight: 'bold',
    fontFamily: theme.fontFamilyBold,
    textAlign: 'left',
  },
  centerTitle: {
    fontSize: theme.fontSize20,
    lineHeight: theme.space26,
    textAlign: 'center',
  },
  subTitle: {
    color: theme.textColor,
    fontSize: theme.fontSize14,
    fontWeight: 'normal',
    fontFamily: theme.fontFamilyRegular,
    textAlign: 'left',
    marginBottom: theme.aligned(1),
  },
});
