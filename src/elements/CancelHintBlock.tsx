import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';


import theme from '../styles/theme';

type Props = {
  onPress: () => void;
};

export const CancelHintBlock: React.FC<Props> = (props: Props) => (
  <TouchableOpacity onPress={props.onPress} style={styles.block}>
    <Text maxFontSizeMultiplier={1.5} style={styles.text}>Закрыть</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  block: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: theme.screenWidthPadding,
  },
  text: {
    fontFamily: theme.fontFamilyInter,
    fontWeight: '500',
    fontSize: theme.fontSize14,
    color: theme.blueH800Color,
  },
});
