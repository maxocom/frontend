import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import theme from '../styles/theme';
import {observer} from 'mobx-react-lite';

type Props = {
  labelText: string;
};

export const FormInputLabel: React.FC<Props> = observer((props) => {
  return (
    <View style={styles.row}>
      <Text maxFontSizeMultiplier={1.5} style={styles.text}>
        {props.labelText}
      </Text>
    </View>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: theme.aligned(12),
    justifyContent: 'flex-start',
    width: '100%'
  },
  text: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    flexShrink: 1,
    color: theme.textLabelColor
  },
});
