import {StyleSheet, Switch, Text, View, ViewStyle} from 'react-native';
import {observer} from 'mobx-react-lite';

import theme from '../styles/theme';
import React from 'react';
import {appProvider} from '../appProvider';

import {updateSpace} from '../services/spaces.service';
import * as api from '../api';

type Props = {
  hintText?: string;
  style?: ViewStyle;
};

export const ProfilePublicSelector: React.FC<Props> = observer((props: Props) => {
  const profile = appProvider.application.model.profile;

  const onChange = () => {
    // TODO use updateSpace from codecs.spaces.ts
    const spaceData = new FormData();
    spaceData.append(`space[enabled]`, !profile.isPublic);
    updateSpace(spaceData, profile.profileSpaceId).then((space: api.Space) => {
      appProvider.application.syncUserSpace(space);
    });
  };

  const renderHintText = () => {
    if (!props.hintText) return null;
    return (
      <Text maxFontSizeMultiplier={1.5} style={styles.hintBlock}>
        {props.hintText}
      </Text>
    );
  };

  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.row}>
        <Switch
          trackColor={{true: theme.tintColor, false: theme.bgColorLightGray}}
          value={profile.isPublic}
          onValueChange={onChange}
        />
        <Text maxFontSizeMultiplier={1.5} style={styles.switchText}>
          {profile.isPublic ? strings('profile.rows.public.label_enabled') : strings('profile.rows.public.label')}{' '}
        </Text>
      </View>
      {renderHintText()}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  switchText: {
    fontFamily: theme.fontFamilyInter,
    color: theme.textColor,
    fontSize: theme.fontSize14,
    lineHeight: theme.fontSize18,
    fontWeight: '500',
    marginLeft: theme.aligned(12),
  },
  hintBlock: {
    marginTop: theme.aligned(10),
    fontFamily: theme.fontFamilyInter,
    color: theme.grayIconColor,
    fontSize: theme.fontSize12,
    lineHeight: theme.fontSize16,
    fontWeight: '500',
  },
});
