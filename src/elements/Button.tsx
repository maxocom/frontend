import React from 'react';
import {ActivityIndicator, StyleProp, StyleSheet, Text, TouchableOpacity, ViewStyle} from 'react-native';

import theme from '../styles/theme';

type Props = {
  onPress: () => void;
  loading?: boolean;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
  text: string;
  withShadow?: boolean;
};

export const Button: React.FC<Props> = (props: Props) => {
  const disabled = props.disabled || props.loading;

  const renderLoading = () => {
    if (!props.loading) return null;
    return <ActivityIndicator animating size="small" color={theme.textBtnInactive} style={styles.rightMargin} />;
  };

  const style = StyleSheet.flatten([
    styles.button,
    props.style ? props.style : null,
    disabled ? styles.disabled : null,
  ]);

  return (
    <TouchableOpacity onPress={props.onPress} style={style} disabled={disabled}>
      <Text maxFontSizeMultiplier={1.5} style={[styles.text, disabled && styles.disabledText]}>
        {renderLoading()}
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: '100%',
    backgroundColor: theme.bgBtnOrange,
    borderRadius: theme.aligned(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: theme.textColor,
    fontSize: theme.fontSize16,
    lineHeight: theme.space20,
    textAlign: 'center',
  },
  rightMargin: {
    marginRight: theme.space10,
  },
  disabled: {
    backgroundColor: theme.bgBtnInactive,
  },
  disabledText: {
    color: theme.textBtnInactive,
  },
});
