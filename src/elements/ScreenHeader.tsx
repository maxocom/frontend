import React, {ReactNode} from 'react';
import {View, StyleProp, TouchableOpacity, TextStyle, Platform, ActionSheetIOS, StyleSheet, Alert} from 'react-native';
import ActionSheet from 'react-native-action-sheet';

import {appProvider} from '../appProvider';
import OwnIcon, {IconName} from './OwnIcons';
import {ScreenHeaderTitle} from './ScreenHeaderTitle';
import theme from '../styles/theme';

type Props = {
  left?: IItem;
  title?: string | TitleRenderer;
  subtitle?: string | ReactNode;
  right?: IItem;
};

interface IItem {
  color?: string;
  style?: StyleProp<TextStyle>;
  icon?: IconName;
  onPress?: () => void;
}

export type TitleRenderer = () => React.ReactNode;

export const ScreenHeader: React.FC<Props> = (props) => {
  const app = appProvider.application;
  let center: ReactNode = null;

  if (typeof props.title === 'function') {
    center = props.title();
  } else {
    center = <ScreenHeaderTitle title={props.title as string} subTitle={props.subtitle} position={props.left ? 'center' : 'left'} />;
  }

  const onPressContextMenu = () => {
    const IOS = Platform.OS === 'ios';
    if (IOS) {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ['На главную', 'О нас','Истории лидеров', 'База знаний', 'Выйти', 'Закрыть'],
          cancelButtonIndex: 5,
        },
        (buttonIndex) => {
          switch (buttonIndex) {

            case 0:
              app.navigateToStartScreens();
              break;

            case 1:
              app.navigateToAbout();
              break;

            case 2:
              app.navigateToLeaderStories();
              break;

            case 3:
              app.navigateToFaq();
              break;

            case 4:
              confirmLogOut();
              break;
            case 5:
              break;

            default:
              break;
          }
        },
      );
    } else {
      ActionSheet.showActionSheetWithOptions(
        {
          options: ['База знаний', 'Истории лидеров', 'О нас', 'На главную', 'Выйти'],
          destructiveButtonIndex: 1,
          cancelButtonIndex: 2,
        },
        (buttonIndex) => {
          switch (buttonIndex) {

            case 0:
              app.navigateToFaq();
              break;

            case 1:
              app.navigateToLeaderStories();
              break;

            case 2:
              app.navigateToAbout();
              break;

            case 3:
              app.navigateToStartScreens();
              break;

            case 4:
              confirmLogOut();
              break;

            default:
              break;
          }
        },
      );
    }
  };

  const confirmLogOut = () => {
    Alert.alert(
      'Выход',
      'Вы уверены что хотите выйти?',
      [
        {text: 'Закрыть'},
        {text: 'Выйти', style: 'destructive', onPress: logOut},
      ],
      {cancelable: false},
    );
  };

  const logOut = () => {
    app.logout();
  };

  return (
    <View style={[styles.header, props.left && styles.fullHeader, props.right && !props.left && styles.rightHeader]}>
      { props.left && <Item {...props.left} style={styles.leftIcon} /> }
      <View style={props.left ? styles.centerItem : styles.leftItem}>{center}</View>
      {props.right ? (
        <TouchableOpacity onPress={onPressContextMenu} hitSlop={theme.smallElementsHitSlop} style={styles.item}>
          <View>
            <OwnIcon name={props.right.icon} style={styles.rightIcon} />
          </View>
        </TouchableOpacity>
      ) : (
        <View style={styles.dummyBlock} />
      )}
    </View>
  );
};

type ItemProps = IItem & {style: StyleProp<TextStyle>};

const Item: React.FC<ItemProps> = (props) => {
  const style: StyleProp<TextStyle> = [props.style, props.color ? {color: props.color} : null];
  return (
    <TouchableOpacity onPress={props.onPress} hitSlop={theme.smallElementsHitSlop} style={styles.item}>
      <View>{props.icon ? <OwnIcon name={props.icon} style={style} /> : null}</View>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    top: 0,
    left: 0,
    right: 0,
    paddingTop: theme.aligned(70),
    paddingBottom: theme.aligned(39),
    paddingHorizontal: theme.aligned(30),
    marginBottom: theme.aligned(32),
    backgroundColor: theme.bgColorDarkGray,
    borderBottomLeftRadius: theme.aligned(20),
    borderBottomRightRadius: theme.aligned(20),
  },
  fullHeader: {
    paddingHorizontal: theme.aligned(12),
  },
  rightHeader: {
    paddingRight: theme.aligned(12),
  },
  dummyBlock: {
    width: theme.aligned(46),
  },
  item: {
    width: theme.aligned(46),
    height: theme.aligned(46),
    borderRadius: theme.aligned(46),
    overflow: 'hidden',
  },
  centerItem: {
    alignItems: 'center',
    flex: 1,
    marginLeft: theme.aligned(15),
    marginRight: theme.aligned(15),
  },
  leftItem: {
    alignItems: 'flex-start',
    flex: 1,
  },
  leftIcon: {
    height: '100%',
    width: '100%',
    textAlign: 'center',
    lineHeight: theme.aligned(46),
    backgroundColor: theme.bgBtnLightGray,
    fontSize: theme.fontSize18,
    color: theme.textColor,
    borderRadius: theme.aligned(46),
  },
  rightIcon: {
    height: '100%',
    width: '100%',
    textAlign: 'center',
    lineHeight: theme.aligned(46),
    backgroundColor: theme.bgBtnOrange,
    fontSize: theme.fontSize18,
    color: theme.textColor,
    borderRadius: theme.aligned(46),
  },
});
