import React from 'react';
import {StyleProp, StyleSheet, Text, TouchableOpacity, ViewStyle} from 'react-native';

import theme, {withShadow} from '../theme';
import Icon, {IconName} from './OwnIcons';

type Props = {
  onPress: () => void;
  text: string;
  loading?: boolean;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
  withShadow?: boolean;
  icon?: IconName;
};

export const ButtonOutlined: React.FC<Props> = (props: Props) => {
  const disabled = props.disabled || props.loading;

  const style = StyleSheet.flatten([
    props.withShadow ? withShadow(styles.button, theme.lightGrayColor) : styles.button,
    props.style ? props.style : null,
    disabled ? styles.disabled : null,
  ]);

  const renderIcon = () => {
    if (!props.icon) return null;
    return <Icon name={props.icon} size={20} style={styles.icon} />;
  };

  return (
    <TouchableOpacity onPress={props.onPress} style={style}>
      {renderIcon()}
      <Text maxFontSizeMultiplier={1.5} style={styles.text}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    backgroundColor: theme.bgColor,
    borderRadius: theme.aligned(15),
    borderWidth: theme.aligned(1),
    borderColor: theme.tintColor,
    height: theme.aligned(50),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontFamily: theme.fontFamilyButton,
    fontSize: theme.fontSize16,
    fontWeight: '500',
    color: theme.tintColor,
  },
  disabled: {
    borderColor: theme.grayIconColor,
  },
  icon: {
    color: theme.tintColor,
    marginRight: theme.aligned(8),
  },
});
