import React from 'react';
import {
  StyleSheet,
  View,
  StyleProp,
  ViewStyle,
  Text,
} from 'react-native';
import {observer} from 'mobx-react-lite';
import {expr} from 'mobx-utils';
import RNPickerSelect, { Item } from 'react-native-picker-select';

import theme from '../styles/theme';
import {IFormField} from '../models/formField';
import Icon from './OwnIcons';

interface IProps {
  field: IFormField;
  disabled?: boolean;
  data: Item[];
  placeholder?: string;
  onSelect?: (id: any) => void;
}

export const FormInputList: React.FC<IProps> = observer((props: IProps) => {
  const onSelectItem = (val: string) => {
    props.field.setValue(val);
  };

  const inputStyles: StyleProp<ViewStyle> = expr(() => {
    const style: ViewStyle[] = [styles.input];
    if (props.field.focused)  style.push(styles.focused);
    if (!!props.field.error && props.field.isDirty) style.push(styles.inputError);
    return style;
  });

  const hintBlock = () => {
    switch (true) {
      case !!props.field.error && props.field.isDirty:
        return <Text style={[styles.hintText, styles.errorText]}>{props.field.error}</Text>;
      default:
        return null;
    }
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.row}>
        <RNPickerSelect
          onValueChange={(value) => onSelectItem(value)}
          items={props.data}
          value={props.field.value}
          disabled={props.disabled}
          placeholder={{
            label: props.placeholder,
            value: null,
          }}
          placeholderTextColor={theme.textColor}
          style={{inputIOS: inputStyles, inputAndroid: inputStyles }}
        />
        <Icon style={styles.icon} name={'bottomArr'} size={theme.aligned(16)} />
      </View>
      {hintBlock()}
    </View>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
  },
  row: {
    alignSelf: 'stretch',
  },
  input: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor,
    height: theme.aligned(56),
    borderRadius: theme.aligned(10),
    paddingHorizontal: theme.aligned(25),
    paddingLeft: theme.aligned(27),
    paddingRight: theme.aligned(43),
    textAlignVertical: 'center',
    backgroundColor: theme.bgColorLightGray,
    flexDirection: 'row',
  },
  icon: {
    position: 'absolute',
    right: theme.aligned(23),
    top: theme.aligned(23),
    bottom: 0,
    zIndex: 99,
    color: theme.bgBtnOrange,
  },
  focused: {
    backgroundColor: theme.bgColorLightGray,
  },
  inputError: {
    backgroundColor: theme.lightPink,
    color: theme.textErrorColor,
  },
  errorText: {
    color: theme.textErrorColor,
  },
  hintText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space16,
    color: theme.textErrorColor,
    marginTop: theme.aligned(8),
  },
});
