import React, {PropsWithChildren} from 'react';
import LinearGradient, {LinearGradientProps} from 'react-native-linear-gradient';
import {ViewStyle} from 'react-native';

const GRADIENT_OVERLAY_DUAL_COLORS: string[] = ['rgba(0, 0, 0, 0.7)', 'rgba(0, 0, 0, 0)'];
const GRADIENT_OVERLAY_TRIPLE_COLORS: string[] = ['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.8)'];
const GRADIENT_OVERLAY_HISTORY_COLORS: string[] = ['rgba(0, 0, 0, 0.2)', 'rgba(0, 0, 0, 0.6)'];

type Props = {
  style: ViewStyle;
  appearance: 'dualColors' | 'tripleColors' | 'historyColors';
};

export const GradientOverlay: React.FC<PropsWithChildren<Props>> = (props) => {
  const gradientOverlayProps = () => {
    const gradientProps: LinearGradientProps = {
      style: props.style,
      colors: GRADIENT_OVERLAY_DUAL_COLORS,
    };

    switch (props.appearance) {
      case 'dualColors':
        gradientProps.colors = GRADIENT_OVERLAY_DUAL_COLORS;
        break;
      case 'tripleColors':
        gradientProps.colors = GRADIENT_OVERLAY_TRIPLE_COLORS;
        break;
      case 'historyColors':
        gradientProps.colors = GRADIENT_OVERLAY_HISTORY_COLORS;
        break;
    }
    return gradientProps;
  };

  return React.createElement(LinearGradient, gradientOverlayProps(), props.children);
};

export default GradientOverlay;
