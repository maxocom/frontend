import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import {observer} from 'mobx-react-lite';
import FastImage from 'react-native-fast-image';

import theme from '../styles/theme';
import {IFormField} from '../models/formField';
import Icon, {IconName} from './OwnIcons';
import {selectPicture} from '../utils/mediaProcessing';

interface IProps {
  field: IFormField;
  icon?: IconName;
}

export const FormInputImage: React.FC<IProps> = observer((props: IProps) => {
  const onChangeAvatar = () => {
    selectPicture(true).then(({url}) => {
      props.field.setValue(url);
    });
  };

  const renderAvatarImage = () => {
    if (!props.field.value) {
      const style = StyleSheet.flatten([
        styles.avatarEmptyWrap,
        (props.field.error && props.field.isDirty) ? styles.avatarError : null,
      ]);

      return (
        <View style={style}>
          <Icon name={props.icon} size={30} style={styles.emptyIcon} />
        </View>
      );
    }

    return (
      <FastImage source={{uri: props.field.value}} style={styles.avatarImage} />
    );
  };

  const hintBlock = () => {
    switch (true) {
      case !!props.field.error && props.field.isDirty:
        return <Text style={[styles.hintText, styles.errorText]}>{props.field.error}</Text>;
      default:
        return null;
    }
  };

  return (
    <>
      <TouchableOpacity onPress={onChangeAvatar} style={styles.avatarContainer}>
        <View style={styles.avatarWrap}>{renderAvatarImage()}</View>
        {hintBlock()}
      </TouchableOpacity>
    </>
  );
});

const styles = StyleSheet.create({
  avatarContainer: {
    width: '100%',
  },
  avatarWrap: {
    width: '100%',
    height: theme.aligned(100),
    borderRadius: theme.aligned(10),
    overflow: 'hidden',
  },
  avatarEmptyWrap: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: theme.bgDDArea,
    borderWidth: 1,
    borderColor: theme.borderDDArea,
    borderRadius: 10,
    borderStyle: 'dashed',
  },
  emptyIcon: {
    color: theme.bgBtnOrange,
  },
  avatarImage: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
  },
  avatarError: {
    backgroundColor: theme.lightPink,
    borderColor: theme.textErrorColor,
  },
  hintText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space16,
    color: theme.textErrorColor,
    marginTop: theme.aligned(8),
  },
  errorText: {
    color: theme.textErrorColor,
  },
});
