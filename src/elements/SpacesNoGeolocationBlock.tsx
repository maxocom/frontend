import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';


import theme from '../styles/theme';
import {appProvider} from '../appProvider';
import {observer} from 'mobx-react-lite';
import {SpacesFilteredList} from '../models/spacesFilteredList';

type Props = {
  list: SpacesFilteredList;
};

export const SpacesNoGeolocationBlock: React.FC<Props> = observer((props) => {
  const onRefresh = () => {
    appProvider.application.loadInitialSpaces(props.list);
  };

  return (
    <View style={styles.container}>
      <Text maxFontSizeMultiplier={1.5} style={styles.title}>
        {strings('common.spaces.permission_denied')}
      </Text>
      <TouchableOpacity onPress={onRefresh}>
        <View style={styles.refreshBlock}>
          <Text maxFontSizeMultiplier={1.5} numberOfLines={1} style={styles.refreshText}>
            Refresh
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontFamily: theme.fontFamilyInter,
    fontWeight: 'normal',
    fontSize: theme.fontSize18,
    lineHeight: theme.aligned(24),
    textAlign: 'center',
    color: theme.textSecondaryGray,
  },
  refreshBlock: {
    marginTop: theme.aligned(5),
    borderWidth: theme.pt,
    borderColor: theme.tintColor,
    borderRadius: theme.aligned(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: theme.aligned(7),
  },
  refreshText: {
    fontFamily: theme.fontFamilyMedium,
    fontSize: theme.fontSize16,
    fontWeight: '500',
    color: theme.tintColor,
    letterSpacing: theme.aligned(-0.2),
    marginHorizontal: theme.aligned(5),
  },
});
