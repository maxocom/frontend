import React, {ClassAttributes, createRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TextInputProps,
  ViewStyle,
  TextStyle,
  StyleProp,
  LayoutAnimation,
} from 'react-native';
import {observer} from 'mobx-react-lite';
import {expr} from 'mobx-utils';

import theme from '../styles/theme';
import {IFormField} from '../models/formField';
import {cleanStringWithNumberVal, dateToString} from '../utils/formatters';
import Icon, {IconName} from './OwnIcons';

export type InputType = 'email' | 'password' | 'number' | 'text' | 'date' | 'phone' | 'search';

export type TextareaProps = {
  numberOfLines?: number;
  multiline?: boolean;
};

interface IProps {
  field: IFormField;
  type: InputType;
  autofocus?: boolean;
  disabled?: boolean;
  hint?: string;
  maxLength?: number;
  placeholder?: string;
  wrapperStyle?: ViewStyle;
  inputStyle?: TextStyle;
  icon?: IconName;
  textarea?: TextareaProps;
  showCancelHint?: boolean;
}

export const FormInputBlock: React.FC<IProps> = observer((props: IProps) => {
  const inputRef = createRef<TextInput>();
  const hintBlock = () => {
    switch (true) {
      case !!props.field.error && props.field.isDirty:
        return <Text style={[styles.hintText, styles.errorText]}>{props.field.error}</Text>;
      case !!props.hint:
        return <Text style={styles.hintText}>{props.hint || ''}</Text>;
      default:
        return null;
    }
  };

  const onChangeText = (val: string) => {
    if (props.type === 'number') {
      props.field.setValue(cleanStringWithNumberVal(val));
      return;
    }
    props.field.setValue(val);
  };

  const onBlur = () => {
    props.field.markAsDirty();
    props.field.setFocused(false);
    LayoutAnimation.easeInEaseOut();
  };

  const onFocus = () => {
    props.field.setFocused(true);
    LayoutAnimation.easeInEaseOut();
  };

  const inputStyles: StyleProp<TextStyle> = expr(() => {
    const style: TextStyle[] = [styles.input];
    if (props.field.focused) style.push(styles.focused);
    if (!!props.field.error && props.field.isDirty) style.push(styles.inputError);
    if (props.inputStyle) style.push(props.inputStyle);
    if (props?.textarea?.multiline) style.push(styles.textArea);
    if (!props?.icon) style.push(styles.withoutIcon);
    return style;
  });

  const inputProps: TextInputProps & ClassAttributes<TextInput> = {
    onBlur,
    onFocus,
    onChangeText,
    maxLength: props.maxLength,
    style: inputStyles,
    value: String(props.field.value ? props.field.value : ''),
    underlineColorAndroid: 'transparent',
    autoFocus: props.autofocus,
    placeholderTextColor: theme.placeholderColor,
    maxFontSizeMultiplier: 1.5,
    ref: inputRef,

    ...props.textarea,
  };

  if (props.placeholder) inputProps.placeholder = props.placeholder;

  switch (props.type) {
    case 'email':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.keyboardType = 'email-address';
      break;
    case 'password':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.secureTextEntry = true;
      break;
    case 'number':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.keyboardType = 'numeric';
      break;
    case 'phone':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.keyboardType = 'phone-pad';
      break;
    case 'date':
      inputProps.editable = false;
      if (props.field.value) {
        inputProps.value = dateToString(Number(props.field.value));
      }
      break;
    case 'text':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.keyboardType = 'default';
      break;
    case 'search':
      inputProps.autoCapitalize = 'none';
      inputProps.autoCorrect = false;
      inputProps.keyboardType = 'default';
      break;
  }

  if (!props.field.isValid && props.field.isDirty) {
    inputProps.style = StyleSheet.flatten([inputProps.style, styles.inputError]);
  }
  if (props.disabled) {
    inputProps.editable = false;
  }

  return (
    <View style={[styles.wrapper, props.wrapperStyle]}>
      <View style={styles.row}>
        {
          props.icon && (
            <View style={styles.leftIconBlock}>
              <Icon style={[styles.icon, (!!props.field.error && props.field.isDirty) && styles.errorText]} name={props.icon} size={theme.aligned(28)} />
            </View>
          )
        }
        {React.createElement(TextInput, inputProps)}
      </View>
      {hintBlock()}
    </View>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftIconBlock: {
    position: 'absolute',
    left: 0,
    top: theme.aligned(14),
    bottom: 0,
    zIndex: 99,
  },
  icon: {
    color: theme.textColor,
    paddingLeft: theme.aligned(25),
  },
  input: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space18,
    color: theme.textColor,
    height: theme.aligned(56),
    borderRadius: theme.aligned(10),
    paddingHorizontal: theme.aligned(25),
    paddingLeft: theme.aligned(67),
    paddingVertical: 0,
    flex: 1,
    textAlignVertical: 'center',
    backgroundColor: theme.bgColorLightGray,
    flexDirection: 'row',
  },
  withoutIcon: {
    paddingLeft: theme.aligned(25),
  },
  inputError: {
    backgroundColor: theme.lightPink,
    color: theme.textErrorColor,
  },
  focused: {
    backgroundColor: theme.bgColorLightGray,
  },
  errorText: {
    color: theme.textErrorColor,
  },
  hintText: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize14,
    lineHeight: theme.space16,
    color: theme.textErrorColor,
    marginTop: theme.aligned(8),
  },
  textArea: {
    minHeight: theme.aligned(120),
    textAlignVertical: 'top',
  },

});
