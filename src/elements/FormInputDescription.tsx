import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import theme from '../styles/theme';
import {observer} from 'mobx-react-lite';

type Props = {
  descriptionText: string;
};

export const FormInputDescription: React.FC<Props> = observer((props) => {
  return (
    <View style={styles.row}>
      <Text maxFontSizeMultiplier={1.5} style={styles.text}>
        {props.descriptionText}
      </Text>
    </View>
  );
});

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flex: 1,
    marginTop: theme.aligned(10),
    justifyContent: 'center',
    width: '85%'
  },
  text: {
    fontFamily: theme.fontFamilyRegular,
    fontSize: theme.fontSize12,
    lineHeight: theme.space16,
    flexShrink: 1,
    color: theme.textLabelColor,
    textAlign: 'center'
  },
});
