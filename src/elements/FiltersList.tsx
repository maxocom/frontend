import React, {ReactText, useCallback} from 'react';
import {ScrollView, StyleSheet, TouchableOpacity, Text, View} from 'react-native';

import theme from '../styles/theme';
import {IFilterItem} from '../types';

interface Props {
  value: ReactText;
  data: IFilterItem[];
  onSelect: (id: ReactText) => void;
}

export const FiltersList: React.FC<Props> = (props: Props) => {
  const onClick = useCallback(
    (id: ReactText) => {
      props.onSelect(id);
    },
    [props],
  );

  const renderButtons = () => {
    return props.data.map((item) => {
      const isActive = item.id === props.value;
      const textStyle = [styles.text, isActive ? styles.activeText : null];
      return (
        <TouchableOpacity key={item.id} onPress={() => onClick(item.id)}>
          <Text style={textStyle}>{item.text}</Text>
          {isActive ? <View style={styles.underline} /> : null}
        </TouchableOpacity>
      );
    });
  };

  return (
    <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={styles.container}>
      {renderButtons()}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: theme.space16,
    paddingBottom: theme.space8,
  },
  activeText: {
    color: theme.tintColor,
  },
  text: {
    paddingVertical: theme.space12,
    marginRight: theme.aligned(24),
    alignSelf: 'center',
    fontFamily: theme.fontFamilyInter,
    fontWeight: '600',
    fontSize: theme.fontSize14,
    color: theme.borderGreyColor,
  },
  underline: {
    width: theme.aligned(13),
    marginRight: theme.aligned(24),
    height: theme.aligned(3),
    backgroundColor: theme.tintColor,
    alignSelf: 'center',
  },
});
