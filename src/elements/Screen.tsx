import React, {PropsWithChildren} from 'react';
import {ViewStyle, StyleSheet, StyleProp, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import theme from '../styles/theme';

type Props = {
  style?: StyleProp<ViewStyle>;
  safearea?: boolean;
};

export const Screen: React.FC<PropsWithChildren<Props>> = (props) => {
  const style = [styles.main, props.style];
  if (props.safearea)
    return (
      <SafeAreaView edges={['top']} style={style}>
        {props.children}
      </SafeAreaView>
    );
  return <View style={style}>{props.children}</View>;
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: theme.bgColor,
  },
});
