import {observer} from 'mobx-react-lite';
import {Image, Linking, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';

import images from '../images';
import Icon from './OwnIcons';
import React from 'react';
import {Space} from '../models/space';
import {appProvider} from '../appProvider';
import theme from '../styles/theme';


type Props = {
  space: Space;
  onClose: () => void;
};

export const SpaceMapInfoBlock: React.FC<Props> = observer((props) => {
  const onOpenSpace = () => {
    props.onClose();
    appProvider.application.navigateToSpaceScreen(props.space.id);
  };

  const renderCover = () => {
    if (!props.space.cover) {
      return (
        <View style={styles.unknownIconBlock}>
          <Image source={images.unknownIcon} style={styles.unknownIcon} />
        </View>
      );
    }
    return <FastImage source={{uri: props.space.cover}} style={styles.spaceImage} />;
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <TouchableOpacity style={styles.spaceImageBlock} onPress={onOpenSpace}>
          {renderCover()}
        </TouchableOpacity>
        <View style={styles.headerTitleBlock}>
          <TouchableOpacity onPress={onOpenSpace}>
            <Text maxFontSizeMultiplier={1.5} style={styles.titleText} numberOfLines={3}>
              {props.space.name}
            </Text>
          </TouchableOpacity>
          <Text maxFontSizeMultiplier={1.5} style={styles.spaceTypeText}>
            {strings(props.space.typeName)}
          </Text>
        </View>
        <TouchableOpacity style={styles.closeBtn} onPress={props.onClose}>
          <Image // TODO make font icon symbol and use OwnIcon component instead
            source={images.map_space_close}
            style={styles.closeImg}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const onRoutePress = () => {
    const app = appProvider.application;
    const routeUrl = app.model.geoPosition.getRouteUrl({lat: props.space.lat, lng: props.space.lng});
    if (!routeUrl) return;
    Linking.openURL(routeUrl);
  };

  const renderRouteBlock = () => {
    if (!props.space.hasLocation) return null;
    return (
      <TouchableOpacity style={styles.routeBtn}>
        <Text style={styles.routeText} onPress={onRoutePress}>
          {strings('common.route')}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderAddressBlock = () => {
    if (!props.space.address) return null;
    return (
      <View style={styles.row}>
        <Icon name="mapPoint" size={16} style={styles.icon} />
        <Text maxFontSizeMultiplier={1.5} style={styles.rowText} ellipsizeMode="tail" numberOfLines={2}>
          {props.space.address}
        </Text>
      </View>
    );
  };

  const renderDistanceBlock = () => {
    return (
      <View style={styles.row}>
        <Icon name="faWalking" size={18} style={styles.icon} />
        <Text maxFontSizeMultiplier={1.5} style={styles.rowText}>
          {props.space.distanceFormatted}
        </Text>
      </View>
    );
  };

  const renderDescriptionBlock = () => {
    return (
      <TouchableOpacity style={styles.descriptionBlock} onPress={onOpenSpace}>
        <Text style={styles.descriptionText}>{props.space.description}</Text>
      </TouchableOpacity>
    );
  };

  const renderSpaceRows = () => {
    return (
      <View style={styles.rowsBlock}>
        {renderAddressBlock()}
        {renderRouteBlock()}
        {renderDistanceBlock()}
        {renderDescriptionBlock()}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {renderHeader()}
      {renderSpaceRows()}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    paddingVertical: theme.space12,
    minHeight: theme.windowHeight * 0.5,
    flex: 1,
    backgroundColor: theme.bgColor,
    borderTopLeftRadius: theme.aligned(15),
    borderTopRightRadius: theme.aligned(15),
  },
  header: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: theme.aligned(7),
  },
  spaceImage: {
    width: theme.aligned(70),
    height: theme.aligned(70),
    borderRadius: theme.aligned(12),
  },
  unknownIcon: {
    resizeMode: 'contain',
    width: theme.aligned(12),
    height: undefined,
    aspectRatio: 1,
  },
  unknownIconBlock: {
    width: theme.aligned(70),
    height: theme.aligned(70),
    borderRadius: theme.aligned(12),
    backgroundColor: theme.blueH400Color,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  spaceImageBlock: {
    width: theme.aligned(70),
    height: theme.aligned(70),
    marginRight: theme.aligned(12),
  },
  headerTitleBlock: {
    flexGrow: 1,
    maxWidth: theme.windowWidth - theme.aligned(154),
    justifyContent: 'center',
  },
  titleText: {
    fontFamily: theme.fontFamilyMedium,
    fontSize: theme.aligned(16),
    lineHeight: theme.aligned(18),
    fontWeight: '500',
    color: theme.mainDarkH600,
  },
  spaceTypeText: {
    fontFamily: theme.fontFamilyInter,
    fontSize: theme.aligned(16),
    lineHeight: theme.aligned(18),
    color: theme.grayIconColor,
    fontWeight: '500',
    marginTop: theme.aligned(2),
  },
  closeBtn: {
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: theme.aligned(30),
    height: theme.aligned(30),
    backgroundColor: theme.lightGrayColor,
    borderRadius: theme.aligned(8),
    marginLeft: theme.aligned(12),
  },
  closeImg: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: theme.aligned(14),
    aspectRatio: 1,
  },
  rowsBlock: {
    width: '100%',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: theme.aligned(8),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: theme.aligned(8),
    paddingHorizontal: theme.screenWidthPadding,
  },
  icon: {
    marginRight: theme.aligned(7),
    color: theme.blueH400Color,
  },
  rowText: {
    width: theme.windowWidth - theme.aligned(51),
    alignSelf: 'center',
    fontFamily: 'Inter',
    fontSize: theme.fontSize14,
    lineHeight: theme.aligned(17),
    color: theme.textColor,
    fontWeight: '500',
  },
  descriptionBlock: {
    width: '100%',
    marginVertical: theme.aligned(5),
    paddingHorizontal: theme.screenWidthPadding,
  },
  descriptionText: {
    fontFamily: theme.fontFamilyInter,
    fontSize: theme.fontSize14,
    lineHeight: theme.aligned(18),
    fontWeight: 'normal',
    color: theme.mainDarkH600,
  },
  routeBtn: {
    paddingVertical: theme.aligned(3),
    marginLeft: theme.screenWidthPadding,
  },
  routeText: {
    flex: 1,
    flexShrink: 1,
    paddingHorizontal: theme.aligned(5),
    paddingVertical: theme.aligned(3),
    borderColor: theme.tintColor,
    color: theme.tintColor,
    fontSize: theme.fontSize14,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
  },
});
