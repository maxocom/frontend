import React from 'react';
import {Platform} from 'react-native';
import {CardStyleInterpolators, createStackNavigator, StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';

import {Training} from './models/training';
import {Event} from './models/event';
import {Faq} from './models/faq';
import {About} from './models/about';
import {LeaderStories, LeaderStory} from './models/leaderStories';
import {Galleries, Gallery} from './models/galleries';

import {HomeScreen} from './screens/HomeScreen';
import {LogInScreen} from './screens/LogInScreen';
import {AgreementScreen} from './screens/AgreementScreen';
import {RulesScreen} from './screens/RulesScreen';
import {MainScreen} from "./screens/MainScreen";
import {AuthTypeScreen} from './screens/AuthTypeScreen';
import {VerificationScreen} from './screens/VerificationScreen';
import TrainingsScreen from './screens/TrainingsScreen';
import TrainingScreen from './screens/TrainingScreen';
import TopScreen from './screens/TopScreen';
import StatisticsScreen from './screens/StatisticsScreen';
import {SendStatisticsScreen} from './screens/SendStatisticsScreen';
import CalendarScreen from './screens/CalendarScreen';
import EventScreen from './screens/EventScreen';
import {ProfileSettingsScreen} from './screens/ProfileSettingsScreen';
import FaqScreen from './screens/FaqScreen';
import LeaderStoriesScreen from './screens/LeaderStoriesScreen';
import LeaderStoryScreen from './screens/LeaderStoryScreen';
import AboutScreen from './screens/AboutScreen';
import GalleriesScreen from './screens/GalleriesScreen';
import GalleryScreen from './screens/GalleryScreen';

import {AppScreens} from './constants/screens';

import {ProfileEditForm} from './models/profileEditForm';
import {VerificationForm} from './models/verificationForm';
import {SendStatisticsForm} from './models/sendStatisticsForm';
import {Profile} from './models/profile';

import {TeamsListsConfig, TrainingListsConfig, TopListsConfig, StatisticsConfig, CalendarConfig} from './types';
import SpiritualityScreen from "./screens/SpiritualityScreen";

const Stack = createStackNavigator();

export type RootStackParamList = {
  [AppScreens.ProfileSettings]: {form: ProfileEditForm};
  [AppScreens.Agreement]: {backTo: AppScreens; backToParams?: object;};
  [AppScreens.Rules]: {backTo: AppScreens; backToParams?: object;};
  [AppScreens.Main]: {};
  [AppScreens.Spirituality]: {};
  [AppScreens.AuthType]: {};
  [AppScreens.Verification]: {backTo: AppScreens; backToParams?: object; form: VerificationForm, cfg: TeamsListsConfig};
  [AppScreens.Trainings]: {cfg: TrainingListsConfig};
  [AppScreens.Training]: {backTo?: AppScreens; backToParams?: object; training: Training};
  [AppScreens.Top]: {cfg: TopListsConfig};
  [AppScreens.Statistics]: {cfg: StatisticsConfig};
  [AppScreens.SendStatistics]: {backTo: AppScreens; backToParams?: object; form: SendStatisticsForm; profile: Profile};
  [AppScreens.Calendar]: {cfg: CalendarConfig};
  [AppScreens.Event]: {backTo?: AppScreens; backToParams?: object; event: Event};
  [AppScreens.Faq]: {cfg: Faq};
  [AppScreens.LeaderStories]: {cfg: LeaderStories};
  [AppScreens.LeaderStory]: {backTo?: AppScreens; backToParams?: object; leaderStory: LeaderStory};
  [AppScreens.About]: {cfg: About};
  [AppScreens.Galleries]: {cfg: Galleries};
  [AppScreens.Gallery]: {backTo?: AppScreens; backToParams?: object; gallery: Gallery};
};

type RootStackScreens = keyof RootStackParamList;

export type RouteScreenProp<T extends RootStackScreens> = RouteProp<RootStackParamList, T>;
export type NavigationScreenProp<T extends RootStackScreens> = StackNavigationProp<RootStackParamList, T>;

export type NavigationScreenProps<T extends RootStackScreens> = {
  route: RouteScreenProp<T>;
  navigation: NavigationScreenProp<T>;
};

const defaultOptions = {gestureEnabled: false, headerShown: false};
const screenOptions = {
  cardStyleInterpolator: Platform.select({
    ios: CardStyleInterpolators.forHorizontalIOS,
    android: CardStyleInterpolators.forNoAnimation,
  }),
  animationEnabled: false,
};

const Routes: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen name={AppScreens.Home} component={HomeScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.LogIn} component={LogInScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Agreement} component={AgreementScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Rules} component={RulesScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Main} component={MainScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Spirituality} component={SpiritualityScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.AuthType} component={AuthTypeScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Verification} component={VerificationScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.ProfileSettings} component={ProfileSettingsScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Trainings} component={TrainingsScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Training} component={TrainingScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Top} component={TopScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Statistics} component={StatisticsScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.SendStatistics} component={SendStatisticsScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Calendar} component={CalendarScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Event} component={EventScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Faq} component={FaqScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.LeaderStories} component={LeaderStoriesScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.LeaderStory} component={LeaderStoryScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.About} component={AboutScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Galleries} component={GalleriesScreen} options={defaultOptions} />
      <Stack.Screen name={AppScreens.Gallery} component={GalleryScreen} options={defaultOptions} />
    </Stack.Navigator>
  );
};

export default Routes;
