import {makeHandler} from './makeHandler';
import * as api from './index';

export const requestOtp = makeHandler(
  'requestOtp',
  (otp: RequestOtpParams) => ({
    method: 'POST',
    path: 'users/send-login-code/',
    data: otp,
  }),
  (session: Session) => session,
);

export const resendOtp = makeHandler(
  'resendOtp',
  (otp: RequestOtpParams) => ({
    method: 'POST',
    path: `users/send-login-code/`,
    data: otp,
  }),
  (session: Session) => session,
);

export const checkOtp = makeHandler(
  'checkOtp',
  (session: SessionParams) => ({
    method: 'POST',
    path: 'users/login-or-register/',
    data: session,
  }),
  (authData: AuthData) => authData.data,
);

export const refreshToken = makeHandler(
  'refreshToken',
  (refresh: TokenRefreshParams) => ({
    method: 'POST',
    path: 'users/token/refresh/',
    data: refresh,
  }),
  (response: {
    data: AccessTokens
  }) => response.data,
);

export type SessionParams = {
  /** Номер мобильного телефона */
  phone_number: string;
  /** Пин-код */
  code?: string;
};

export type RequestOtpParams = {
  /** Номер мобильного телефона */
  phone_number: string;
};

export type TokenRefreshParams = {
  refresh: string;
};

export type ConfirmCodeResponse = {
  expired_at: string;
  message: string;
};

export type AuthData = {
  data: {
    access: string;
    refresh: string;
    user: api.Profile
  }
};

export type AccessTokens = {
  access_token: string;
  refresh_token: string;
};

export type Session = {
  expired_at: string;
  message: string;
};
