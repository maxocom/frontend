import {makeHandler} from './makeHandler';

export type Event = {
  id: number;
  name: string;
  date: string;
  image: string;
  speaker: string;
  link: string;
  description: string;
  subscribe_event: boolean;
};

export const getEvents = makeHandler(
  'getEvents',
  (req: {token: string; params: EventsRequestParams}) => ({
    method: 'GET',
    path: `events/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: EventsResponse
  }) => response.data,
);

export const getEvent = makeHandler(
  'getEvent',
  (req: {token: string; id: number}) => ({
    method: 'GET',
    path: `events/${req.id}/`,
    token: req.token,
  }),
  (response: {
    data: Event
  }) => response.data,
);

export const sendRequestEvent = makeHandler(
  'sendRequestEvent',
  (req: {token: string; data: SendEventsRequestParams;}) => ({
    method: 'POST',
    path: `events/user-events/`,
    token: req.token,
    data: req.data,
  }),
  (response: {
    data: Event
  }) => response.data,
);

export type EventsRequestParams = {
  limit: number;
  offset?: number;
  from_date?: string;
  to_date?: string;
};

export type SendEventsRequestParams = {
  user: number;
  events: number;
};

export type EventsResponse = {
  results: Event[];
  count: number;
};

function getRequestString(req: EventsRequestParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}
