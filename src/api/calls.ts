import {
  requestOtp,
  resendOtp,
  checkOtp,
  refreshToken
} from './codecs.session';
import {
  getProfile,
  updateUser,
  sendVerification,
  getTop,
  createFcmRegistration,
  updateFcmRegistration,
  removeFcmRegistration,
} from './codecs.users';
import {
  getTeams
} from './codecs.teams';
import {
  getStatistics,
  sendStatistic
} from './codecs.statistics';
import {
  getTrainings,
  getTraining
} from './codecs.trainings';
import {
  getEvents,
  getEvent,
  sendRequestEvent,
} from './codecs.events';
import {
  uploadFile,
} from './codecs.files';
import {
  getFaq,
  getAbout,
  getLeaderStories,
  getLeaderStory,
  getGalleries,
  getGallery,
} from './codecs.pages';

const handlers = [
  requestOtp,
  resendOtp,
  checkOtp,
  getProfile,
  updateUser,
  getTeams,
  sendVerification,
  getTrainings,
  getTraining,
  getTop,
  getStatistics,
  sendStatistic,
  refreshToken,
  getEvents,
  getEvent,
  sendRequestEvent,
  createFcmRegistration,
  updateFcmRegistration,
  removeFcmRegistration,
  uploadFile,
  getFaq,
  getAbout,
  getLeaderStories,
  getLeaderStory,
  getGalleries,
  getGallery,
] as const;

const handlersMap: Map<ApiHandlerTypes, ApiHandlers> = new Map();
for (let h of handlers) {
  handlersMap.set(h.type, h);
}

type DiscriminateUnion<T, K extends keyof T, V extends T[K]> = T extends Record<K, V> ? T : never;

type ApiHandlers = typeof handlers[number];
export type ApiHandlerTypes = ApiHandlers['type'];
export type ApiHandler<Type extends ApiHandlerTypes = ApiHandlerTypes> = DiscriminateUnion<ApiHandlers, 'type', Type>;

export type ApiHandlerParams<Type extends ApiHandlerTypes> = Parameters<ApiHandler<Type>['prepare']>[0] extends {}
  ? Parameters<ApiHandler<Type>['prepare']>[0]
  : undefined;
export type ApiHandlerResponse<Type extends ApiHandlerTypes> = ReturnType<ApiHandler<Type>['decode']>;

export function getHandler<Type extends ApiHandlerTypes>(type: Type): ApiHandler<Type> {
  return handlersMap.get(type) as ApiHandler<Type>;
}
