import {ApiHandlerParams, ApiHandlerTypes, getHandler, ApiHandlerResponse} from './calls';
import {ApiError, isApiError} from './error';
import {ApiUrlPrefixes, FormDataEntry, Request} from './request';
import {API_BASE_URL} from '../constants/general';
import {appProvider} from '../appProvider';

type ApiConfig = {
  base: string;
  timeout?: number;
  token?: string;
  onError?: ErrorHandler;
};

type ErrorHandler = (e: ApiError) => void;

type ProtoError = {
  errors: Record<string, string[]>;
};

const extraGoodHttpStatuses = new Set<number>([204]);

export class Api {
  private timeout: number;
  private token: string;
  private errorHandler?: ErrorHandler;
  base: string;

  constructor(config: ApiConfig) {
    this.base = config.base;
    this.timeout = config.timeout ?? 60 * 1000;
    this.token = config.token ?? '';
    if (config.onError) this.errorHandler = config.onError;
  }

  setToken(token: string) {
    this.token = token;
  }

  call<Type extends ApiHandlerTypes>(type: Type, params: ApiHandlerParams<Type>): Promise<ApiHandlerResponse<Type>>;
  call(type: any, params: any): any {
    const h = getHandler(type);
    const req = h.prepare(params);
    if (this.token) req.token = this.token;
    const a = this.makeRequest(req).then((data) => h.decode(data));
    if (this.errorHandler) a.catch(this.errorHandler);
    return a;
  }

  private makeRequest(params: Request): Promise<any> {
    const urlPrefix = params.apiType ? ApiUrlPrefixes[params.apiType] : API_BASE_URL;
    const url = `${this.base}/${urlPrefix}${params.path}`;
    const headers: Record<string, string> = {
      'Content-Type': 'application/json',
      'accept': 'application/json',
      ...params.headers,
    };

    const formData = params.form ? processFormData(params.form) : null;
    const jsonData = params.data ? JSON.stringify(params.data) : null;
    if (formData) {
      headers['Content-Type'] = 'multipart/form-data';
    }
    const init: RequestInit = {
      method: params.method,
      headers: headers,
      body: formData ? formData : jsonData,
    };

    const abortController = new AbortController();
    init.signal = abortController.signal;

    let timeoutHandle: ReturnType<typeof setTimeout> | undefined = setTimeout(
      () => abortController.abort(),
      this.timeout,
    );
    const cleanupTimeout = () => {
      if (!timeoutHandle) return;
      clearTimeout(timeoutHandle);
      timeoutHandle = undefined;
    };

    if (params.token) {
      headers['Authorization'] = `Bearer ${params.token}`;
    }

    appProvider.application.logger.info('api request', url, init);

    return fetch(url, init)
      .then((res) => {
        if (res.ok || extraGoodHttpStatuses.has(res.status)) {
          const contentType = res.headers.get('Content-Type');
          const contentLength = res.headers.get('content-length');
          if (contentType === 'text/plain' || contentLength === '0') {
            // TODO may need to tune this logic
            return Promise.resolve({});
          }
          return res.json().then((r) => {
            let responseData: any = r.data || {data: r} || {};
            if (r.hasOwnProperty('page')) {
              // pagination handling
              responseData = {data: r.data || {data: r}, page: r.page};
            }
            if (params.apiType) {
              responseData.apiType = params.apiType;
            }
            return responseData;
          });
        }

        const err = new ApiError('NetworkError');
        err.httpStatus = res.status;
        err.httpStatusText = res.statusText;
        err.message = '';

        return res.text().then((text: string) => {
  
          appProvider.application.logger.error(text);
          try {
            const data = JSON.parse(text) as ProtoError;

            const errors = data?.errors || (data as ProtoError);

            let errorDetail: Record<string, string> = {};
            for (let i in errors) {
              if (errors.hasOwnProperty(i)) {
                let e = errors[i];
                if (e instanceof Array) {
                  err.message = `${i}: ${e.join(';')}`;
                  errorDetail[i] = e.join(';');
                } else {
                  errorDetail[i] = e;
                }
              }
            }

            if (errorDetail.hasOwnProperty('detail')) {
              err.message = errorDetail.detail;
            }

            err.details = errorDetail;

            if (errorDetail.code === 'token_not_valid') {
              appProvider.application.updateAccessToken();
            }
          } catch (e) {
            err.message = text;
          }
          throw err;
        });
      })
      .catch((e: Error) => {
        if (isApiError(e)) throw e;
        const err = new ApiError('NetworkError', e.toString());
        throw err;
      })
      .finally(() => {
        cleanupTimeout();
      });
  }
}

function processFormData(form: FormDataEntry[]) {
  if (!Array.isArray(form)) return null;
  const res = new FormData();
  for (const entry of form) {
    res.append(entry.name, entry.value);
  }
  return res;
}
