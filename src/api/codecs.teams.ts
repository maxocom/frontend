import {makeHandler} from './makeHandler'

export type Team = {
  id: number;
  name: string;
};

export const getTeams = makeHandler(
  'getTeams',
  (req: {params: TeamsRequestParams}) => ({
    method: 'GET',
    path: `users/teams/?${getTeamsRequestString(req.params)}`,
  }),
  (response: {
    data: TeamsResponse
  }) => response.data,
);

function getTeamsRequestString(req: TeamsRequestParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}

export type TeamsRequestParams = {
  limit: number;
  offset?: number;
};

export type TeamsResponse = {
  results: Team[];
  count: number;
};
