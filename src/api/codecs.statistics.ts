import {makeHandler} from './makeHandler'

export type Statistics = {
  posted_video: number;
  number_of_meetings: number;
  number_of_touches: number;
  date: string;
};

export const getStatistics = makeHandler(
  'getStatistics',
  (req: {token: string; params: StatisticParams}) => ({
    method: 'GET',
    path: `statistics/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: StatisticsResponse
  }) => response.data,
);

export const sendStatistic = makeHandler(
  'sendStatistic',
  (req: {token: string; statistic: Statistic4Export}) => ({
    method: 'POST',
    path: 'statistics/',
    token: req.token,
    data: req.statistic,
  }),
  (statistics: Statistics) => statistics,
);

export interface Statistic4Export {
  posted_video: number | null;
  number_of_meetings: number | null;
  number_of_touches: number | null;
  user: number | null;
}

export type StatisticParams = {
  limit: number;
  offset?: number;
  user_id?: number;
  from_date?: string;
  to_date?: string;
};

export type StatisticsResponse = {
  results: Statistics[];
  count: number;
};

function getRequestString(req: StatisticParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}