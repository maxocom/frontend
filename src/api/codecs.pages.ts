import {makeHandler} from './makeHandler';

export type Faq = {
  id: number;
  title: string;
  answer: string;
};

export type About = {
  text: string;
};

export type LeaderStory = {
  id: number;
  first_name: string;
  last_name: string;
  image: string;
  description: string;
  link: string;
};

export type Gallery = {
  id: number;
  name: string;
  date: string;
  image: string;
  gallery_content: any[];
};

export const getFaq = makeHandler(
  'getFaq',
  (req: {token: string; params: PageParams}) => ({
    method: 'GET',
    path: `utils/faq/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: FaqResponse
  }) => response.data,
);

export const getAbout = makeHandler(
  'getAbout',
  (req: {token: string;}) => ({
    method: 'GET',
    path: 'utils/about-us/',
    token: req.token,
  }),
  (response: {
    data: About
  }) => response.data,
);

export const getLeaderStories = makeHandler(
  'getLeaderStories',
  (req: {token: string; params: PageParams}) => ({
    method: 'GET',
    path: `utils/leader-stories/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: LeaderStoriesResponse
  }) => response.data,
);

export const getLeaderStory = makeHandler(
  'getLeaderStory',
  (req: {token: string; id: number}) => ({
    method: 'GET',
    path: `utils/leader-stories/${req.id}/`,
    token: req.token,
  }),
  (response: {
    data: LeaderStory
  }) => response.data,
);

export const getGalleries = makeHandler(
  'getGalleries',
  (req: {token: string; params: PageParams}) => ({
    method: 'GET',
    path: `events/gallery/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: GalleriesResponse
  }) => response.data,
);

export const getGallery = makeHandler(
  'getGallery',
  (req: {token: string; id: number}) => ({
    method: 'GET',
    path: `events/gallery/${req.id}/`,
    token: req.token,
  }),
  (response: {
    data: Gallery
  }) => response.data,
);

export type PageParams = {
  limit: number;
  offset?: number;
};

export type FaqResponse = {
  results: Faq[];
  count: number;
};

export type LeaderStoriesResponse = {
  results: LeaderStory[];
  count: number;
};

export type GalleriesResponse = {
  results: Gallery[];
  count: number;
};

function getRequestString(req: PageParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}
