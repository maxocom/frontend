import {makeHandler} from './makeHandler';
import {FormDataEntry} from './request';

export const uploadFile = makeHandler(
  'uploadFile',
  (req: {token: string; file: string}) => ({
    method: 'POST',
    path: 'files/',
    token: req.token,
    form: getFileFormData(req.file)
  }),
  (response: {
    data: MediaFile
  }) => response.data,
);

export interface MediaFile {
  url: string;
}

function getFileFormData(file: string): FormDataEntry[] {
  return [
    {name: 'file_url', value: { type: 'image/jpg', uri: file, name: 'image.jpg' } },
  ];
}