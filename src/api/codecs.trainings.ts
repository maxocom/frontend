import {makeHandler} from './makeHandler'

export type Lesson = {
  id: number;
  name: string;
  seen_by_me: boolean;
  description: string;
  video: string;
};

export type Training = {
  id: number;
  name: string;
  lessons?: Lesson[];
};

export const getTrainings = makeHandler(
  'getTrainings',
  (req: {token: string; params: TrainingsRequestParams}) => ({
    method: 'GET',
    path: `education/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: TrainingsResponse
  }) => response.data,
);

export const getTraining = makeHandler(
  'getTraining',
  (req: {token: string;  params: TrainingsRequestParams}) => ({
    method: 'GET',
    path: `education/course/?${getRequestString(req.params)}`,
    token: req.token,
  }),
  (response: {
    data: LessonsResponse
  }) => response.data,
);

export type TrainingsResponse = {
  results: Training[];
};

export type LessonsResponse = {
  results: Lesson[];
  count: number;
};

export type TrainingsRequestParams = {
  limit: number;
  offset?: number;
  page?: number;
  education_id?: number;
};

function getRequestString(req: TrainingsRequestParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}
