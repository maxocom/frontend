import {API_BASE_URL} from '../constants/general';

export type Request = {
  /** HTTP method */
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
  /** path */
  path: string;
  /** JSON data */
  data?: object;
  /** Form data */
  form?: FormDataEntry[];
  /** auth token */
  token?: string;
  /** Any additional headers */
  headers?: Record<string, string>;
  apiType?: RequestApiType;
};

export type FormDataEntry = {
  name: string;
  value: string | {uri: string; type: string; name: string};
};

export enum RequestApiType {
  BaseApi = 'base',
}

export const ApiUrlPrefixes = {
  [RequestApiType.BaseApi]: API_BASE_URL,
};
