import {makeHandler} from './makeHandler';

export const getProfile = makeHandler(
  'getProfile',
  (token: string) => ({
    method: 'GET',
    path: 'users/get-me/',
    token,
  }),
  (response: {
    data: Profile
  }) => response.data,
);

export const updateUser = makeHandler(
  'updateUser',
  (req: {token: string; user: User4Export, userId: number}) => ({
    method: 'PUT',
    path: `users/custom-user/${req.userId}/`,
    token: req.token,
    data: req.user,
  }),
  (profile: Profile) => profile,
);

export const sendVerification = makeHandler(
  'sendVerification',
  (req: {token: string; verification: Verification4Export, userId: number}) => ({
    method: 'PUT',
    path: `users/edit-user/${req.userId}/`,
    token: req.token,
    data: req.verification,
  }),
  (profile: Profile) => profile,
);

export const getTop = makeHandler(
  'getTop',
  (req: {params: TopUserRequestParams}) => ({
    method: 'GET',
    path: `users/rating/?${getTopUserRequestString(req.params)}`,
  }),
  (response: {
    data: TopUserResponse
  }) => response.data,
);

export const createFcmRegistration = makeHandler(
  'createFcmRegistration',
  (req: {token: string; data: FcmRegistrationData}) => ({
    method: 'POST',
    path: 'users/push-token/',
    token: req.token,
    data: req.data,
  }),
  (response: {
    data: FcmRegistrationResponse
  }) => response.data,
);

export const updateFcmRegistration = makeHandler(
  'updateFcmRegistration',
  (req: {token: string; data: FcmRegistrationData, fcmTokenId: number}) => ({
    method: 'PUT',
    path: `users/push-token/${req.fcmTokenId}/`,
    token: req.token,
    data: req.data,
  }),
  (response: {
    data: FcmRegistrationResponse
  }) => response.data,
);

export const removeFcmRegistration = makeHandler(
  'removeFcmRegistration',
  (req: {token: string; fcmTokenId: number}) => ({
    method: 'DELETE',
    path: `users/push-token/${req.fcmTokenId}/`,
    token: req.token,
  }),
  (response) => response,
);

export interface Profile {
  id: number;
  first_name: string;
  last_name: string;
  image: string;
  instagram_name: string;
  email: string;
  phone_number: string;
  is_active: boolean;
  team: number | null;
  document: string;
  statistic_is_writen: boolean;
  verification_status: string;
}

export interface TopUser {
  id: number;
  first_name: string;
  last_name: string;
  image: string;
  instagram_name: string;
  team: number | null;
  amount: number | null;
}

export interface User4Export {
  first_name: string;
  last_name: string;
  image: string;
  instagram_name: string;
  email: string;
}

export interface Verification4Export {
  email: string;
  team: number | null;
  instagram_name: string;
  document: string;
}

function getTopUserRequestString(req: TopUserRequestParams): string {
  const keys = Object.keys(req);
  let res: string = '';
  for (const key of keys) {
    res = `${res}&${key}=${encodeURIComponent(req[key])}`;
  }
  return res;
}

export type TopUserRequestParams = {
  limit: number;
  offset?: number;
};

export type FcmRegistrationData = {
  user: number;
  push_token: string;
};

export type TopUserResponse = {
  results: TopUser[];
  count: number;
};

export type FcmRegistrationResponse = {
  id: number;
  push_token: string;
};

